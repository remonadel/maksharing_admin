<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class News extends MX_Controller {

	public function __construct()
    {
        parent::__construct();
		$this->deny->deny_if_logged_out();

		$this->load->model("news_model");
    }


	public function index()
	{
		redirect(site_url() . "news/unpublished");
	}


	public function unpublished()
	{
		$authorized = $this->common_model->authorized_to_view_page("news");
		if ($authorized)
		{
			$data = array();

			$current_page = (int) $this->uri->segment(3);
			$per_page = 20;
			$news_count = $this->news_model->get_news_count(0);

			$config["base_url"] = site_url() . "news/unpublishd";
			$config['uri_segment'] = 3;
			$config["total_rows"] = $news_count;
			$config["per_page"] = $per_page;
			$this->pagination->initialize($config);

			$news = $this->news_model->get_news(0, $current_page, $per_page);
			if ($news)
			{
				$data["news"] = $news;
				$data["pagination"] = $this->pagination->create_links();
			}

			if (isset($_POST["search"]))
			{
				$query = htmlspecialchars(trim($_POST["search"]));
				redirect(site_url() . "news/search/$query");
			}

			$this->load->view("manage_news_view", $data);
		}
    }


	public function published()
	{
		$authorized = $this->common_model->authorized_to_view_page("news");
		if ($authorized)
		{
			$data = array();

			$current_page = (int) $this->uri->segment(3);
			$per_page = 20;
			$news_count = $this->news_model->get_news_count(1);

			$config["base_url"] = site_url() . "news/publishd";
			$config['uri_segment'] = 3;
			$config["total_rows"] = $news_count;
			$config["per_page"] = $per_page;
			$this->pagination->initialize($config);

			$news = $this->news_model->get_news(1, $current_page, $per_page);
			if ($news)
			{
				$data["news"] = $news;
				$data["pagination"] = $this->pagination->create_links();
			}

			if (isset($_POST["search"]))
			{
				$query = htmlspecialchars(trim($_POST["search"]));
				redirect(site_url() . "news/search/$query");
			}

			$this->load->view("manage_news_view", $data);
		}
    }


	public function search($query)
	{
		$authorized = $this->common_model->authorized_to_view_page("news");
		if ($authorized)
		{
			if (empty($query)) redirect(site_url() . "news/unpublished");
			$query = urldecode($query);

			$data = array();

			$current_page = (int) $this->uri->segment(4);
			$per_page = 20;
			$news_count = $this->news_model->get_search_rows_count($query);

			$config["base_url"] = site_url() . "news/search/$query";
			$config['uri_segment'] = 4;
			$config["total_rows"] = $news_count;
			$config["per_page"] = $per_page;
			$this->pagination->initialize($config);

			$news = $this->news_model->search_news($query, $current_page, $per_page);
			if ($news)
			{
				$data["news"] = $news;
				$data["pagination"] = $this->pagination->create_links();
			}

			if (isset($_POST["search"]))
			{
				$query = htmlspecialchars(trim($_POST["search"]));
				redirect(site_url() . "news/search/$query");
			}

			$this->load->view("manage_news_view", $data);
		}
	}


	public function add()
	{
        $authorized = $this->common_model->authorized_to_view_page("news");
		if ($authorized)
		{
			$data = array();

			$users = $this->news_model->get_all_users();
			if ($users)	$data["users"] = $users;

			if (isset($_POST["submit"]))
			{
				$title = htmlspecialchars(trim($_POST["title"]));
				$description = htmlspecialchars(trim($_POST["description"]));
				$content = str_replace('../uploads/', site_url().'uploads/', trim($_POST["content"]));
				$content = htmlspecialchars(str_replace(array("<br />", "<br>", "<br/>"), "", $content));
				$main_image = (empty($_POST["main_image"])) ? NULL : $_POST["main_image"];
				$main_image_label = NULL;
				if ( ! is_null($main_image)) $main_image_label = (empty($_POST["main_image_label"])) ? NULL : htmlspecialchars(trim($_POST["main_image_label"]));
				$main_album = (empty($_POST["main_album"])) ? NULL : $_POST["main_album"];
				$author = (empty($_POST["author"])) ? NULL : htmlspecialchars(trim($_POST["author"]));
				$photographer = (empty($_POST["photographer"])) ? NULL : htmlspecialchars(trim($_POST["photographer"]));
				$created_by_id = $this->session->userdata("id");
				$publish = (isset($_POST["publish"])) ? 1 : 0;

				if (empty($title) OR empty($description) OR empty($content) OR (is_null($main_image) && is_null($main_album)))
				{
					$data["status"] = "<p class='error-msg'>يجب إدخال جميع البيانات الإجبارية</p>";
				}
				elseif ($this->common_model->subject_exists("news", "title", $title))
				{
					$data["status"] = "<p class='error-msg'>عنوان الخبر موجود بالفعل</p>";
				}
				else
				{
					// Insert information into database
					$this->news_model->insert_news_item($title, $description, $content, $main_image, $main_image_label, $main_album, $author,
														$photographer, $created_by_id, $publish);

					// If main image was chosen, update its 'used' flag by adding 1
					if ( ! is_null($main_image)) $this->news_model->update_image_used_flag("name", $main_image, "+");

					$this->session->set_flashdata("status", "تمت العملية بنجاح");
					$redirect_url = ($publish == 0) ? site_url() . "news/unpublished" : site_url() . "news/published";
					redirect($redirect_url);
				}
			}

			$this->load->view("add_news_item_view", $data);
		}
	}


	public function view($id = "")
	{
		$authorized = $this->common_model->authorized_to_view_page("news");
		if ($authorized)
		{
			$news_item = $this->common_model->get_subject_with_token("news", "id", $id);
			if (empty($id) OR ! $news_item) show_404();

			$news_item["created_by_name"] = $this->common_model->get_info_by_token("users_details", "name", "id", $news_item["created_by_id"]);
			if ($news_item["published"] == 1) $news_item["published_by_name"] = $this->common_model->get_info_by_token("users_details", "name", "id", $news_item["published_by_id"]);
			$data["news_item"] = $news_item;

			$this->load->view("ajax_view_news_item_view", $data);
		}
	}


	public function edit($id = "")
	{
        $authorized = $this->common_model->authorized_to_view_page("news");
		if ($authorized)
		{
			$news_item = $this->common_model->get_subject_with_token("news", "id", $id);
			if (empty($id) OR ! $news_item) show_404();

			$data["news_item"] = $news_item;

			$users = $this->news_model->get_all_users();
			if ($users)	$data["users"] = $users;

			if (isset($_POST["submit"]))
			{
				$title = htmlspecialchars(trim($_POST["title"]));
				$description = htmlspecialchars(trim($_POST["description"]));
				$content = str_replace('../uploads/', site_url().'uploads/', trim($_POST["content"]));
				$content = htmlspecialchars(str_replace(array("<br />", "<br>", "<br/>"), "", $content));
				$main_image = (empty($_POST["main_image"])) ? NULL : $_POST["main_image"];
				$main_image_label = NULL;
				if ( ! is_null($main_image)) $main_image_label = (empty($_POST["main_image_label"])) ? NULL : htmlspecialchars(trim($_POST["main_image_label"]));
				$main_album = (empty($_POST["main_album"])) ? NULL : $_POST["main_album"];
				$author = (empty($_POST["author"])) ? NULL : htmlspecialchars(trim($_POST["author"]));
				$photographer = (empty($_POST["photographer"])) ? NULL : htmlspecialchars(trim($_POST["photographer"]));
				$publish = (isset($_POST["publish"])) ? 1 : 0;
				$published_here = ($news_item["published"] == 0 && $publish == 1) ? TRUE : FALSE;
				$unpublished_here = ($news_item["published"] == 1 && $publish == 0) ? TRUE : FALSE;

				if (empty($title) OR empty($description) OR empty($content) OR (is_null($main_image) && is_null($main_album)))
				{
					$data["status"] = "<p class='error-msg'>يجب إدخال جميع البيانات الإجبارية</p>";
				}
				elseif (($title != $news_item["title"]) && ($this->common_model->subject_exists("news", "title", $title)))
				{
					$data["status"] = "<p class='error-msg'>عنوان الخبر موجود بالفعل</p>";
				}
				else
				{
					// Edit information on database
					$this->news_model->edit_news_item($title, $description, $content, $main_image, $main_image_label, $main_album, $author, $photographer,
													  $published_here, $unpublished_here, $id);

					// Update old and/or new image's 'used' flags appropriately
					if (is_null($news_item["main_image"]) && ! is_null($main_image))
					{
						$this->news_model->update_image_used_flag("name", $main_image, "+");
					}
					elseif ( ! is_null($news_item["main_image"]) && is_null($main_image))
					{
						$this->news_model->update_image_used_flag("name", $news_item["main_image"], "-");
					}
					elseif ( ! is_null($news_item["main_image"]) && ! is_null($main_image))
					{
						if ($news_item["main_image"] != $main_image)
						{
							$this->news_model->update_image_used_flag("name", $news_item["main_image"], "-");
							$this->news_model->update_image_used_flag("name", $main_image, "+");
						}
					}

					$this->session->set_flashdata("status", "تمت العملية بنجاح");
					$redirect_url = ($publish == 0) ? site_url() . "news/unpublished" : site_url() . "news/published";
					redirect($redirect_url);
				}
			}

			$this->load->view("edit_news_item_view", $data);
		}
	}


	public function delete($id = "")
    {
		$authorized = $this->common_model->authorized_to_view_page("news");
		if ($authorized)
		{
			$news_item_exists = $this->common_model->subject_exists("news", "id", $id);
			if (empty($id) OR ! $news_item_exists) show_404();

			$this->common_model->delete_subject("news", "id", $id);

			$this->session->set_flashdata("status", "تمت العملية بنجاح");
			redirect($_SERVER['HTTP_REFERER']);
		}
	}

}


/* End of file news.php */
/* Location: ./application/modules/news/controllers/news.php */
