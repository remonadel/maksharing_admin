<?php $this->load->view("header"); ?>

<div class="container">
    <div class="col-md-12" style="margin-bottom: 20px;">
        <div class="col-md-62">
            <div class="main-title">
                <?php if ($this->uri->segment(2) == "unpublished"): ?>
                    <h1>إدارة الأخبار الغير منشورة</h1>
                <?php elseif ($this->uri->segment(2) == "published"): ?>
                    <h1>إدارة الأخبار المنشورة</h1>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="masonary-grids">
            <div class="col-md-12">
                <div class="widget-area" style="margin-top: 0;">
                    <?php if ($this->session->flashdata("status")): ?>
                        <div class="col-md-122" id="status" style="background-color: #EEE; padding: 10px; margin-bottom: 25px; margin-top: -10px;"><p class="success-msg"><?= $this->session->flashdata("status"); ?></p></div>
                    <?php endif; ?>
                    <div class="col-md-5" style="margin-top: -24px; margin-left: -15px;">
                        <div class="inline-form">
                            <label class="c-label">النوع</label>
                            <select id="news_type">
                                <?php if ($this->uri->segment(2) == "unpublished"): ?>
                                    <option value=""></option>
                                    <option value="unpublished" selected>الأخبار الغير منشورة</option>
                                    <option value="published">الأخبار المنشورة</option>
                                <?php elseif ($this->uri->segment(2) == "published"): ?>
                                    <option value=""></option>
                                    <option value="unpublished">الأخبار الغير منشورة</option>
                                    <option value="published" selected>الأخبار المنشورة</option>
                                <?php else: ?>
                                    <option value=""></option>
                                    <option value="unpublished">الأخبار الغير منشورة</option>
                                    <option value="published">الأخبار المنشورة</option>
                                <?php endif; ?>
                            </select>
                        </div>
                        <div class="col-md-5" >
                            <form action="" method="post" class="search" id="search_by" style="margin-top: -37px; margin-left: 950px;">
                                <input type="text" class="serch_txt" name="search" placeholder="بحث" />
                                <button type="submit"><i class="fa fa-search"></i></button>
                            </form>
                        </div>
                    </div>
                    <div class="streaming-table" style="margin-top: 0px;">
                        <span id="found" class="label label-info"></span>
                        <table id="stream_table" class='table table-striped table-bordered test'>
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>الخبر</th>
                                    <th class="tables-15-width-th">الحالة</th>
                                    <th>التحكم</th>
                                </tr>
                            </thead>
                            <tbody class="tbody_admin">
                                <?php if (isset($news)): ?>
                                <?php foreach ($news as $news_item): ?>
                                    <tr class="tr_<?= $news_item["id"]; ?>">
                                        <td><?= $news_item["id"]; ?></td>
                                        <td>
                                            <?php if ( ! is_null($news_item['main_image'])): ?>
                                                <img class="img-mang-arti" src="<?= IMG_ARCHIVE . "311x253/" . $news_item['main_image']; ?>" />
                                            <?php elseif ( ! is_null($news_item['main_album'])): ?>
                                                <img class="img-mang-arti" src="<?= IMG_ARCHIVE . "311x253/" . end(explode('&', $news_item['main_album'])); ?>" />
                                            <?php endif; ?>
                                            <div class="des-sum-arti">
                                                <p class="p-title-14"><?= $news_item["title"]; ?></p>
                                                <p class="p-desc-ription" title="<?= $news_item["description"]; ?>">
                                                    <?= mb_substr($news_item["description"], 0, 80, "utf-8"); ?>
                                                </p>
                                                <div style="width: 660px; height: 20px; overflow: hidden; float: right; display: table;">
                                                    <p class="sub-menus">تاريخ الإدخال: <?= $news_item["created_at"]; ?></p>
                                                </div>
                                            </div>
                                        </td>
                                        <?php if ($news_item["published"] == 1): ?>
                                            <td class="tables-centered-both-td" style="color: green;">منشور</td>
                                        <?php elseif ($news_item["published"] == 0): ?>
                                            <td class="tables-centered-both-td" style="color: red;">غير منشور</td>
                                        <?php endif; ?>
                                        <td>
                                            <a onclick="viewInModal('news/view/<?= $news_item['id']; ?>');" href="javascript:void(null);">
                                                <button class="btn btn-primary btn-font tables-full-width-btn" data-toggle="modal" data-target=".bs-example-modal-lg" type="button"> التفاصيل</button>
                                            </a>
                                            <a href="<?= site_url(); ?>news/edit/<?= $news_item['id']; ?>">
                                                <button class="btn btn-warning btn-font new-width" type="button">تعديل</button>
                                            </a>
                                            <a onclick="alertDelete('news/delete/<?= $news_item['id']; ?>', 'هل أنت متأكد من حذف هذا الخبر؟');" href="javascript:void(null);">
                                                <button class="btn btn-danger btn-font new-width" type="button" >حذف</button>
                                            </a>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                                <?php endif; ?>
                            </tbody>
                        </table>
                    </div>

                    <?php if (isset($pagination)): ?>
                        <div class="pagination-news">
                            <?= $pagination; ?>
                        </div>
                    <?php endif; ?>

                    <!-- Empty modal -->
                    <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content"></div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
<?php //$this->load->view("slide_panel");   ?>
</div><!-- Page Container -->
<?php $this->load->view("footer"); ?>
<script>
    $(document).ready(function () {
        $("#news_type").on("change", function () {
            if (this.value)
            {
                window.location.href = "<?= site_url(); ?>news/" + this.value;
            }
        });

        setTimeout(function () {
            $("#status").fadeOut(2000);
        }, 3000);
    });
</script>
</body>
</html>
