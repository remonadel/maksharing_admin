<div class="widget-area" style="margin-top: 0px;">
	<div class="col-md-122" style="padding: 20px; margin-bottom: 30px; background-color: #EEE;">
		<div class="col-md-62">
			<?php if ($news_item["published"] == 1): ?>
				<h4>
					<span class="fon-bo-ba">الرابط: </span>
					<span class="fon-rg-ba"><a target="_blank" href="http://www.electricity.com/news/details/<?= $news_item["id"]; ?>">مشاهدة الخبر</a></span>
				</h4>
			<?php endif; ?>
			<?php if ( ! is_null($news_item["author"])): ?>
				<h4>
					<span class="fon-bo-ba">كتب: </span>
					<span class="fon-rg-ba"><?= $news_item["author"]; ?></span>
				</h4>
			<?php endif; ?>
			<h4>
				<span class="fon-bo-ba">أُدخل بواسطة: </span>
				<span class="fon-rg-ba"><?= $news_item["created_by_name"]; ?></span>
			</h4>
			<h4>
				<span class="fon-bo-ba">تاريخ الإدخال: </span>
				<span class="fon-rg-ba"><?= date("Y-m-d H:i:s", strtotime($news_item["created_at"])); ?></span>
			</h4>
		<?php if ($news_item["published"] == 1): ?>
			<h4>
				<span class="fon-bo-ba">نُشر بواسطة: </span>
				<span class="fon-rg-ba"><?= $news_item["published_by_name"]; ?></span>
			</h4>
			<h4>
				<span class="fon-bo-ba">تاريخ النشر: </span>
				<span class="fon-rg-ba"><?= date("Y-m-d H:i:s", strtotime($news_item["published_at"])); ?></span>
			</h4>
		<?php endif; ?>
		</div>
		<div class="col-md-62" style="position: relative; right: 94px;">
			<?php if ( ! is_null($news_item["main_image"])): ?>
				<img src="<?= IMG_ARCHIVE . "311x253/" . $news_item['main_image']; ?>" />
			<?php elseif ( ! is_null($news_item["main_album"])): ?>
				<img src="<?= IMG_ARCHIVE . "311x253/" . end(explode("&", $news_item['main_album'])); ?>" />
			<?php endif; ?>
		</div>
	</div>
	<div class="fon-bo-y">
        <h3 style="width: 90%; margin-bottom: 15px; font-family: font_bo; color: #cc0000"><?= $news_item["title"]; ?></h3>
		<?= htmlspecialchars_decode($news_item["content"]); ?>
	</div>
</div>
<script>
    $(".fon-bo-y p").addClass("p-description");
</script>
