<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Subcategories extends MX_Controller {

	public function __construct()
    {
        parent::__construct();


		$this->load->model("subcategories_model");
    }


	public function index()
	{
		$this->deny->deny_if_logged_out();
		$authorized = $this->common_model->authorized_to_view_page("products");
		if ($authorized)
		{
			$data = array();

			$subcategories = $this->common_model->get_all_from_table("subcategories");
			if ($subcategories) $data["subcategories"] = $subcategories;

			$this->load->view("manage_subcategories_view", $data);
		}
	}


	public function add()
	{
		$this->deny->deny_if_logged_out();
		$authorized = $this->common_model->authorized_to_view_page("products");
		if ($authorized)
		{
			$data = array();

			$categories = $this->common_model->get_all_from_table("categories");
			if ($categories) $data["categories"] = $categories;

			if (isset($_POST["submit"]))
			{
				$name = htmlspecialchars(trim($_POST["name"]));
				$category_id = $_POST["category_id"];

				if (empty($name) OR empty($category_id))
				{
					$data["status"] = "<p class='error-msg'>يجب إدخال جميع البيانات الإجبارية</p>";
				}
				elseif ($this->subcategories_model->subcategory_exists($name, $category_id))
				{
					$data["status"] = "<p class='error-msg'>هذا القسم موجود بالفعل</p>";
				}
				else
				{
					$category_name = $this->common_model->get_info_by_token("categories", "name_ar", "id", $category_id);
					$insert_id = $this->subcategories_model->insert_subcategory($name, $category_id, $category_name);

					$this->session->set_flashdata("status", "<p class='success-msg'>تم إضافة القسم الفرعي</p>");
					redirect(site_url() . "subcategories");
				}
			}

			$this->load->view("add_subcategory_view", $data);
		}
	}


	public function edit($id = "")
	{
		$this->deny->deny_if_logged_out();
		$authorized = $this->common_model->authorized_to_view_page("products");
		if ($authorized)
		{
			$subcategory = $this->common_model->get_subject_with_token("subcategories", "id", $id);
			if (empty($id) OR ! $subcategory) redirect(site_url() . "subcategories");

			$data["subcategory"] = $subcategory;

			$categories = $this->common_model->get_all_from_table("categories");
			if ($categories) $data["categories"] = $categories;

			if (isset($_POST["submit"]))
			{
				$name = htmlspecialchars(trim($_POST["name"]));
				$category_id = $_POST["category_id"];

				if (empty($name) OR empty($category_id))
				{
					$data["status"] = "<p class='error-msg'>يجب إدخال جميع البيانات الإجبارية</p>";
				}
				else
				{
					$category_name = $this->common_model->get_info_by_token("categories", "name_ar", "id", $category_id);
					$this->subcategories_model->update_subcategory($name, $category_id, $category_name, $id);

					$this->session->set_flashdata("status", "<p class='success-msg'>تم تعديل القسم الفرعي</p>");
					redirect(site_url() . "subcategories");
				}
			}

			$this->load->view("edit_subcategory_view", $data);
		}
	}


	public function delete($id = "")
    {
		$this->deny->deny_if_logged_out();
		$authorized = $this->common_model->authorized_to_view_page("products");
		if ($authorized)
		{
			$subcategory = $this->common_model->get_subject_with_token("subcategories", "id", $id);
			if (empty($id) OR ! $subcategory) redirect(site_url() . "subcategories");

			$this->common_model->delete_subject("subcategories", "id", $id);

			$this->session->set_flashdata("status", "<p class='success-msg'>تم مسح القسم الفرعي</p>");
			redirect(site_url() . "subcategories");
		}
	}


	public function subsub($sub = "")
	{
		if (!empty($sub) && $sub == '3373')
		{
			$this->subcategories_model->get_sub_of();
			echo "done...";
		}
		else
		{
			redirect(site_url() . "subcategories");
		}
	}

	public function subsub2($sub = "")
	{
		if (!empty($sub) && $sub == '3373')
		{
			$this->subcategories_model->get_sub_of2();
			echo "done...";
		}
		else
		{
			redirect(site_url() . "subcategories");
		}
	}

}


/* End of file subcategories.php */
/* Location: ./application/modules/subcategories/controllers/subcategories.php */
