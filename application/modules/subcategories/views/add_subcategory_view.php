<?php $this->load->view("header"); ?>

<div class="container">
	<div class="col-md-12">
		<div class="main-title">
			<h1>إضافة قناة IPTv </h1>
		</div>
	</div>
	<div class="row">
		<div class="masonary-grids">
			<div class="col-md-12">
				<div class="widget-area">
					<div class="wizard-form-h">
						<form action="" method="post">
							<div class="col-md-122" style="margin-bottom: 10px;">
								<?php if (isset($status)) echo $status; ?>
							</div>
							<div class="col-md-122">
								<div class="inline-form">
									<label class="c-label">إسم القناة</label>
									<input class="input-style2" type="text" name="name"
										   value="<?php if (isset($_POST["name"])) echo htmlspecialchars(trim($_POST['name'])); ?>" required autofocus />
								</div>
							</div>
							<div class="col-md-122">
								<div class="inline-form">
									<label class="c-label">تصنيف القناة </label>
									<select name="category_id" required>
										<option value=""></option>
										<?php if (isset($categories)): ?>
											<?php foreach ($categories as $category): ?>
												<?php if (isset($_POST["category_id"]) && $_POST["category_id"] == $category["id"]): ?>
													<option value="<?= $category["id"]; ?>" selected><?= $category["name_ar"]; ?></option>
												<?php else: ?>
													<option value="<?= $category["id"]; ?>"><?= $category["name_ar"]; ?></option>
												<?php endif; ?>
											<?php endforeach; ?>
										<?php endif; ?>
									</select>
								</div>
							</div>
							<div class="col-md-122" style="margin-top: 12px;">
								<input type="submit" name="submit" value="حفظ" class="btn btn-success btn-font" />
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php //$this->load->view("slide_panel"); ?>
</div><!-- Page Container -->
<?php $this->load->view("footer"); ?>
</body>
</html>
