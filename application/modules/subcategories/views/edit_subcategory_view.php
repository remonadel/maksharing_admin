<?php $this->load->view("header"); ?>

<div class="container">
	<div class="col-md-12">
		<div class="main-title">
			<h1>تعديل  قناة IPTv</h1>
		</div>
	</div>
	<div class="row">
		<div class="masonary-grids">
			<div class="col-md-12">
				<div class="widget-area">
					<div class="wizard-form-h">
						<form action="" method="post">
							<div class="col-md-122" style="margin-bottom: 10px;">
								<?php if (isset($status)) echo $status; ?>
							</div>
							<div class="col-md-122">
								<div class="inline-form">
									<label class="c-label">إسم القسم</label>
									<input class="input-style" type="text" name="name"
										   value="<?= $subcategory["name"]; ?>" required />
								</div>
							</div>
							<div class="col-md-122">
								<div class="inline-form">
									<label class="c-label">القسم الرئيسي التابع له</label>
									<select name="category_id" required>
										<option value=""></option>
										<?php if (isset($categories)): ?>
											<?php foreach ($categories as $category): ?>
												<?php if ($subcategory["category_id"] == $category["id"]): ?>
													<option value="<?= $category["id"]; ?>" selected><?= $category["name_ar"]; ?></option>
												<?php else: ?>
													<option value="<?= $category["id"]; ?>"><?= $category["name_ar"]; ?></option>
												<?php endif; ?>
											<?php endforeach; ?>
										<?php endif; ?>
									</select>
								</div>
							</div>
							<div class="col-md-122" style="margin-top: 12px;">
								<input type="submit" name="submit" value="Save" class="btn btn-success large" />
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php //$this->load->view("slide_panel"); ?>
</div><!-- Page Container -->
<?php $this->load->view("footer"); ?>
</body>
</html>
