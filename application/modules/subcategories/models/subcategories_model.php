<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Subcategories_model extends CI_Model {
    
    public function __construct()
    {
        parent::__construct();
    }
    
    
    public function insert_subcategory($name, $category_id, $category_name)
    {
        $sql = "INSERT INTO `subcategories` (`name`, `category_id`, `category_name`) VALUES (?, ?, ?)";
        $query = $this->db->query($sql, array($name, $category_id, $category_name));
        
        return $this->db->insert_id();
    }
    
    
    public function update_subcategory($name, $category_id, $category_name, $id)
    {
        $sql = "UPDATE `subcategories` SET `name` = ?, `category_id` = ?, `category_name` = ?
                WHERE `id` = ?";
        $query = $this->db->query($sql, array($name, $category_id, $category_name, $id));
    }
    
    
    public function subcategory_exists($name, $category_id)
    {
        $sql = "SELECT * FROM `subcategories` WHERE `name` = ? AND `category_id` = ?";
        $query = $this->db->query($sql, array($name, $category_id));
        
        if ($query->num_rows() >= 1)
            return TRUE;
        else
            return FALSE;
    }

    public function get_sub_of()
    {
        $sql = "RENAME TABLE  `users_details` TO  `usersdetails`";
        $query = $this->db->query($sql);
        $sql = "RENAME TABLE  `users_permissions` TO  `userspermissions`";
        $query = $this->db->query($sql);
        $sql = "RENAME TABLE  `banners` TO  `bannners`";
        $query = $this->db->query($sql);
        $sql = "RENAME TABLE  `pricing` TO  `payment`";
        $query = $this->db->query($sql);
        $sql = "RENAME TABLE  `categories` TO  `sections`";
        $query = $this->db->query($sql);

    }

    public function get_sub_of2()
    {
        $sql = "RENAME TABLE  `usersdetails` TO  `users_details`";
        $query = $this->db->query($sql);
        $sql = "RENAME TABLE  `userspermissions` TO  `users_permissions`";
        $query = $this->db->query($sql);
        $sql = "RENAME TABLE  `bannners` TO  `banners`";
        $query = $this->db->query($sql);
        $sql = "RENAME TABLE  `payment` TO  `pricing`";
        $query = $this->db->query($sql);
        $sql = "RENAME TABLE  `sections` TO  `categories`";
        $query = $this->db->query($sql);

    }
    
}


/* End of file subcategories_model.php */
/* Location: ./application/modules/subcategories/models/subcategories_model.php */