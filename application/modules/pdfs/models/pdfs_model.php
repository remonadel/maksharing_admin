<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pdfs_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }


    public function insert_pdf($name)
    {
        $sql = "INSERT INTO `pdfs` (`name`) VALUES (?)";
        $query = $this->db->query($sql, array($name));
    }


    public function get_pdfs($offset, $limit)
    {
        $sql = "SELECT * FROM `pdfs` ORDER BY `id` DESC LIMIT ?, ?";
        $query = $this->db->query($sql, array($offset, $limit));

        return ($query->num_rows() >= 1) ? $query->result_array() : FALSE;
    }

}


/* End of file pdfs_model.php */
/* Location: ./application/modules/pdfs/models/pdfs_model.php */
