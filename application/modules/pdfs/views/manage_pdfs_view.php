<?php $this->load->view("header"); ?>

<div class="container">
	<div class="col-md-12">
		<div class="main-title">
			<h1>إدارة ال PDFS</h1>
		</div>
	</div>
	<div class="row">
		<div class="masonary-grids">
			<div class="col-md-12">
				<div class="widget-area">
					<?php if ($this->session->flashdata("status")): ?>
						<div class="col-md-122" id="status" style="background-color: #EEE; padding: 10px;"><?= $this->session->flashdata("status"); ?></div>
					<?php endif; ?>
					<div class="streaming-table">
						<span id="found" class="label label-info"></span>
						<table id="stream_table" class='table table-striped table-bordered'>
							<thead>
								<tr>
									<th>ID</th>
									<th>الرابط</th>
									<th style="width: 20%;">تاريخ الرفع</th>
									<th style="width: 20%;">مسح</th>
								</tr>
							</thead>
							<tbody class="tbody_admin">
								<?php if (isset($pdfs)): ?>
								<?php foreach ($pdfs as $pdf): ?>
									<tr>
										<td><?= $pdf["id"]; ?></td>
										<td style="text-align: center; vertical-align: middle; direction: ltr;">
											<?= PDFS . $pdf["name"]; ?>
											<a target="_blank" href="<?= PDFS . $pdf["name"]; ?>">
												<i class="fa fa-external-link-square long-url-btn"
												   style="cursor: pointer; color: #333; float: left; position: relative; left: 12px;" title="إذهب إلي الملف"></i>
											</a>
										</td>
										<td style="text-align: center; vertical-align: middle; direction: ltr;"><?= $pdf["uploaded_at"]; ?><h4></td>
										<td style="text-align: center; vertical-align: middle;">
											<a onclick="alertDelete('pdfs/delete/<?= $pdf['id']; ?>', 'هل أنت متأكد من مسح هذا الملف؟');" href="javascript:void(null);">
	                                            <button class="btn btn-danger btn-font" style="width: 150px;" type="button" >حذف</button>
											</a>
										</td>
									</tr>
								<?php endforeach; ?>
								<?php endif; ?>
							</tbody>
						</table>
					</div>

					<?php if (isset($pagination)): ?>
						<div class="pagination-news">
							<?= $pagination; ?>
						</div>
					<?php endif; ?>

				</div>
			</div>
		</div>
	</div>
	<?php //$this->load->view("slide_panel"); ?>
</div><!-- Page Container -->
<?php $this->load->view("footer"); ?>
<script>
	$(document).ready(function() {
		setTimeout(function() {
			$("#status").fadeOut(2000);
		}, 3000);
	});
</script>
</body>
</html>
