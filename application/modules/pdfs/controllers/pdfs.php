<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pdfs extends MX_Controller {

	public function __construct()
    {
        parent::__construct();
		$this->deny->deny_if_logged_out();

		$this->load->model("pdfs_model");
    }


	public function index()
	{
		$authorized = $this->common_model->authorized_to_view_page("pdfs");
		if ($authorized)
		{
			$data = array();

			$current_page = (int) $this->uri->segment(2);
			$per_page = 20;
			$pdfs_count = $this->common_model->get_table_rows_count("pdfs");

			$config["base_url"] = site_url() . "pdfs/";
			$config['uri_segment'] = 2;
			$config["total_rows"] = $pdfs_count;
			$config["per_page"] = $per_page;
			$this->pagination->initialize($config);

			$pdfs = $this->pdfs_model->get_pdfs($current_page, $per_page);
			if ($pdfs)
			{
				$data["pdfs"] = $pdfs;
				$data["pagination"] = $this->pagination->create_links();
			}

			$this->load->view("manage_pdfs_view", $data);
		}
    }


	public function add()
	{
		$authorized = $this->common_model->authorized_to_view_page("pdfs");
		if ($authorized)
		{
			$data = array();

			if (isset($_POST["submit"]))
			{
				$pdf_name = basename($_FILES["pdf"]["name"]);

				if (empty($pdf_name))
				{
					$data["status"] = "<p class='error-msg'>يجب رفع الملف</p>";
				}
				else
				{
					$tmp_name = $_FILES["pdf"]["tmp_name"];
					$allowed_exts = array("pdf");
					$a = explode(".", $pdf_name);
					$file_ext = strtolower(end($a)); unset($a);
					$path = PDFS_PATH;

					// Restricting file uploading to image files only
					if ( ! in_array($file_ext, $allowed_exts))
					{
						$data["status"] = "<p class='error-msg'>لقد قمت برفع ملف غير مسموح به</p>";
					}
					else
					{
						// Success. Give the file a unique name and upload it, then insert data in database
						$cur_time = time();
						$pdf = $cur_time . "." . $file_ext;
						move_uploaded_file($tmp_name, $path . $pdf);

						$this->pdfs_model->insert_pdf($pdf);

						$this->session->set_flashdata("status", "<p class='success-msg'>تم إضافة الملف</p>");
						redirect(site_url() . "pdfs");
					}
				}
			}

			$this->load->view("add_pdf_view", $data);
		}
	}


	public function delete($id = "")
    {
		$authorized = $this->common_model->authorized_to_view_page("pdfs");
		if ($authorized)
		{
			$pdf = $this->common_model->get_subject_with_token("pdfs", "id", $id);
			if (empty($id) OR ! $pdf) redirect(site_url() . "pdfs");

			$this->common_model->delete_subject("pdfs", "id", $id);
			if (file_exists(PDFS_PATH . $pdf["name"])) unlink(PDFS_PATH . $pdf["name"]);

			$this->session->set_flashdata("status", "<p class='success-msg'>تم مسح الملف</p>");
			redirect(site_url() . "pdfs");
		}
	}

}


/* End of file pdfs.php */
/* Location: ./application/modules/pdfs/controllers/pdfs.php */
