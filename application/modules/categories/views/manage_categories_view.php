<?php $this->load->view("header"); ?>

<div class="container">
	<div class="col-md-12">
		<div class="main-title">
			<h1>إدارة تصنيفات IPTv</h1>
		</div>
	</div>
	<div class="row">
		<div class="masonary-grids">
			<div class="col-md-12">
				<div class="widget-area">
					<?php if ($this->session->flashdata("status")): ?>
						<div class="col-md-122" id="status" style="background-color: #EEE; padding: 10px;"><?= $this->session->flashdata("status"); ?></div>
					<?php endif; ?>
					<div class="streaming-table">
						<span id="found" class="label label-info"></span>
						<table id="stream_table" class='table table-striped table-bordered'>
							<thead>
								<tr>
									<th>ID</th>
									<th> الإسم باللغة العربية</th>
									<th> الإسم باللغة الانجليزية</th>
									<th style="width: 20%;">تعديل</th>
									<th style="width: 20%;">مسح</th>
								</tr>
							</thead>
							<tbody class="tbody_admin">
								<?php if (isset($categories)): ?>
								<?php foreach ($categories as $category): ?>
									<tr>
										<td><?= $category["id"]; ?></td>
										<td style="text-align: center; vertical-align: middle;">
											<img src="<?= CATEGORIES_ICONS . $category['icon']; ?>" style="width: 70px; float: right;" />
											<?= $category["name_ar"]; ?>
											</td>
										<td style="text-align: center; vertical-align: middle;">
												<?= $category["name_en"]; ?>
										</td>
										<td style="text-align: center; vertical-align: middle;">
											<a href="<?= site_url(); ?>categories/edit/<?= $category['id']; ?>">
												<button class="btn btn-warning btn-font" style="width: 150px;" type="button">تعديل</button>
											</a>
										</td>
										<td style="text-align: center; vertical-align: middle;">
											<a onclick="alertDelete('categories/delete/<?= $category['id']; ?>', 'هل أنت متأكد من مسح هذا القسم؟');" href="javascript:void(null);">
	                                            <button class="btn btn-danger btn-font" style="width: 150px;" type="button" >حذف</button>
											</a>
										</td>
									</tr>
								<?php endforeach; ?>
								<?php endif; ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php //$this->load->view("slide_panel"); ?>
</div><!-- Page Container -->
<?php $this->load->view("footer"); ?>
<script>
	$(document).ready(function() {
		setTimeout(function() {
			$("#status").fadeOut(2000);
		}, 3000);
	});
</script>
</body>
</html>
