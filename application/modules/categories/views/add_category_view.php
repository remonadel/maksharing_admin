<?php $this->load->view("header"); ?>

<div class="container">
	<div class="col-md-12">
		<div class="main-title">
			<h1>إضافة تصنيف لباقات IPTv </h1>
		</div>
	</div>
	<div class="row">
		<div class="masonary-grids">
			<div class="col-md-12">
				<div class="widget-area">
					<div class="wizard-form-h">
						<form action="" method="post" enctype="multipart/form-data">
							<div class="col-md-122" style="margin-bottom: 10px;">
								<?php if (isset($status)) echo $status; ?>
							</div>
							<div class="col-md-122">
								<div class="inline-form">
									<label class="c-label">الإسم باللغة العربية *</label>
									<input type="text" name="name_ar"
										   value="<?php if (isset($_POST["name_ar"])) echo htmlspecialchars(trim($_POST['name_ar'])); ?>" required autofocus />
								</div>
								<div class="inline-form">
									<label class="c-label">الإسم باللغة الإنجليزية*</label>
									<input type="text" name="name_en"
										   value="<?php if (isset($_POST["name_en"])) echo htmlspecialchars(trim($_POST['name_en'])); ?>" required autofocus />
								</div>
							</div>
							<div class="col-md-122">
								<div class="inline-form">
									<label class="c-label">الصورة *</label>
									<input type="file" name="icon" accept="image/*" title="يجب ان تكون الصورة jpg او png" required />
								</div>
							</div>
							<div class="col-md-122" style="margin-top: 12px;">
								<input type="submit" name="submit" value="حفظ" class="btn btn-success btn-font" />
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php //$this->load->view("slide_panel"); ?>
</div><!-- Page Container -->
<?php $this->load->view("footer"); ?>
</body>
</html>
