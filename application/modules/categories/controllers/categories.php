<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Categories extends MX_Controller {

	public function __construct()
    {
        parent::__construct();
		$this->deny->deny_if_logged_out();

		$this->load->model("categories_model");
    }


	public function index()
	{
		$authorized = $this->common_model->authorized_to_view_page("products");
		if ($authorized)
		{
			$data = array();

			$categories = $this->common_model->get_all_from_table("categories");
			if ($categories) $data["categories"] = $categories;

			$this->load->view("manage_categories_view", $data);
		}
	}


	public function add()
	{
		$authorized = $this->common_model->authorized_to_view_page("products");
		if ($authorized)
		{
			$data = array();

			if (isset($_POST["submit"]))
			{
				$name_ar = htmlspecialchars(trim($_POST["name_ar"]));
				$name_en = htmlspecialchars(trim($_POST["name_en"]));
				$image_name = basename($_FILES["icon"]["name"]);

				if (empty($name_en) OR empty($name_ar) OR empty($image_name))
				{
					$data["status"] = "<p class='error-msg'>يجب إدخال جميع البيانات</p>";
				}
				elseif ($this->common_model->subject_exists("categories", "name_en", $name_en))
				{
					$data["status"] = "<p class='error-msg'>هذا القسم موجود بالفعل</p>";
				}
				else
				{
					$tmp_name = $_FILES["icon"]["tmp_name"];
					$allowed_exts = array("jpg", "jpeg", "png");
					$a = explode(".", $image_name);
					$file_ext = strtolower(end($a)); unset($a);
					$path = CATEGORIES_ICONS_PATH;

					// Restricting file uploading to image files only
					if ( ! in_array($file_ext, $allowed_exts))
					{
						$data["status"] = "<p class='error-msg'>لقد قمت برفع ملف غير مسموح به</p>";
					}
					else
					{
						// Success. Give the file a unique name and upload it, then insert data in database
						$cur_time = time();
						$image = $cur_time . "." . $file_ext;
						move_uploaded_file($tmp_name, $path . $image);

						$this->categories_model->insert_category($name_ar, $name_en, $image);

						$this->session->set_flashdata("status", "<p class='success-msg'>تم إضافة القسم</p>");
						redirect(site_url() . "categories");
					}
				}
			}

			$this->load->view("add_category_view", $data);
		}
	}


	public function edit($id = "")
	{
		$authorized = $this->common_model->authorized_to_view_page("products");
		if ($authorized)
		{
			$category = $this->common_model->get_subject_with_token("categories", "id", $id);
			if (empty($id) OR ! $category) redirect(site_url() . "categories");

			$data["category"] = $category;

			if (isset($_POST["submit"]))
			{
				$name_ar = htmlspecialchars(trim($_POST["name_ar"]));
				$name_en = htmlspecialchars(trim($_POST["name_en"]));

				if (empty($name_ar) OR empty($name_en))
				{
					$data["status"] = "<p class='error-msg'>يجب إدخال إسم القسم</p>";
				}
				elseif ($name_en != $category["name_en"] && $this->common_model->subject_exists("categories", "name_en", $name_en))
				{
					$data["status"] = "<p class='error-msg'>هذا القسم موجود بالفعل</p>";
				}
				else
				{
					$success = TRUE;
					$icon_name = $category["icon"];

					if ( ! empty($_FILES["icon"]["name"]))
					{
						$image_name = basename($_FILES["icon"]["name"]);
						$tmp_name = $_FILES["icon"]["tmp_name"];
						$allowed_exts = array("jpg", "jpeg", "png");
						$a = explode(".", $image_name);
						$file_ext = strtolower(end($a)); unset($a);
						$path = CATEGORIES_ICONS_PATH;

						// Restricting file uploading to zip files only
						if ( ! in_array($file_ext, $allowed_exts))
						{
							$success = FALSE;
							$data["status"] = "<p class='error-msg'>لقد قمت برفع ملف غير مسموح به</p>";
						}
						else
						{
							// Delete old image and upload new one
							@unlink($path . $category["icon"]);
							$old_file_name = array_shift(explode(".", $icon_name));
							$icon_name = $old_file_name . "." . $file_ext;
							move_uploaded_file($tmp_name, $path . $icon_name);
						}
					}

					if ($success)
					{
						$this->categories_model->update_category($name_ar, $name_en, $icon_name, $id);

						$this->session->set_flashdata("status", "<p class='success-msg'>تم تعديل القسم</p>");
						redirect(site_url() . "categories");
					}
				}
			}

			$this->load->view("edit_category_view", $data);
		}
	}


	public function delete($id = "")
    {
		$authorized = $this->common_model->authorized_to_view_page("products");
		if ($authorized)
		{
			$category = $this->common_model->get_subject_with_token("categories", "id", $id);
			if (empty($id) OR ! $category) redirect(site_url() . "categories");

			$this->common_model->delete_subject("categories", "id", $id);
			@unlink(CATEGORIES_ICONS_PATH . $category["icon"]);

			$this->session->set_flashdata("status", "<p class='success-msg'>تم مسح القسم</p>");
			redirect(site_url() . "categories");
		}
	}


	public function get_subcategories_of_category()
	{
		/* This function is only accessed via ajax */
		if (isset($_POST["category_id"]))
		{
			$category_id = $_POST["category_id"];
			$subcategories = $this->categories_model->get_subcategories_of_category($category_id);

			echo ($subcategories) ? json_encode($subcategories) : "none";
		}
	}

}


/* End of file categories.php */
/* Location: ./application/modules/categories/controllers/categories.php */
