<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Categories_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }


    public function insert_category($name_ar, $name_en, $icon)
    {
        $sql = "INSERT INTO `categories` (`name_ar`, `name_en`, `icon`) VALUES (?, ?, ?)";
        $query = $this->db->query($sql, array($name_ar, $name_en, $icon));
    }


    public function update_category($name_ar, $name_en, $icon, $id)
    {
        $sql = "UPDATE `categories` SET `name_ar` = ?, `name_en` = ?, `icon` = ? WHERE `id` = ?";
        $query = $this->db->query($sql, array($name_ar, $name_en, $icon, $id));
    }


    public function get_category_details($id)
	{
		$sql = "SELECT * FROM `categories` WHERE `id` = ?";
		$query = $this->db->query($sql, array($id));

		if ($query->num_rows() >= 1)
		{
			$row = $query->row_array();

			$subcategories = $this->common_model->get_all_subjects_with_token("subcategories", "category_id", $id);
            if ($subcategories) $row["subcategories"] = $subcategories;

			return $row;
		}
	}


	public function get_subcategories_of_category($category_id)
    {
        $sql = "SELECT * FROM `subcategories` WHERE `category_id` = ?";
        $query = $this->db->query($sql, array($category_id));

		return ($query->num_rows() >= 1) ? $query->result_array() : FALSE;
    }

}


/* End of file categories_model.php */
/* Location: ./application/modules/categories/models/categories_model.php */
