<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pricing extends MX_Controller {

	public function __construct()
    {
        parent::__construct();
		$this->deny->deny_if_logged_out();

		$this->load->model("pricing_model");
    }


	public function index($id)
	{
		if (!$id OR $id > 2) show_404();
		$authorized = $this->common_model->authorized_to_view_page("products");
		if ($authorized)
		{
			$data = array();

			$packages = $this->pricing_model->get_packages($id);
			if ($packages)
			{
				$data["packages"] = $packages;
			}

			$this->load->view("manage_pricing_view", $data);
		}
	}

	public function add()
	{
        $authorized = $this->common_model->authorized_to_view_page("products");
		if ($authorized)
		{
			$data = array();

			if (isset($_POST["submit"]))
			{
				$name_ar = htmlspecialchars(trim($_POST["name_ar"]));
				$name_en = htmlspecialchars(trim($_POST["name_en"]));
				$content_ar = htmlspecialchars(str_replace(array("<br />", "<br>", "<br/>"), "", $_POST['content_ar']));
				$content_en = htmlspecialchars(str_replace(array("<br />", "<br>", "<br/>"), "", $_POST['content_en']));
				$price = htmlspecialchars(trim($_POST["price"]));
				$currency = htmlspecialchars(trim($_POST["currency"]));
				$period_ar = htmlspecialchars(trim($_POST["period_ar"]));
				$period_en = htmlspecialchars(trim($_POST["period_en"]));
				$url = htmlspecialchars(trim($_POST['url']));
				$type = htmlspecialchars(trim($_POST['type']));

				if (empty($name_ar) OR empty($name_en) OR empty($content_ar) OR empty($content_en))
				{
					$data["status"] = "<p class='error-msg'>يجب إدخال جميع البيانات الإجبارية</p>";
				}
				else
				{
					// Insert information into database
					$this->pricing_model->insert_package($name_ar, $name_en, $content_ar, $content_en, $price
											,$currency,$period_ar, $period_en, $url, $type);

					$this->session->set_flashdata("status", "تمت العملية بنجاح");
					$redirect_url =  site_url() . "pricing/index/" . $type ;
					redirect($redirect_url);
				}
			}

			$this->load->view("add_pricing_view", $data);
		}
	}

	public function edit($id = "")
	{
        $authorized = $this->common_model->authorized_to_view_page("products");
		if ($authorized)
		{
			$package = $this->common_model->get_subject_with_token("pricing", "id", $id);
			if (empty($id) OR ! $package) show_404();

			$data["package"] = $package;

			if (isset($_POST["submit"]))
			{
				$name_ar = htmlspecialchars(trim($_POST["name_ar"]));
				$name_en = htmlspecialchars(trim($_POST["name_en"]));
				$content_ar = htmlspecialchars(str_replace(array("<br />", "<br>", "<br/>"), "", $_POST['content_ar']));
				$content_en = htmlspecialchars(str_replace(array("<br />", "<br>", "<br/>"), "", $_POST['content_en']));
				$price = htmlspecialchars(trim($_POST["price"]));
				$currency = htmlspecialchars(trim($_POST["currency"]));
				$period_ar = htmlspecialchars(trim($_POST["period_ar"]));
				$period_en = htmlspecialchars(trim($_POST["period_en"]));
				$url = htmlspecialchars(trim($_POST['url']));
				$type = htmlspecialchars(trim($_POST['type']));

				if (empty($name_ar) OR empty($name_en) OR empty($content_ar) OR empty($content_en))
				{
					$data["status"] = "<p class='error-msg'>يجب إدخال جميع البيانات الإجبارية</p>";
				}
				else
				{
					// Edit information on database
					$this->pricing_model->edit_package($name_ar, $name_en, $content_ar, $content_en, $price
														,$currency,$period_ar, $period_en, $url, $type, $id);

					$this->session->set_flashdata("status", "تمت العملية بنجاح");
					$redirect_url =  site_url() . "pricing/index/" . $type;
					redirect($redirect_url);
				}
			}

			$this->load->view("edit_pricing_view", $data);
		}
	}


	public function delete($id = "")
    {
		$authorized = $this->common_model->authorized_to_view_page("products");
		if ($authorized)
		{
			$product_exists = $this->common_model->subject_exists("pricing", "id", $id);
			if (empty($id) OR ! $product_exists) show_404();

			$this->common_model->delete_subject("pricing", "id", $id);

			$this->session->set_flashdata("status", "تمت العملية بنجاح");
			redirect($_SERVER['HTTP_REFERER']);
		}
	}

}


/* End of file products.php */
/* Location: ./application/modules/products/controllers/products.php */
