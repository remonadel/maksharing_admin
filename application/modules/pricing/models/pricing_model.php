<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pricing_model extends CI_Model {
    
    public function __construct()
    {
        parent::__construct();
    }

	
    public function insert_package($name_ar, $name_en, $content_ar, $content_en, $price,
											$currency,$period_ar, $period_en, $url, $type)
    {
        $sql = "INSERT INTO `pricing` (`name_ar`, `name_en`, `content_ar`, `content_en`, `price`, `currency`, `period_ar`, `period_en`, `url`,
 										 `type`, `created_at`)
                VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, NOW())";
				
        $query = $this->db->query($sql, array($name_ar, $name_en, $content_ar, $content_en, $price, $currency, $period_ar, $period_en, $url, $type));
    }
	
    
    public function edit_package($name_ar, $name_en, $content_ar, $content_en, $price
									,$currency, $period_ar, $period_en, $url, $type, $id)
    {
			$sql = "UPDATE `pricing` SET `name_ar` = ?, `name_en` = ?, `content_ar` = ?, `content_en` = ?, `price` = ?, `currency` = ?, `period_ar` = ?, `period_en` = ?,
					 `url` = ?, `type` = ? WHERE `id` = ?";
			
			$query = $this->db->query($sql, array($name_ar, $name_en, $content_ar, $content_en, $price, $currency, $period_ar, $period_en, $url, $type, $id));

    }
	

	public function get_packages($type)
	{
		$sql = "SELECT * FROM `pricing` WHERE `type` = ? ORDER BY id DESC";
		$query = $this->db->query($sql, array($type));
		
		return ($query->num_rows() >= 1) ? $query->result_array() : FALSE;
	}
	

}


/* End of file products_model.php */
/* Location: ./application/modules/products/models/products_model.php */