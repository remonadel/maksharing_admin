<?php $this->load->view("header"); ?>

<div class="container">
    <div class="col-md-12" style="margin-bottom: 20px;">
        <div class="col-md-62">
            <div class="main-title">
                <?php if ($this->uri->segment(3) == "1"): ?>
                    <h1>إدارة خطط  CCcam</h1>
                <?php elseif ($this->uri->segment(3) == "2"): ?>
                    <h1>إدارة خطط IPTv </h1>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="masonary-grids">
            <div class="col-md-12">
                <div class="widget-area" style="margin-top: 0;">
                    <?php if ($this->session->flashdata("status")): ?>
                        <div class="col-md-122" id="status" style="background-color: #EEE; padding: 10px; margin-bottom: 25px; margin-top: -10px;"><p class="success-msg"><?= $this->session->flashdata("status"); ?></p></div>
                    <?php endif; ?>

                    <div class="streaming-table" style="margin-top: 0px;">
                        <span id="found" class="label label-info"></span>
                        <table id="stream_table" class='table table-striped table-bordered test'>
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>الباقة</th>
                                    <th>السعر</th>
                                    <th>التحكم</th>
                                </tr>
                            </thead>
                            <tbody class="tbody_admin">
                                <?php if (isset($packages)): ?>
                                <?php foreach ($packages as $package): ?>
                                    <tr class="tr_<?= $package["id"]; ?>">
                                        <td><?= $package["id"]; ?></td>
                                        <td>

                                            <div class="des-sum-arti">
                                                <p class="p-title-14"><?= $package["name_ar"]; ?> - <?= $package["name_en"]; ?></p>
                                                </p>
                                                <div style="width: 660px; height: 20px; overflow: hidden; float: right; display: table;">
                                                    <p class="sub-menus">تاريخ الإدخال: <?= $package["created_at"]; ?></p>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <?= $package["price"]; ?>
                                        </td>
                                        <td>
                                            <a href="<?= site_url(); ?>pricing/edit/<?= $package['id']; ?>">
                                                <button class="btn btn-warning btn-font new-width" type="button">تعديل</button>
                                            </a>
                                            <a onclick="alertDelete('pricing/delete/<?= $package['id']; ?>', 'هل أنت متأكد من حذف هذه الخطة؟');" href="javascript:void(null);">
                                                <button class="btn btn-danger btn-font new-width" type="button" >حذف</button>
                                            </a>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                                <?php endif; ?>
                            </tbody>
                        </table>
                    </div>

                    <?php if (isset($pagination)): ?>
                        <div class="pagination-news">
                            <?= $pagination; ?>
                        </div>
                    <?php endif; ?>

                    <!-- Empty modal -->
                    <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content"></div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
<?php //$this->load->view("slide_panel"); ?>
</div><!-- Page Container -->
<?php $this->load->view("footer"); ?>

</body>
</html>
