<?php $this->load->view("header"); ?>
<div class="container">
    <div class="col-md-12">
        <div class="main-title">
            <h1>تعديل خطة أسعار <?= $package['name_ar'] ?> </h1>
        </div>
    </div>
    <div class="row">
        <div class="masonary-grids">
            <form action="" method="post" id="insert-form">
                <div class="col-md-722">
                    <div class="col-md-122" style="margin-top: -4px;">
                        <div class="widget-area">
                            <?php if (isset($status)): ?>
                                <div class="col-md-122" style="margin-right: 10px;"><?= $status; ?></div>
                            <?php endif; ?>
                            <div class="col-md-122">
                                <div class="inline-form" id="title-div">
                                    <label>إسم الخطة باللغة العربية *</label>
                                    <input class="input-style"  type="text" name="name_ar" value="<?= htmlspecialchars(trim(@$package["name_ar"]));?>" required autofocus />
                                </div>
                                <div class="inline-form" id="title-div">
                                    <label>إسم الخطة باللغة الإنجليزية *</label>
                                    <input class="input-style"  type="text" name="name_en" value="<?= htmlspecialchars(trim(@$package["name_en"]));?>" required autofocus />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-122">
                        <div class="widget-area">
                            <div class="inline-form">
                                <label>الوصف باللغة العربية *</label>
                                <textarea name="content_ar" style="height: 100px;"><?= htmlspecialchars_decode(trim(@$package["content_ar"])); ?></textarea>
                            </div>
                            <div class="inline-form">
                                <label>الوصف باللغة الإنجليزية *</label>
                                <textarea name="content_en" style="height: 100px;"><?= htmlspecialchars_decode(trim(@$package["content_en"])); ?></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-122 clear">
                        <div class="widget-area">
                            <div class="col-md-62">
                                <input type="submit" name="submit" value="حفظ" id="submit_form" class="btn btn-success btn-font" style="width: 100px;" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-5new">
                    <div class="col-md-122">
                        <div class="widget-area">
                            <div class="col-md-122">
                                <div class="inline-form">
                                    <label>السعر *</label>
                                    <input type="number" min="1" name="price" id="price"
                                           value="<?= htmlspecialchars(trim(@$package["price"])); ?>" required />
                                </div>
                                <div class="inline-form">
                                    <label>العملة *</label>
                                    <input type="text"  name="currency"
                                           value="<?= htmlspecialchars(trim(@$package["currency"])); ?>" required />
                                </div>
                                <div class="inline-form">
                                    <label>الفترة باللغة العربية *</label>
                                    <input type="text" name="period_ar"
                                           value="<?= htmlspecialchars(trim(@$package["period_ar"])); ?>" required />
                                </div>
                                <div class="inline-form">
                                    <label>الفترة باللغة الإنجليزية*</label>
                                    <input type="text" name="period_en"
                                           value="<?= htmlspecialchars(trim(@$package["period_en"])); ?>" required />
                                </div>
                                <div class="inline-form">
                                    <label>الرابط </label>
                                    <input type="text" name="url"
                                           value="<?= htmlspecialchars(trim(@$package["url"])); ?>"  />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-122 clear">
                        <div class="widget-area">
                            <div class="col-md-62">
                                <div class="inline-form">
                                    <label class="c-label">النوع *</label>
                                    <select name="type" id="categories" required>
                                        <option value=""></option>

                                        <option value="1"  <?php if ($package['type'] == 1) echo "selected" ?>>CCcam</option>
                                        <option value="2"  <?php if ($package['type'] == 2) echo "selected" ?>>IPTv</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

            </form>
        </div>
    </div>
<?php //$this->load->view("slide_panel"); ?>
</div><!-- Page Container -->
<?php $this->load->view("footer"); ?>
<script>
    tinymce.init({
        selector: "textarea#tinymce-content",
        theme: "modern",
        height: 400,
        directionality: "rtl",
        language: "ar",
        menubar: false,
        content_css: "<?= ASSETS; ?>css/content.css",
        plugins: [
            "preview searchreplace fullscreen wordcount charcount paste link image media"
        ],
        toolbar: "searchreplace | undo redo | bold | bullist numlist | preview | link | image | fullscreen",
        paste_as_text: true,
        invalid_elements : "span,div,br",
        setup: function (ed) {
            $('#add_image_btn').click(function () {
                add_image(ed);
            });
        }
    });
</script>

</body>
</html>
