<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Companies_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }


    public function insert_company($name, $address, $phone, $mobile, $fax, $email, $website, $postal_code, $management_address, $management_phone, $management_fax,
								   $second_branch_address, $second_branch_phone, $second_branch_fax, $third_branch_address, $third_branch_phone,
								   $third_branch_fax, $factory_address, $factory_phone, $factory_fax, $first_official_name, $first_official_title,
								   $first_official_phone, $second_official_name, $second_official_title, $second_official_phone, $third_official_name,
								   $third_official_title, $third_official_phone, $fourth_official_name, $fourth_official_title, $fourth_official_phone,
								   $technical_official_name, $technical_official_phone, $marketing_manager_name, $marketing_manager_phone, $distribution_manager_name,
								   $distribution_manager_phone, $sales_manager_name, $sales_manager_phone, $accounting_manager_name, $accounting_manager_phone,
								   $activity, $services, $products, $other_products, $distributers, $company_type, $number_of_workers, $capital, $year_founded,
								   $previous_works, $important_clients, $selling_points, $commercial_agencies, $company_number)
    {
        $sql = "INSERT INTO `companies` (`name`, `address`, `phone`, `mobile`, `fax`, `email`, `website`, `postal_code`, `management_address`, `management_phone`,
				`management_fax`, `second_branch_address`, `second_branch_phone`, `second_branch_fax`, `third_branch_address`, `third_branch_phone`, `third_branch_fax`,
				`factory_address`, `factory_phone`, `factory_fax`, `first_official_name`, `first_official_title`, `first_official_phone`, `second_official_name`,
				`second_official_title`, `second_official_phone`, `third_official_name`, `third_official_title`, `third_official_phone`, `fourth_official_name`,
				`fourth_official_title`, `fourth_official_phone`, `technical_official_name`, `technical_official_phone`, `marketing_manager_name`, `marketing_manager_phone`,
				`distribution_manager_name`, `distribution_manager_phone`, `sales_manager_name`, `sales_manager_phone`, `accounting_manager_name`, `accounting_manager_phone`,
				`activity`, `services`, `products`, `other_products`, `distributers`, `company_type`, `number_of_workers`, `capital`, `year_founded`, `previous_works`,
				`important_clients`, `selling_points`, `commercial_agencies`, `company_number`)
				VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,
				?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

        $query = $this->db->query($sql, array($name, $address, $phone, $mobile, $fax, $email, $website, $postal_code, $management_address, $management_phone, $management_fax,
											  $second_branch_address, $second_branch_phone, $second_branch_fax, $third_branch_address, $third_branch_phone,
											  $third_branch_fax, $factory_address, $factory_phone, $factory_fax, $first_official_name, $first_official_title,
											  $first_official_phone, $second_official_name, $second_official_title, $second_official_phone, $third_official_name,
											  $third_official_title, $third_official_phone, $fourth_official_name, $fourth_official_title, $fourth_official_phone,
											  $technical_official_name, $technical_official_phone, $marketing_manager_name, $marketing_manager_phone, $distribution_manager_name,
											  $distribution_manager_phone, $sales_manager_name, $sales_manager_phone, $accounting_manager_name, $accounting_manager_phone,
											  $activity, $services, $products, $other_products, $distributers, $company_type, $number_of_workers, $capital, $year_founded,
											  $previous_works, $important_clients, $selling_points, $commercial_agencies, $company_number));

        return $this->db->insert_id();
    }


    public function update_company($name, $address, $phone, $mobile, $fax, $email, $website, $postal_code, $management_address, $management_phone, $management_fax,
								   $second_branch_address, $second_branch_phone, $second_branch_fax, $third_branch_address, $third_branch_phone,
								   $third_branch_fax, $factory_address, $factory_phone, $factory_fax, $first_official_name, $first_official_title,
								   $first_official_phone, $second_official_name, $second_official_title, $second_official_phone, $third_official_name,
								   $third_official_title, $third_official_phone, $fourth_official_name, $fourth_official_title, $fourth_official_phone,
								   $technical_official_name, $technical_official_phone, $marketing_manager_name, $marketing_manager_phone, $distribution_manager_name,
								   $distribution_manager_phone, $sales_manager_name, $sales_manager_phone, $accounting_manager_name, $accounting_manager_phone,
								   $activity, $services, $products, $other_products, $distributers, $company_type, $number_of_workers, $capital, $year_founded,
								   $previous_works, $important_clients, $selling_points, $commercial_agencies, $company_number, $id)
    {
        $sql = "UPDATE `companies` SET `name` = ?, `address` = ?, `phone` = ?, `mobile` = ?, `fax` = ?, `email` = ?, `website` = ?, `postal_code` = ?, `management_address` = ?,
				`management_phone` = ?, `management_fax` = ?, `second_branch_address` = ?, `second_branch_phone` = ?, `second_branch_fax` = ?, `third_branch_address` = ?,
				`third_branch_phone` = ?, `third_branch_fax` = ?, `factory_address` = ?, `factory_phone` = ?, `factory_fax` = ?, `first_official_name` = ?,
				`first_official_title` = ?, `first_official_phone` = ?, `second_official_name` = ?, `second_official_title` = ?, `second_official_phone` = ?,
				`third_official_name` = ?, `third_official_title` = ?, `third_official_phone` = ?, `fourth_official_name` = ?, `fourth_official_title` = ?,
				`fourth_official_phone` = ?, `technical_official_name` = ?, `technical_official_phone` = ?, `marketing_manager_name` = ?, `marketing_manager_phone` = ?,
				`distribution_manager_name` = ?, `distribution_manager_phone` = ?, `sales_manager_name` = ?, `sales_manager_phone` = ?, `accounting_manager_name` = ?,
				`accounting_manager_phone` = ?, `activity` = ?, `services` = ?, `products` = ?, `other_products` = ?, `distributers` = ?, `company_type` = ?,
				`number_of_workers` = ?, `capital` = ?, `year_founded` = ?, `previous_works` = ?, `important_clients` = ?, `selling_points` = ?, `commercial_agencies` = ?,
				`company_number` = ?
				WHERE `id` = ?";

        $query = $this->db->query($sql, array($name, $address, $phone, $mobile, $fax, $email, $website, $postal_code, $management_address, $management_phone, $management_fax,
											  $second_branch_address, $second_branch_phone, $second_branch_fax, $third_branch_address, $third_branch_phone,
											  $third_branch_fax, $factory_address, $factory_phone, $factory_fax, $first_official_name, $first_official_title,
											  $first_official_phone, $second_official_name, $second_official_title, $second_official_phone, $third_official_name,
											  $third_official_title, $third_official_phone, $fourth_official_name, $fourth_official_title, $fourth_official_phone,
											  $technical_official_name, $technical_official_phone, $marketing_manager_name, $marketing_manager_phone, $distribution_manager_name,
											  $distribution_manager_phone, $sales_manager_name, $sales_manager_phone, $accounting_manager_name, $accounting_manager_phone,
											  $activity, $services, $products, $other_products, $distributers, $company_type, $number_of_workers, $capital, $year_founded,
											  $previous_works, $important_clients, $selling_points, $commercial_agencies, $company_number, $id));
    }


	public function get_companies($offset, $limit)
    {
        $sql = "SELECT * FROM `companies` ORDER BY `id` DESC LIMIT ?, ?";
        $query = $this->db->query($sql, array($offset, $limit));

        if ($query->num_rows() >= 1)
            return $query->result_array();
        else
            return FALSE;
    }


    public function get_search_rows_count($query)
    {
        $sql = "SELECT COUNT(`id`) AS `count` FROM `companies` WHERE `name` LIKE '%$query%'";
        $query = $this->db->query($sql);
        $count = $query->row_array();
        return $count["count"];
    }


	public function search_companies($query, $offset, $limit)
    {
        $sql = "SELECT * FROM `companies` WHERE `name` LIKE '%$query%' ORDER BY `id` DESC LIMIT ?, ?";
        $query = $this->db->query($sql, array($offset, $limit));

		return ($query->num_rows() >= 1) ? $query->result_array() : FALSE;
    }

}


/* End of file companies_model.php */
/* Location: ./application/modules/companies/models/companies_model.php */
