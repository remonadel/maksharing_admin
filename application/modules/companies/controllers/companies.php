<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Companies extends MX_Controller {

	public function __construct()
    {
        parent::__construct();
		$this->deny->deny_if_logged_out();

		$this->load->model("companies_model");
    }


	public function index()
	{
		$authorized = $this->common_model->authorized_to_view_page("companies");
		if ($authorized)
		{
			$data = array();

			$current_page = (int) $this->uri->segment(2);
			$per_page = 20;
			$companies_count = $this->common_model->get_table_rows_count("companies");

			$config["base_url"] = site_url() . "companies/";
			$config['uri_segment'] = 2;
			$config["total_rows"] = $companies_count;
			$config["per_page"] = $per_page;
			$this->pagination->initialize($config);

			$companies = $this->companies_model->get_companies($current_page, $per_page);
			if ($companies)
			{
				$data["companies"] = $companies;
				$data["pagination"] = $this->pagination->create_links();
			}

			if (isset($_POST["search"]))
			{
				$query = htmlspecialchars(trim($_POST["search"]));
				redirect(site_url() . "companies/search/$query");
			}

			$this->load->view("manage_companies_view", $data);
		}
	}


	public function search($query)
	{
		$authorized = $this->common_model->authorized_to_view_page("companies");
		if ($authorized)
		{
			if (empty($query)) redirect(site_url() . "companies/unpublished");
			$query = urldecode($query);

			$data = array();

			$current_page = (int) $this->uri->segment(4);
			$per_page = 20;
			$companies_count = $this->companies_model->get_search_rows_count($query);

			$config["base_url"] = site_url() . "companies/search/$query";
			$config['uri_segment'] = 4;
			$config["total_rows"] = $companies_count;
			$config["per_page"] = $per_page;
			$this->pagination->initialize($config);

			$companies = $this->companies_model->search_companies($query, $current_page, $per_page);
			if ($companies)
			{
				$data["companies"] = $companies;
				$data["pagination"] = $this->pagination->create_links();
			}

			if (isset($_POST["search"]))
			{
				$query = htmlspecialchars(trim($_POST["search"]));
				redirect(site_url() . "companies/search/$query");
			}

			$this->load->view("manage_companies_view", $data);
		}
	}


	public function add()
	{
		$authorized = $this->common_model->authorized_to_view_page("companies");
		if ($authorized)
		{
			$data = array();

			if (isset($_POST["submit"]))
			{
				$name = htmlspecialchars(trim($_POST["name"]));
				$address = (empty($_POST["address"])) ? NULL : htmlspecialchars(trim($_POST["address"]));
				$phone = (empty($_POST["phone"])) ? NULL : htmlspecialchars(trim($_POST["phone"]));
				$mobile = (empty($_POST["mobile"])) ? NULL : htmlspecialchars(trim($_POST["mobile"]));
				$fax = (empty($_POST["fax"])) ? NULL : htmlspecialchars(trim($_POST["fax"]));
				$email = (empty($_POST["email"])) ? NULL : htmlspecialchars(trim($_POST["email"]));
				$website = (empty($_POST["website"])) ? NULL : htmlspecialchars(trim($_POST["website"]));
				$postal_code = (empty($_POST["postal_code"])) ? NULL : htmlspecialchars(trim($_POST["postal_code"]));
				$management_address = (empty($_POST["management_address"])) ? NULL : htmlspecialchars(trim($_POST["management_address"]));
				$management_phone = (empty($_POST["management_phone"])) ? NULL : htmlspecialchars(trim($_POST["management_phone"]));
				$management_fax = (empty($_POST["management_fax"])) ? NULL : htmlspecialchars(trim($_POST["management_fax"]));
				$second_branch_address = (empty($_POST["second_branch_address"])) ? NULL : htmlspecialchars(trim($_POST["second_branch_address"]));
				$second_branch_phone = (empty($_POST["second_branch_phone"])) ? NULL : htmlspecialchars(trim($_POST["second_branch_phone"]));
				$second_branch_fax = (empty($_POST["second_branch_fax"])) ? NULL : htmlspecialchars(trim($_POST["second_branch_fax"]));
				$third_branch_address = (empty($_POST["third_branch_address"])) ? NULL : htmlspecialchars(trim($_POST["third_branch_address"]));
				$third_branch_phone = (empty($_POST["third_branch_phone"])) ? NULL : htmlspecialchars(trim($_POST["third_branch_phone"]));
				$third_branch_fax = (empty($_POST["third_branch_fax"])) ? NULL : htmlspecialchars(trim($_POST["third_branch_fax"]));
				$factory_address = (empty($_POST["factory_address"])) ? NULL : htmlspecialchars(trim($_POST["factory_address"]));
				$factory_phone = (empty($_POST["factory_phone"])) ? NULL : htmlspecialchars(trim($_POST["factory_phone"]));
				$factory_fax = (empty($_POST["factory_fax"])) ? NULL : htmlspecialchars(trim($_POST["factory_fax"]));
				$first_official_name = (empty($_POST["first_official_name"])) ? NULL : htmlspecialchars(trim($_POST["first_official_name"]));
				$first_official_title = (empty($_POST["first_official_title"])) ? NULL : htmlspecialchars(trim($_POST["first_official_title"]));
				$first_official_phone = (empty($_POST["first_official_phone"])) ? NULL : htmlspecialchars(trim($_POST["first_official_phone"]));
				$second_official_name = (empty($_POST["second_official_name"])) ? NULL : htmlspecialchars(trim($_POST["second_official_name"]));
				$second_official_title = (empty($_POST["second_official_title"])) ? NULL : htmlspecialchars(trim($_POST["second_official_title"]));
				$second_official_phone = (empty($_POST["second_official_phone"])) ? NULL : htmlspecialchars(trim($_POST["second_official_phone"]));
				$third_official_name = (empty($_POST["third_official_name"])) ? NULL : htmlspecialchars(trim($_POST["third_official_name"]));
				$third_official_title = (empty($_POST["third_official_title"])) ? NULL : htmlspecialchars(trim($_POST["third_official_title"]));
				$third_official_phone = (empty($_POST["third_official_phone"])) ? NULL : htmlspecialchars(trim($_POST["third_official_phone"]));
				$fourth_official_name = (empty($_POST["fourth_official_name"])) ? NULL : htmlspecialchars(trim($_POST["fourth_official_name"]));
				$fourth_official_title = (empty($_POST["fourth_official_title"])) ? NULL : htmlspecialchars(trim($_POST["fourth_official_title"]));
				$fourth_official_phone = (empty($_POST["fourth_official_phone"])) ? NULL : htmlspecialchars(trim($_POST["fourth_official_phone"]));
				$technical_official_name = (empty($_POST["technical_official_name"])) ? NULL : htmlspecialchars(trim($_POST["technical_official_name"]));
				$technical_official_phone = (empty($_POST["technical_official_phone"])) ? NULL : htmlspecialchars(trim($_POST["technical_official_phone"]));
				$marketing_manager_name = (empty($_POST["marketing_manager_name"])) ? NULL : htmlspecialchars(trim($_POST["marketing_manager_name"]));
				$marketing_manager_phone = (empty($_POST["marketing_manager_phone"])) ? NULL : htmlspecialchars(trim($_POST["marketing_manager_phone"]));
				$distribution_manager_name = (empty($_POST["distribution_manager_name"])) ? NULL : htmlspecialchars(trim($_POST["distribution_manager_name"]));
				$distribution_manager_phone = (empty($_POST["distribution_manager_phone"])) ? NULL : htmlspecialchars(trim($_POST["distribution_manager_phone"]));
				$sales_manager_name = (empty($_POST["sales_manager_name"])) ? NULL : htmlspecialchars(trim($_POST["sales_manager_name"]));
				$sales_manager_phone = (empty($_POST["sales_manager_phone"])) ? NULL : htmlspecialchars(trim($_POST["sales_manager_phone"]));
				$accounting_manager_name = (empty($_POST["accounting_manager_name"])) ? NULL : htmlspecialchars(trim($_POST["accounting_manager_name"]));
				$accounting_manager_phone = (empty($_POST["accounting_manager_phone"])) ? NULL : htmlspecialchars(trim($_POST["accounting_manager_phone"]));
				$activity = (empty($_POST["activity"])) ? NULL : htmlspecialchars(trim($_POST["activity"]));
				$services = (empty($_POST["services"])) ? NULL : htmlspecialchars(trim($_POST["services"]));
				$products = (empty($_POST["products"])) ? NULL : htmlspecialchars(trim($_POST["products"]));
				$other_products = (empty($_POST["other_products"])) ? NULL : htmlspecialchars(trim($_POST["other_products"]));
				$distributers = (empty($_POST["distributers"])) ? NULL : htmlspecialchars(trim($_POST["distributers"]));
				$company_type = (empty($_POST["company_type"])) ? NULL : htmlspecialchars(trim($_POST["company_type"]));
				$number_of_workers = (empty($_POST["number_of_workers"])) ? NULL : htmlspecialchars(trim($_POST["number_of_workers"]));
				$capital = (empty($_POST["capital"])) ? NULL : htmlspecialchars(trim($_POST["capital"]));
				$year_founded = (empty($_POST["year_founded"])) ? NULL : htmlspecialchars(trim($_POST["year_founded"]));
				$previous_works = (empty($_POST["previous_works"])) ? NULL : htmlspecialchars(trim($_POST["previous_works"]));
				$important_clients = (empty($_POST["important_clients"])) ? NULL : htmlspecialchars(trim($_POST["important_clients"]));
				$selling_points = (empty($_POST["selling_points"])) ? NULL : htmlspecialchars(trim($_POST["selling_points"]));
				$commercial_agencies = (empty($_POST["commercial_agencies"])) ? NULL : htmlspecialchars(trim($_POST["commercial_agencies"]));
				$company_number = (empty($_POST["company_number"])) ? NULL : htmlspecialchars(trim($_POST["company_number"]));

				if (empty($name))
				{
					$data["status"] = "<p class='error-msg'>يجب إدخال إسم الشركة</p>";
				}
				elseif ($this->common_model->subject_exists("companies", "name", $name))
				{
					$data["status"] = "<p class='error-msg'>هذا الشركة موجودة بالفعل</p>";
				}
				else
				{
					$insert_id = $this->companies_model->insert_company($name, $address, $phone, $mobile, $fax, $email, $website, $postal_code, $management_address,
																		$management_phone, $management_fax, $second_branch_address, $second_branch_phone, $second_branch_fax,
																		$third_branch_address, $third_branch_phone, $third_branch_fax, $factory_address, $factory_phone,
																		$factory_fax, $first_official_name, $first_official_title, $first_official_phone, $second_official_name,
																		$second_official_title, $second_official_phone, $third_official_name, $third_official_title,
																		$third_official_phone, $fourth_official_name, $fourth_official_title, $fourth_official_phone,
																		$technical_official_name, $technical_official_phone, $marketing_manager_name, $marketing_manager_phone,
																		$distribution_manager_name, $distribution_manager_phone, $sales_manager_name, $sales_manager_phone,
																		$accounting_manager_name, $accounting_manager_phone, $activity, $services, $products, $other_products,
																		$distributers, $company_type, $number_of_workers, $capital, $year_founded, $previous_works,
																		$important_clients, $selling_points, $commercial_agencies, $company_number);

					$this->session->set_flashdata("status", "<p class='success-msg'>تم إضافة الشركة</p>");
					redirect(site_url() . "companies");
				}
			}

			$this->load->view("add_company_view", $data);
		}
	}


	public function edit($id = "")
	{
		$authorized = $this->common_model->authorized_to_view_page("companies");
		if ($authorized)
		{
			$company = $this->common_model->get_subject_with_token("companies", "id", $id);
			if (empty($id) OR ! $company) redirect(site_url() . "companies");

			$data["company"] = $company;

			if (isset($_POST["submit"]))
			{
				$name = htmlspecialchars(trim($_POST["name"]));
				$address = (empty($_POST["address"])) ? NULL : htmlspecialchars(trim($_POST["address"]));
				$phone = (empty($_POST["phone"])) ? NULL : htmlspecialchars(trim($_POST["phone"]));
				$mobile = (empty($_POST["mobile"])) ? NULL : htmlspecialchars(trim($_POST["mobile"]));
				$fax = (empty($_POST["fax"])) ? NULL : htmlspecialchars(trim($_POST["fax"]));
				$email = (empty($_POST["email"])) ? NULL : htmlspecialchars(trim($_POST["email"]));
				$website = (empty($_POST["website"])) ? NULL : htmlspecialchars(trim($_POST["website"]));
				$postal_code = (empty($_POST["postal_code"])) ? NULL : htmlspecialchars(trim($_POST["postal_code"]));
				$management_address = (empty($_POST["management_address"])) ? NULL : htmlspecialchars(trim($_POST["management_address"]));
				$management_phone = (empty($_POST["management_phone"])) ? NULL : htmlspecialchars(trim($_POST["management_phone"]));
				$management_fax = (empty($_POST["management_fax"])) ? NULL : htmlspecialchars(trim($_POST["management_fax"]));
				$second_branch_address = (empty($_POST["second_branch_address"])) ? NULL : htmlspecialchars(trim($_POST["second_branch_address"]));
				$second_branch_phone = (empty($_POST["second_branch_phone"])) ? NULL : htmlspecialchars(trim($_POST["second_branch_phone"]));
				$second_branch_fax = (empty($_POST["second_branch_fax"])) ? NULL : htmlspecialchars(trim($_POST["second_branch_fax"]));
				$third_branch_address = (empty($_POST["third_branch_address"])) ? NULL : htmlspecialchars(trim($_POST["third_branch_address"]));
				$third_branch_phone = (empty($_POST["third_branch_phone"])) ? NULL : htmlspecialchars(trim($_POST["third_branch_phone"]));
				$third_branch_fax = (empty($_POST["third_branch_fax"])) ? NULL : htmlspecialchars(trim($_POST["third_branch_fax"]));
				$factory_address = (empty($_POST["factory_address"])) ? NULL : htmlspecialchars(trim($_POST["factory_address"]));
				$factory_phone = (empty($_POST["factory_phone"])) ? NULL : htmlspecialchars(trim($_POST["factory_phone"]));
				$factory_fax = (empty($_POST["factory_fax"])) ? NULL : htmlspecialchars(trim($_POST["factory_fax"]));
				$first_official_name = (empty($_POST["first_official_name"])) ? NULL : htmlspecialchars(trim($_POST["first_official_name"]));
				$first_official_title = (empty($_POST["first_official_title"])) ? NULL : htmlspecialchars(trim($_POST["first_official_title"]));
				$first_official_phone = (empty($_POST["first_official_phone"])) ? NULL : htmlspecialchars(trim($_POST["first_official_phone"]));
				$second_official_name = (empty($_POST["second_official_name"])) ? NULL : htmlspecialchars(trim($_POST["second_official_name"]));
				$second_official_title = (empty($_POST["second_official_title"])) ? NULL : htmlspecialchars(trim($_POST["second_official_title"]));
				$second_official_phone = (empty($_POST["second_official_phone"])) ? NULL : htmlspecialchars(trim($_POST["second_official_phone"]));
				$third_official_name = (empty($_POST["third_official_name"])) ? NULL : htmlspecialchars(trim($_POST["third_official_name"]));
				$third_official_title = (empty($_POST["third_official_title"])) ? NULL : htmlspecialchars(trim($_POST["third_official_title"]));
				$third_official_phone = (empty($_POST["third_official_phone"])) ? NULL : htmlspecialchars(trim($_POST["third_official_phone"]));
				$fourth_official_name = (empty($_POST["fourth_official_name"])) ? NULL : htmlspecialchars(trim($_POST["fourth_official_name"]));
				$fourth_official_title = (empty($_POST["fourth_official_title"])) ? NULL : htmlspecialchars(trim($_POST["fourth_official_title"]));
				$fourth_official_phone = (empty($_POST["fourth_official_phone"])) ? NULL : htmlspecialchars(trim($_POST["fourth_official_phone"]));
				$technical_official_name = (empty($_POST["technical_official_name"])) ? NULL : htmlspecialchars(trim($_POST["technical_official_name"]));
				$technical_official_phone = (empty($_POST["technical_official_phone"])) ? NULL : htmlspecialchars(trim($_POST["technical_official_phone"]));
				$marketing_manager_name = (empty($_POST["marketing_manager_name"])) ? NULL : htmlspecialchars(trim($_POST["marketing_manager_name"]));
				$marketing_manager_phone = (empty($_POST["marketing_manager_phone"])) ? NULL : htmlspecialchars(trim($_POST["marketing_manager_phone"]));
				$distribution_manager_name = (empty($_POST["distribution_manager_name"])) ? NULL : htmlspecialchars(trim($_POST["distribution_manager_name"]));
				$distribution_manager_phone = (empty($_POST["distribution_manager_phone"])) ? NULL : htmlspecialchars(trim($_POST["distribution_manager_phone"]));
				$sales_manager_name = (empty($_POST["sales_manager_name"])) ? NULL : htmlspecialchars(trim($_POST["sales_manager_name"]));
				$sales_manager_phone = (empty($_POST["sales_manager_phone"])) ? NULL : htmlspecialchars(trim($_POST["sales_manager_phone"]));
				$accounting_manager_name = (empty($_POST["accounting_manager_name"])) ? NULL : htmlspecialchars(trim($_POST["accounting_manager_name"]));
				$accounting_manager_phone = (empty($_POST["accounting_manager_phone"])) ? NULL : htmlspecialchars(trim($_POST["accounting_manager_phone"]));
				$activity = (empty($_POST["activity"])) ? NULL : htmlspecialchars(trim($_POST["activity"]));
				$services = (empty($_POST["services"])) ? NULL : htmlspecialchars(trim($_POST["services"]));
				$products = (empty($_POST["products"])) ? NULL : htmlspecialchars(trim($_POST["products"]));
				$other_products = (empty($_POST["other_products"])) ? NULL : htmlspecialchars(trim($_POST["other_products"]));
				$distributers = (empty($_POST["distributers"])) ? NULL : htmlspecialchars(trim($_POST["distributers"]));
				$company_type = (empty($_POST["company_type"])) ? NULL : htmlspecialchars(trim($_POST["company_type"]));
				$number_of_workers = (empty($_POST["number_of_workers"])) ? NULL : htmlspecialchars(trim($_POST["number_of_workers"]));
				$capital = (empty($_POST["capital"])) ? NULL : htmlspecialchars(trim($_POST["capital"]));
				$year_founded = (empty($_POST["year_founded"])) ? NULL : htmlspecialchars(trim($_POST["year_founded"]));
				$previous_works = (empty($_POST["previous_works"])) ? NULL : htmlspecialchars(trim($_POST["previous_works"]));
				$important_clients = (empty($_POST["important_clients"])) ? NULL : htmlspecialchars(trim($_POST["important_clients"]));
				$selling_points = (empty($_POST["selling_points"])) ? NULL : htmlspecialchars(trim($_POST["selling_points"]));
				$commercial_agencies = (empty($_POST["commercial_agencies"])) ? NULL : htmlspecialchars(trim($_POST["commercial_agencies"]));
				$company_number = (empty($_POST["company_number"])) ? NULL : htmlspecialchars(trim($_POST["company_number"]));

				if (empty($name))
				{
					$data["status"] = "<p class='error-msg'>يجب إدخال إسم الشركة</p>";
				}
				elseif ($name != $company["name"] && $this->common_model->subject_exists("companies", "name", $name))
				{
					$data["status"] = "<p class='error-msg'>هذا الشركة موجودة بالفعل</p>";
				}
				else
				{
					$this->companies_model->update_company($name, $address, $phone, $mobile, $fax, $email, $website, $postal_code, $management_address,
														   $management_phone, $management_fax, $second_branch_address, $second_branch_phone, $second_branch_fax,
														   $third_branch_address, $third_branch_phone, $third_branch_fax, $factory_address, $factory_phone,
														   $factory_fax, $first_official_name, $first_official_title, $first_official_phone, $second_official_name,
														   $second_official_title, $second_official_phone, $third_official_name, $third_official_title,
														   $third_official_phone, $fourth_official_name, $fourth_official_title, $fourth_official_phone,
														   $technical_official_name, $technical_official_phone, $marketing_manager_name, $marketing_manager_phone,
														   $distribution_manager_name, $distribution_manager_phone, $sales_manager_name, $sales_manager_phone,
														   $accounting_manager_name, $accounting_manager_phone, $activity, $services, $products, $other_products,
														   $distributers, $company_type, $number_of_workers, $capital, $year_founded, $previous_works,
														   $important_clients, $selling_points, $commercial_agencies, $company_number, $id);

					$this->session->set_flashdata("status", "<p class='success-msg'>تم تعديل الشركة</p>");
					redirect(site_url() . "companies");
				}
			}

			$this->load->view("edit_company_view", $data);
		}
	}


	public function delete($id = "")
    {
		$authorized = $this->common_model->authorized_to_view_page("companies");
		if ($authorized)
		{
			$company = $this->common_model->get_subject_with_token("companies", "id", $id);
			if (empty($id) OR ! $company) redirect(site_url() . "companies");

			$this->common_model->delete_subject("companies", "id", $id);

			$this->session->set_flashdata("status", "<p class='success-msg'>تم مسح الشركة</p>");
			redirect(site_url() . "companies");
		}
	}

}


/* End of file companies.php */
/* Location: ./application/modules/companies/controllers/companies.php */
