<?php $this->load->view("header"); ?>

<div class="container">
	<div class="col-md-12">
		<div class="main-title">
			<h1>تعديل شركة</h1>
		</div>
	</div>
	<div class="row">
		<div class="masonary-grids">
			<div class="col-md-12">
				<div class="widget-area">
					<div class="wizard-form-h">
						<form action="" method="post">
							<div class="col-md-122" style="margin-bottom: 10px;">
								<?php if (isset($status)) echo $status; ?>
							</div>
							<div class="col-md-122">
								<div class="inline-form">
									<label class="c-label">الإسم</label>
									<input type="text" name="name"
										   value="<?= $company['name']; ?>" required autofocus />
								</div>
							</div>
							<div class="col-md-122">
								<div class="inline-form">
									<label class="c-label">العنوان</label>
									<input type="text" name="address"
										   value="<?= $company['address']; ?>" />
								</div>
							</div>
							<div class="col-md-62">
								<div class="inline-form">
									<label class="c-label">التليفون</label>
									<input type="text" name="phone"
										   value="<?= $company['phone']; ?>" />
								</div>
							</div>
							<div class="col-md-62">
								<div class="inline-form">
									<label class="c-label">الموبايل</label>
									<input type="text" name="mobile"
										   value="<?= $company['mobile']; ?>" />
								</div>
							</div>
							<div class="col-md-62">
								<div class="inline-form">
									<label class="c-label">الفاكس</label>
									<input type="text" name="fax"
										   value="<?= $company['fax']; ?>" />
								</div>
							</div>
							<div class="col-md-62">
								<div class="inline-form">
									<label class="c-label">البريد الإلكتروني</label>
									<input type="text" name="email"
										   value="<?= $company['email']; ?>" />
								</div>
							</div>
							<div class="col-md-62">
								<div class="inline-form">
									<label class="c-label">الموقع الإلكتروني</label>
									<input type="text" name="website"
										   value="<?= $company['website']; ?>" />
								</div>
							</div>
							<div class="col-md-62">
								<div class="inline-form">
									<label class="c-label">الرمز البريدي</label>
									<input type="text" name="postal_code"
										   value="<?= $company['postal_code']; ?>" />
								</div>
							</div>
							<div class="col-md-122">
								<div class="inline-form">
									<label class="c-label">عنوان الإدارة</label>
									<input type="text" name="management_address"
										   value="<?= $company['management_address']; ?>" />
								</div>
							</div>
							<div class="col-md-62">
								<div class="inline-form">
									<label class="c-label">تليفون الإدارة</label>
									<input type="text" name="management_phone"
										   value="<?= $company['management_phone']; ?>" />
								</div>
							</div>
							<div class="col-md-62">
								<div class="inline-form">
									<label class="c-label">فاكس الإدارة</label>
									<input type="text" name="management_fax"
										   value="<?= $company['management_fax']; ?>" />
								</div>
							</div>
							<div class="col-md-122">
								<div class="inline-form">
									<label class="c-label">عنوان الفرع الثاني</label>
									<input type="text" name="second_branch_address"
										   value="<?= $company['second_branch_address']; ?>" />
								</div>
							</div>
							<div class="col-md-62">
								<div class="inline-form">
									<label class="c-label">تليفون الفرع الثاني</label>
									<input type="text" name="second_branch_phone"
										   value="<?= $company['second_branch_phone']; ?>" />
								</div>
							</div>
							<div class="col-md-62">
								<div class="inline-form">
									<label class="c-label">فاكس الفرع الثاني</label>
									<input type="text" name="second_branch_fax"
										   value="<?= $company['second_branch_fax']; ?>" />
								</div>
							</div>
							<div class="col-md-122">
								<div class="inline-form">
									<label class="c-label">عنوان الفرع الثالث</label>
									<input type="text" name="third_branch_address"
										   value="<?= $company['third_branch_address']; ?>" />
								</div>
							</div>
							<div class="col-md-62">
								<div class="inline-form">
									<label class="c-label">تليفون الفرع الثالث</label>
									<input type="text" name="third_branch_phone"
										   value="<?= $company['third_branch_phone']; ?>" />
								</div>
							</div>
							<div class="col-md-62">
								<div class="inline-form">
									<label class="c-label">فاكس الفرع الثالث</label>
									<input type="text" name="third_branch_fax"
										   value="<?= $company['third_branch_fax']; ?>" />
								</div>
							</div>
							<div class="col-md-122">
								<div class="inline-form">
									<label class="c-label">عنوان المصنع</label>
									<input type="text" name="factory_address"
										   value="<?= $company['factory_address']; ?>" />
								</div>
							</div>
							<div class="col-md-62">
								<div class="inline-form">
									<label class="c-label">تليفون المصنع</label>
									<input type="text" name="factory_phone"
										   value="<?= $company['factory_phone']; ?>" />
								</div>
							</div>
							<div class="col-md-62">
								<div class="inline-form">
									<label class="c-label">فاكس المصنع</label>
									<input type="text" name="factory_fax"
										   value="<?= $company['factory_fax']; ?>" />
								</div>
							</div>
							<div class="col-md-122">
								<div class="inline-form">
									<label class="c-label">إسم المسئول الأول</label>
									<input type="text" name="first_official_name"
										   value="<?= $company['first_official_name']; ?>" />
								</div>
							</div>
							<div class="col-md-62">
								<div class="inline-form">
									<label class="c-label">وظيفة المسئول الأول</label>
									<input type="text" name="first_official_title"
										   value="<?= $company['first_official_title']; ?>" />
								</div>
							</div>
							<div class="col-md-62">
								<div class="inline-form">
									<label class="c-label">تليفون المسئول الأول</label>
									<input type="text" name="first_official_phone"
										   value="<?= $company['first_official_phone']; ?>" />
								</div>
							</div>
							<div class="col-md-122">
								<div class="inline-form">
									<label class="c-label">إسم المسئول الثاني</label>
									<input type="text" name="second_official_name"
										   value="<?= $company['second_official_name']; ?>" />
								</div>
							</div>
							<div class="col-md-62">
								<div class="inline-form">
									<label class="c-label">وظيفة المسئول الثاني</label>
									<input type="text" name="second_official_title"
										   value="<?= $company['second_official_title']; ?>" />
								</div>
							</div>
							<div class="col-md-62">
								<div class="inline-form">
									<label class="c-label">تليفون المسئول الثاني</label>
									<input type="text" name="second_official_phone"
										   value="<?= $company['second_official_phone']; ?>" />
								</div>
							</div>
							<div class="col-md-122">
								<div class="inline-form">
									<label class="c-label">إسم المسئول الثالث</label>
									<input type="text" name="third_official_name"
										   value="<?= $company['third_official_name']; ?>" />
								</div>
							</div>
							<div class="col-md-62">
								<div class="inline-form">
									<label class="c-label">وظيفة المسئول الثالث</label>
									<input type="text" name="third_official_title"
										   value="<?= $company['third_official_title']; ?>" />
								</div>
							</div>
							<div class="col-md-62">
								<div class="inline-form">
									<label class="c-label">تليفون المسئول الثالث</label>
									<input type="text" name="third_official_phone"
										   value="<?= $company['third_official_phone']; ?>" />
								</div>
							</div>
							<div class="col-md-122">
								<div class="inline-form">
									<label class="c-label">إسم المسئول الرابع</label>
									<input type="text" name="fourth_official_name"
										   value="<?= $company['fourth_official_name']; ?>" />
								</div>
							</div>
							<div class="col-md-62">
								<div class="inline-form">
									<label class="c-label">وظيفة المسئول الرابع</label>
									<input type="text" name="fourth_official_title"
										   value="<?= $company['fourth_official_title']; ?>" />
								</div>
							</div>
							<div class="col-md-62">
								<div class="inline-form">
									<label class="c-label">تليفون المسئول الرابع</label>
									<input type="text" name="fourth_official_phone"
										   value="<?= $company['fourth_official_phone']; ?>" />
								</div>
							</div>
							<div class="col-md-62">
								<div class="inline-form">
									<label class="c-label">إسم مسئول المكتب الفني</label>
									<input type="text" name="technical_official_name"
										   value="<?= $company['technical_official_name']; ?>" />
								</div>
							</div>
							<div class="col-md-62">
								<div class="inline-form">
									<label class="c-label">تليفون مسئول المكتب الفني</label>
									<input type="text" name="technical_official_phone"
										   value="<?= $company['technical_official_phone']; ?>" />
								</div>
							</div>
							<div class="col-md-62">
								<div class="inline-form">
									<label class="c-label">إسم مدير التسويق</label>
									<input type="text" name="marketing_manager_name"
										   value="<?= $company['marketing_manager_name']; ?>" />
								</div>
							</div>
							<div class="col-md-62">
								<div class="inline-form">
									<label class="c-label">تليفون مدير التسويق</label>
									<input type="text" name="marketing_manager_phone"
										   value="<?= $company['marketing_manager_phone']; ?>" />
								</div>
							</div>
							<div class="col-md-62">
								<div class="inline-form">
									<label class="c-label">إسم مدير التوزيع</label>
									<input type="text" name="distribution_manager_name"
										   value="<?= $company['distribution_manager_name']; ?>" />
								</div>
							</div>
							<div class="col-md-62">
								<div class="inline-form">
									<label class="c-label">تليفون مدير التوزيع</label>
									<input type="text" name="distribution_manager_phone"
										   value="<?= $company['distribution_manager_phone']; ?>" />
								</div>
							</div>
							<div class="col-md-62">
								<div class="inline-form">
									<label class="c-label">إسم مدير المبيعات</label>
									<input type="text" name="sales_manager_name"
										   value="<?= $company['sales_manager_name']; ?>" />
								</div>
							</div>
							<div class="col-md-62">
								<div class="inline-form">
									<label class="c-label">تليفون مدير المبيعات</label>
									<input type="text" name="sales_manager_phone"
										   value="<?= $company['sales_manager_phone']; ?>" />
								</div>
							</div>
							<div class="col-md-62">
								<div class="inline-form">
									<label class="c-label">إسم مدير الحسابات</label>
									<input type="text" name="accounting_manager_name"
										   value="<?= $company['accounting_manager_name']; ?>" />
								</div>
							</div>
							<div class="col-md-62">
								<div class="inline-form">
									<label class="c-label">تليفون مدير الحسابات</label>
									<input type="text" name="accounting_manager_phone"
										   value="<?= $company['accounting_manager_phone']; ?>" />
								</div>
							</div>
							<div class="col-md-122">
								<div class="inline-form">
									<label class="c-label">النشاط</label>
									<textarea name="activity" style="height: 80px;"><?= $company['activity']; ?></textarea>
								</div>
							</div>
							<div class="col-md-122">
								<div class="inline-form">
									<label class="c-label">الخدمات</label>
									<textarea name="services" style="height: 80px;"><?= $company['services']; ?></textarea>
								</div>
							</div>
							<div class="col-md-122">
								<div class="inline-form">
									<label class="c-label">المنتجات</label>
									<textarea name="products" style="height: 80px;"><?= $company['products']; ?></textarea>
								</div>
							</div>
							<div class="col-md-122">
								<div class="inline-form">
									<label class="c-label">منتجات أخري</label>
									<textarea name="other_products" style="height: 80px;"><?= $company['other_products']; ?></textarea>
								</div>
							</div>
							<div class="col-md-122">
								<div class="inline-form">
									<label class="c-label">الموزعين</label>
									<textarea name="distributers" style="height: 80px;"><?= $company['distributers']; ?></textarea>
								</div>
							</div>
							<div class="col-md-62">
								<div class="inline-form">
									<label class="c-label">نوع الشركة</label>
									<input type="text" name="company_type"
										   value="<?= $company['company_type']; ?>" />
								</div>
							</div>
							<div class="col-md-62">
								<div class="inline-form">
									<label class="c-label">عدد العاملين</label>
									<input type="text" name="number_of_workers"
										   value="<?= $company['number_of_workers']; ?>" />
								</div>
							</div>
							<div class="col-md-62">
								<div class="inline-form">
									<label class="c-label">رأس المال</label>
									<input type="text" name="capital"
										   value="<?= $company['capital']; ?>" />
								</div>
							</div>
							<div class="col-md-62">
								<div class="inline-form">
									<label class="c-label">سنة التأسيس</label>
									<input type="text" name="year_founded"
										   value="<?= $company['year_founded']; ?>" />
								</div>
							</div>
							<div class="col-md-122">
								<div class="inline-form">
									<label class="c-label">سابقة الأعمال</label>
									<textarea name="previous_works" style="height: 80px;"><?= $company['previous_works']; ?></textarea>
								</div>
							</div>
							<div class="col-md-122">
								<div class="inline-form">
									<label class="c-label">أهم العملاء</label>
									<textarea name="important_clients" style="height: 80px;"><?= $company['important_clients']; ?></textarea>
								</div>
							</div>
							<div class="col-md-122">
								<div class="inline-form">
									<label class="c-label">نقاط البيع</label>
									<textarea name="selling_points" style="height: 80px;"><?= $company['selling_points']; ?></textarea>
								</div>
							</div>
							<div class="col-md-122">
								<div class="inline-form">
									<label class="c-label">توكيلات تجارية</label>
									<textarea name="commercial_agencies" style="height: 80px;"><?= $company['commercial_agencies']; ?></textarea>
								</div>
							</div>
							<div class="col-md-122">
								<div class="inline-form">
									<label class="c-label">رقم الشركة</label>
									<input type="text" name="company_number"
										   value="<?= $company['company_number']; ?>" />
								</div>
							</div>
							<div class="col-md-122" style="margin-top: 20px;">
								<input type="submit" name="submit" value="حفظ" class="btn btn-success btn-font" style="width: 100px;" />
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php //$this->load->view("slide_panel"); ?>
</div><!-- Page Container -->
<?php $this->load->view("footer"); ?>
</body>
</html>
