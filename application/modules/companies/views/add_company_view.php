<?php $this->load->view("header"); ?>

<div class="container">
	<div class="col-md-12">
		<div class="main-title">
			<h1>إضافة شركة</h1>
		</div>
	</div>
	<div class="row">
		<div class="masonary-grids">
			<div class="col-md-12">
				<div class="widget-area">
					<div class="wizard-form-h">
						<form action="" method="post">
							<div class="col-md-122" style="margin-bottom: 10px;">
								<?php if (isset($status)) echo $status; ?>
							</div>
							<div class="col-md-122">
								<div class="inline-form">
									<label class="c-label">الإسم</label>
									<input type="text" name="name"
										   value="<?php if (isset($_POST["name"])) echo htmlspecialchars(trim($_POST['name'])); ?>" required autofocus />
								</div>
							</div>
							<div class="col-md-122">
								<div class="inline-form">
									<label class="c-label">العنوان</label>
									<input type="text" name="address"
										   value="<?php if (isset($_POST["address"])) echo htmlspecialchars(trim($_POST['address'])); ?>" />
								</div>
							</div>
							<div class="col-md-62">
								<div class="inline-form">
									<label class="c-label">التليفون</label>
									<input type="text" name="phone"
										   value="<?php if (isset($_POST["phone"])) echo htmlspecialchars(trim($_POST['phone'])); ?>" />
								</div>
							</div>
							<div class="col-md-62">
								<div class="inline-form">
									<label class="c-label">الموبايل</label>
									<input type="text" name="mobile"
										   value="<?php if (isset($_POST["mobile"])) echo htmlspecialchars(trim($_POST['mobile'])); ?>" />
								</div>
							</div>
							<div class="col-md-62">
								<div class="inline-form">
									<label class="c-label">الفاكس</label>
									<input type="text" name="fax"
										   value="<?php if (isset($_POST["fax"])) echo htmlspecialchars(trim($_POST['fax'])); ?>" />
								</div>
							</div>
							<div class="col-md-62">
								<div class="inline-form">
									<label class="c-label">البريد الإلكتروني</label>
									<input type="text" name="email"
										   value="<?php if (isset($_POST["email"])) echo htmlspecialchars(trim($_POST['email'])); ?>" />
								</div>
							</div>
							<div class="col-md-62">
								<div class="inline-form">
									<label class="c-label">الموقع الإلكتروني</label>
									<input type="text" name="website"
										   value="<?php if (isset($_POST["website"])) echo htmlspecialchars(trim($_POST['website'])); ?>" />
								</div>
							</div>
							<div class="col-md-62">
								<div class="inline-form">
									<label class="c-label">الرمز البريدي</label>
									<input type="text" name="postal_code"
										   value="<?php if (isset($_POST["postal_code"])) echo htmlspecialchars(trim($_POST['postal_code'])); ?>" />
								</div>
							</div>
							<div class="col-md-122">
								<div class="col-md-122" style="margin: 20px 10px 10px 0px;">
									<button type="button" id="management-btn" class="btn btn-primary btn-font" style="width: 200px;">بيانات الإدارة</button>
								</div>
								<div class="col-md-122" id="management-info" style="display: none;">
									<div class="col-md-122">
										<div class="inline-form">
											<label class="c-label">عنوان الإدارة</label>
											<input type="text" name="management_address"
												   value="<?php if (isset($_POST["management_address"])) echo htmlspecialchars(trim($_POST['management_address'])); ?>" />
										</div>
									</div>
									<div class="col-md-62">
										<div class="inline-form">
											<label class="c-label">تليفون الإدارة</label>
											<input type="text" name="management_phone"
												   value="<?php if (isset($_POST["management_phone"])) echo htmlspecialchars(trim($_POST['management_phone'])); ?>" />
										</div>
									</div>
									<div class="col-md-62">
										<div class="inline-form">
											<label class="c-label">فاكس الإدارة</label>
											<input type="text" name="management_fax"
												   value="<?php if (isset($_POST["management_fax"])) echo htmlspecialchars(trim($_POST['management_fax'])); ?>" />
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-122">
								<div class="col-md-122" style="margin: 20px 10px 10px 0px;">
									<button type="button" id="second-branch-btn" class="btn btn-primary btn-font" style="width: 200px;">بيانات الفرع الثاني</button>
								</div>
								<div class="col-md-122" id="second-branch-info" style="display: none;">
									<div class="col-md-122">
										<div class="inline-form">
											<label class="c-label">عنوان الفرع الثاني</label>
											<input type="text" name="second_branch_address"
												   value="<?php if (isset($_POST["second_branch_address"])) echo htmlspecialchars(trim($_POST['second_branch_address'])); ?>" />
										</div>
									</div>
									<div class="col-md-62">
										<div class="inline-form">
											<label class="c-label">تليفون الفرع الثاني</label>
											<input type="text" name="second_branch_phone"
												   value="<?php if (isset($_POST["second_branch_phone"])) echo htmlspecialchars(trim($_POST['second_branch_phone'])); ?>" />
										</div>
									</div>
									<div class="col-md-62">
										<div class="inline-form">
											<label class="c-label">فاكس الفرع الثاني</label>
											<input type="text" name="second_branch_fax"
												   value="<?php if (isset($_POST["second_branch_fax"])) echo htmlspecialchars(trim($_POST['second_branch_fax'])); ?>" />
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-122">
								<div class="col-md-122" style="margin: 20px 10px 10px 0px;">
									<button type="button" id="third-branch-btn" class="btn btn-primary btn-font" style="width: 200px;">بيانات الفرع الثالث</button>
								</div>
								<div class="col-md-122" id="third-branch-info" style="display: none;">
									<div class="col-md-122">
										<div class="inline-form">
											<label class="c-label">عنوان الفرع الثالث</label>
											<input type="text" name="third_branch_address"
												   value="<?php if (isset($_POST["third_branch_address"])) echo htmlspecialchars(trim($_POST['third_branch_address'])); ?>" />
										</div>
									</div>
									<div class="col-md-62">
										<div class="inline-form">
											<label class="c-label">تليفون الفرع الثالث</label>
											<input type="text" name="third_branch_phone"
												   value="<?php if (isset($_POST["third_branch_phone"])) echo htmlspecialchars(trim($_POST['third_branch_phone'])); ?>" />
										</div>
									</div>
									<div class="col-md-62">
										<div class="inline-form">
											<label class="c-label">فاكس الفرع الثالث</label>
											<input type="text" name="third_branch_fax"
												   value="<?php if (isset($_POST["third_branch_fax"])) echo htmlspecialchars(trim($_POST['third_branch_fax'])); ?>" />
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-122">
								<div class="col-md-122" style="margin: 20px 10px 10px 0px;">
									<button type="button" id="factory-btn" class="btn btn-primary btn-font" style="width: 200px;">بيانات المصنع</button>
								</div>
								<div class="col-md-122" id="factory-info" style="display: none;">
									<div class="col-md-122">
										<div class="inline-form">
											<label class="c-label">عنوان المصنع</label>
											<input type="text" name="factory_address"
												   value="<?php if (isset($_POST["factory_address"])) echo htmlspecialchars(trim($_POST['factory_address'])); ?>" />
										</div>
									</div>
									<div class="col-md-62">
										<div class="inline-form">
											<label class="c-label">تليفون المصنع</label>
											<input type="text" name="factory_phone"
												   value="<?php if (isset($_POST["factory_phone"])) echo htmlspecialchars(trim($_POST['factory_phone'])); ?>" />
										</div>
									</div>
									<div class="col-md-62">
										<div class="inline-form">
											<label class="c-label">فاكس المصنع</label>
											<input type="text" name="factory_fax"
												   value="<?php if (isset($_POST["factory_fax"])) echo htmlspecialchars(trim($_POST['factory_fax'])); ?>" />
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-122">
								<div class="col-md-122" style="margin: 20px 10px 10px 0px;">
									<button type="button" id="first-official-btn" class="btn btn-primary btn-font" style="width: 200px;">بيانات المسئول الأول</button>
								</div>
								<div class="col-md-122" id="first-official-info" style="display: none;">
									<div class="col-md-122">
										<div class="inline-form">
											<label class="c-label">إسم المسئول الأول</label>
											<input type="text" name="first_official_name"
												   value="<?php if (isset($_POST["first_official_name"])) echo htmlspecialchars(trim($_POST['first_official_name'])); ?>" />
										</div>
									</div>
									<div class="col-md-62">
										<div class="inline-form">
											<label class="c-label">وظيفة المسئول الأول</label>
											<input type="text" name="first_official_title"
												   value="<?php if (isset($_POST["first_official_title"])) echo htmlspecialchars(trim($_POST['first_official_title'])); ?>" />
										</div>
									</div>
									<div class="col-md-62">
										<div class="inline-form">
											<label class="c-label">تليفون المسئول الأول</label>
											<input type="text" name="first_official_phone"
												   value="<?php if (isset($_POST["first_official_phone"])) echo htmlspecialchars(trim($_POST['first_official_phone'])); ?>" />
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-122">
								<div class="col-md-122" style="margin: 20px 10px 10px 0px;">
									<button type="button" id="second-official-btn" class="btn btn-primary btn-font" style="width: 200px;">بيانات المسئول الثاني</button>
								</div>
								<div class="col-md-122" id="second-official-info" style="display: none;">
									<div class="col-md-122">
										<div class="inline-form">
											<label class="c-label">إسم المسئول الثاني</label>
											<input type="text" name="second_official_name"
												   value="<?php if (isset($_POST["second_official_name"])) echo htmlspecialchars(trim($_POST['second_official_name'])); ?>" />
										</div>
									</div>
									<div class="col-md-62">
										<div class="inline-form">
											<label class="c-label">وظيفة المسئول الثاني</label>
											<input type="text" name="second_official_title"
												   value="<?php if (isset($_POST["second_official_title"])) echo htmlspecialchars(trim($_POST['second_official_title'])); ?>" />
										</div>
									</div>
									<div class="col-md-62">
										<div class="inline-form">
											<label class="c-label">تليفون المسئول الثاني</label>
											<input type="text" name="second_official_phone"
												   value="<?php if (isset($_POST["second_official_phone"])) echo htmlspecialchars(trim($_POST['second_official_phone'])); ?>" />
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-122">
								<div class="col-md-122" style="margin: 20px 10px 10px 0px;">
									<button type="button" id="third-official-btn" class="btn btn-primary btn-font" style="width: 200px;">بيانات المسئول الثالث</button>
								</div>
								<div class="col-md-122" id="third-official-info" style="display: none;">
									<div class="col-md-122">
										<div class="inline-form">
											<label class="c-label">إسم المسئول الثالث</label>
											<input type="text" name="third_official_name"
												   value="<?php if (isset($_POST["third_official_name"])) echo htmlspecialchars(trim($_POST['third_official_name'])); ?>" />
										</div>
									</div>
									<div class="col-md-62">
										<div class="inline-form">
											<label class="c-label">وظيفة المسئول الثالث</label>
											<input type="text" name="third_official_title"
												   value="<?php if (isset($_POST["third_official_title"])) echo htmlspecialchars(trim($_POST['third_official_title'])); ?>" />
										</div>
									</div>
									<div class="col-md-62">
										<div class="inline-form">
											<label class="c-label">تليفون المسئول الثالث</label>
											<input type="text" name="third_official_phone"
												   value="<?php if (isset($_POST["third_official_phone"])) echo htmlspecialchars(trim($_POST['third_official_phone'])); ?>" />
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-122">
								<div class="col-md-122" style="margin: 20px 10px 10px 0px;">
									<button type="button" id="fourth-official-btn" class="btn btn-primary btn-font" style="width: 200px;">بيانات المسئول الرابع</button>
								</div>
								<div class="col-md-122" id="fourth-official-info" style="display: none;">
									<div class="col-md-122">
										<div class="inline-form">
											<label class="c-label">إسم المسئول الرابع</label>
											<input type="text" name="fourth_official_name"
												   value="<?php if (isset($_POST["fourth_official_name"])) echo htmlspecialchars(trim($_POST['fourth_official_name'])); ?>" />
										</div>
									</div>
									<div class="col-md-62">
										<div class="inline-form">
											<label class="c-label">وظيفة المسئول الرابع</label>
											<input type="text" name="fourth_official_title"
												   value="<?php if (isset($_POST["fourth_official_title"])) echo htmlspecialchars(trim($_POST['fourth_official_title'])); ?>" />
										</div>
									</div>
									<div class="col-md-62">
										<div class="inline-form">
											<label class="c-label">تليفون المسئول الرابع</label>
											<input type="text" name="fourth_official_phone"
												   value="<?php if (isset($_POST["fourth_official_phone"])) echo htmlspecialchars(trim($_POST['fourth_official_phone'])); ?>" />
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-122">
								<div class="col-md-122" style="margin: 20px 10px 10px 0px;">
									<button type="button" id="technical-official-btn" class="btn btn-primary btn-font" style="width: 200px;">بيانات مسئول المكتب الفني</button>
								</div>
								<div class="col-md-122" id="technical-official-info" style="display: none;">
									<div class="col-md-62">
										<div class="inline-form">
											<label class="c-label">إسم مسئول المكتب الفني</label>
											<input type="text" name="technical_official_name"
												   value="<?php if (isset($_POST["technical_official_name"])) echo htmlspecialchars(trim($_POST['technical_official_name'])); ?>" />
										</div>
									</div>
									<div class="col-md-62">
										<div class="inline-form">
											<label class="c-label">تليفون مسئول المكتب الفني</label>
											<input type="text" name="technical_official_phone"
												   value="<?php if (isset($_POST["technical_official_phone"])) echo htmlspecialchars(trim($_POST['technical_official_phone'])); ?>" />
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-122">
								<div class="col-md-122" style="margin: 20px 10px 10px 0px;">
									<button type="button" id="marketing-manager-btn" class="btn btn-primary btn-font" style="width: 200px;">بيانات مدير التسويق</button>
								</div>
								<div class="col-md-122" id="marketing-manager-info" style="display: none;">
									<div class="col-md-62">
										<div class="inline-form">
											<label class="c-label">إسم مدير التسويق</label>
											<input type="text" name="marketing_manager_name"
												   value="<?php if (isset($_POST["marketing_manager_name"])) echo htmlspecialchars(trim($_POST['marketing_manager_name'])); ?>" />
										</div>
									</div>
									<div class="col-md-62">
										<div class="inline-form">
											<label class="c-label">تليفون مدير التسويق</label>
											<input type="text" name="marketing_manager_phone"
												   value="<?php if (isset($_POST["marketing_manager_phone"])) echo htmlspecialchars(trim($_POST['marketing_manager_phone'])); ?>" />
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-122">
								<div class="col-md-122" style="margin: 20px 10px 10px 0px;">
									<button type="button" id="distribution-manager-btn" class="btn btn-primary btn-font" style="width: 200px;">بيانات مدير التوزيع</button>
								</div>
								<div class="col-md-122" id="distribution-manager-info" style="display: none;">
									<div class="col-md-62">
										<div class="inline-form">
											<label class="c-label">إسم مدير التوزيع</label>
											<input type="text" name="distribution_manager_name"
												   value="<?php if (isset($_POST["distribution_manager_name"])) echo htmlspecialchars(trim($_POST['distribution_manager_name'])); ?>" />
										</div>
									</div>
									<div class="col-md-62">
										<div class="inline-form">
											<label class="c-label">تليفون مدير التوزيع</label>
											<input type="text" name="distribution_manager_phone"
												   value="<?php if (isset($_POST["distribution_manager_phone"])) echo htmlspecialchars(trim($_POST['distribution_manager_phone'])); ?>" />
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-122">
								<div class="col-md-122" style="margin: 20px 10px 10px 0px;">
									<button type="button" id="sales-manager-btn" class="btn btn-primary btn-font" style="width: 200px;">بيانات مدير المبيعات</button>
								</div>
								<div class="col-md-122" id="sales-manager-info" style="display: none;">
									<div class="col-md-62">
										<div class="inline-form">
											<label class="c-label">إسم مدير المبيعات</label>
											<input type="text" name="sales_manager_name"
												   value="<?php if (isset($_POST["sales_manager_name"])) echo htmlspecialchars(trim($_POST['sales_manager_name'])); ?>" />
										</div>
									</div>
									<div class="col-md-62">
										<div class="inline-form">
											<label class="c-label">تليفون مدير المبيعات</label>
											<input type="text" name="sales_manager_phone"
												   value="<?php if (isset($_POST["sales_manager_phone"])) echo htmlspecialchars(trim($_POST['sales_manager_phone'])); ?>" />
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-122">
								<div class="col-md-122" style="margin: 20px 10px 10px 0px;">
									<button type="button" id="accounting-manager-btn" class="btn btn-primary btn-font" style="width: 200px;">بيانات مدير الحسابات</button>
								</div>
								<div class="col-md-122" id="accounting-manager-info" style="display: none;">
									<div class="col-md-62">
										<div class="inline-form">
											<label class="c-label">إسم مدير الحسابات</label>
											<input type="text" name="accounting_manager_name"
												   value="<?php if (isset($_POST["accounting_manager_name"])) echo htmlspecialchars(trim($_POST['accounting_manager_name'])); ?>" />
										</div>
									</div>
									<div class="col-md-62">
										<div class="inline-form">
											<label class="c-label">تليفون مدير الحسابات</label>
											<input type="text" name="accounting_manager_phone"
												   value="<?php if (isset($_POST["accounting_manager_phone"])) echo htmlspecialchars(trim($_POST['accounting_manager_phone'])); ?>" />
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-122">
								<div class="inline-form">
									<label class="c-label">النشاط</label>
									<textarea name="activity" style="height: 80px;"><?php if (isset($_POST["activity"])) echo htmlspecialchars(trim($_POST['activity'])); ?></textarea>
								</div>
							</div>
							<div class="col-md-122">
								<div class="inline-form">
									<label class="c-label">الخدمات</label>
									<textarea name="services" style="height: 80px;"><?php if (isset($_POST["services"])) echo htmlspecialchars(trim($_POST['services'])); ?></textarea>
								</div>
							</div>
							<div class="col-md-122">
								<div class="inline-form">
									<label class="c-label">المنتجات</label>
									<textarea name="products" style="height: 80px;"><?php if (isset($_POST["products"])) echo htmlspecialchars(trim($_POST['products'])); ?></textarea>
								</div>
							</div>
							<div class="col-md-122">
								<div class="inline-form">
									<label class="c-label">منتجات أخري</label>
									<textarea name="other_products" style="height: 80px;"><?php if (isset($_POST["other_products"])) echo htmlspecialchars(trim($_POST['other_products'])); ?></textarea>
								</div>
							</div>
							<div class="col-md-122">
								<div class="inline-form">
									<label class="c-label">الموزعين</label>
									<textarea name="distributers" style="height: 80px;"><?php if (isset($_POST["distributers"])) echo htmlspecialchars(trim($_POST['distributers'])); ?></textarea>
								</div>
							</div>
							<div class="col-md-62">
								<div class="inline-form">
									<label class="c-label">نوع الشركة</label>
									<input type="text" name="company_type"
										   value="<?php if (isset($_POST["company_type"])) echo htmlspecialchars(trim($_POST['company_type'])); ?>" />
								</div>
							</div>
							<div class="col-md-62">
								<div class="inline-form">
									<label class="c-label">عدد العاملين</label>
									<input type="text" name="number_of_workers"
										   value="<?php if (isset($_POST["number_of_workers"])) echo htmlspecialchars(trim($_POST['number_of_workers'])); ?>" />
								</div>
							</div>
							<div class="col-md-62">
								<div class="inline-form">
									<label class="c-label">رأس المال</label>
									<input type="text" name="capital"
										   value="<?php if (isset($_POST["capital"])) echo htmlspecialchars(trim($_POST['capital'])); ?>" />
								</div>
							</div>
							<div class="col-md-62">
								<div class="inline-form">
									<label class="c-label">سنة التأسيس</label>
									<input type="text" name="year_founded"
										   value="<?php if (isset($_POST["year_founded"])) echo htmlspecialchars(trim($_POST['year_founded'])); ?>" />
								</div>
							</div>
							<div class="col-md-122">
								<div class="inline-form">
									<label class="c-label">سابقة الأعمال</label>
									<textarea name="previous_works" style="height: 80px;"><?php if (isset($_POST["previous_works"])) echo htmlspecialchars(trim($_POST['previous_works'])); ?></textarea>
								</div>
							</div>
							<div class="col-md-122">
								<div class="inline-form">
									<label class="c-label">أهم العملاء</label>
									<textarea name="important_clients" style="height: 80px;"><?php if (isset($_POST["important_clients"])) echo htmlspecialchars(trim($_POST['important_clients'])); ?></textarea>
								</div>
							</div>
							<div class="col-md-122">
								<div class="inline-form">
									<label class="c-label">نقاط البيع</label>
									<textarea name="selling_points" style="height: 80px;"><?php if (isset($_POST["selling_points"])) echo htmlspecialchars(trim($_POST['selling_points'])); ?></textarea>
								</div>
							</div>
							<div class="col-md-122">
								<div class="inline-form">
									<label class="c-label">توكيلات تجارية</label>
									<textarea name="commercial_agencies" style="height: 80px;"><?php if (isset($_POST["commercial_agencies"])) echo htmlspecialchars(trim($_POST['commercial_agencies'])); ?></textarea>
								</div>
							</div>
							<div class="col-md-122">
								<div class="inline-form">
									<label class="c-label">رقم الشركة</label>
									<input type="text" name="company_number"
										   value="<?php if (isset($_POST["company_number"])) echo htmlspecialchars(trim($_POST['company_number'])); ?>" />
								</div>
							</div>
							<div class="col-md-122" style="margin-top: 20px;">
								<input type="submit" name="submit" value="حفظ" class="btn btn-success btn-font" style="width: 100px;" />
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php //$this->load->view("slide_panel"); ?> 
</div><!-- Page Container -->
<?php $this->load->view("footer"); ?>
<script>
	$(document).ready(function() {
		$("#management-btn").click(function () {
			$("#management-info").slideToggle();
		});

		$("#second-branch-btn").click(function () {
			$("#second-branch-info").slideToggle();
		});

		$("#third-branch-btn").click(function () {
			$("#third-branch-info").slideToggle();
		});

		$("#factory-btn").click(function () {
			$("#factory-info").slideToggle();
		});

		$("#first-official-btn").click(function () {
			$("#first-official-info").slideToggle();
		});

		$("#second-official-btn").click(function () {
			$("#second-official-info").slideToggle();
		});

		$("#third-official-btn").click(function () {
			$("#third-official-info").slideToggle();
		});

		$("#fourth-official-btn").click(function () {
			$("#fourth-official-info").slideToggle();
		});

		$("#technical-official-btn").click(function () {
			$("#technical-official-info").slideToggle();
		});

		$("#marketing-manager-btn").click(function () {
			$("#marketing-manager-info").slideToggle();
		});

		$("#distribution-manager-btn").click(function () {
			$("#distribution-manager-info").slideToggle();
		});

		$("#sales-manager-btn").click(function () {
			$("#sales-manager-info").slideToggle();
		});

		$("#accounting-manager-btn").click(function () {
			$("#accounting-manager-info").slideToggle();
		});
	});
</script>
</body>
</html>
