<?php $this->load->view("header"); ?>

<div class="container">
	<div class="col-md-12">
		<div class="col-md-62">
			<div class="main-title">
				<h1>إدارة الشركات</h1>
			</div>
		</div>
		<div class="col-md-62" style="position: relative; top: 10px; right: 50px;">
			<form action="" method="post" class="search">
				<input type="text" name="search" placeholder="بحث" required autofocus />
				<button name="submit"><i class="fa fa-search"></i></button>
			</form>
		</div>
	</div>
	<div class="row">
		<div class="masonary-grids">
			<div class="col-md-12">
				<div class="widget-area">
					<?php if ($this->session->flashdata("status")): ?>
						<div class="col-md-122" id="status" style="background-color: #EEE; padding: 10px;"><?= $this->session->flashdata("status"); ?></div>
					<?php endif; ?>
					<div class="streaming-table">
						<span id="found" class="label label-info"></span>
						<table id="stream_table" class='table table-striped table-bordered'>
							<thead>
								<tr>
									<th>ID</th>
									<th>الإسم</th>
									<th style="width: 20%;">تعديل</th>
									<th style="width: 20%;">حذف</th>
								</tr>
							</thead>
							<tbody class="tbody_admin">
								<?php if (isset($companies)): ?>
								<?php foreach ($companies as $company): ?>
									<tr>
										<td><?= $company["id"]; ?></td>
										<td style="text-align: center; vertical-align: middle;"><?= $company["name"]; ?><h4></td>
										<td style="text-align: center; vertical-align: middle;">
											<a href="<?= site_url(); ?>companies/edit/<?= $company['id']; ?>">
												<button class="btn btn-warning btn-font" style="width: 150px;" type="button">تعديل</button>
											</a>
										</td>
										<td style="text-align: center; vertical-align: middle;">
											<a onclick="alertDelete('companies/delete/<?= $company['id']; ?>', 'هل أنت متأكد من مسح هذه الشركة؟');" href="javascript:void(null);">
	                                            <button class="btn btn-danger btn-font" style="width: 150px;" type="button" >حذف</button>
											</a>
										</td>
									</tr>
								<?php endforeach; ?>
								<?php endif; ?>
							</tbody>
						</table>
					</div>

					<?php if (isset($pagination)): ?>
						<div class="pagination-news">
							<?= $pagination; ?>
						</div>
					<?php endif; ?>

				</div>
			</div>
		</div>
	</div>
	<?php //$this->load->view("slide_panel"); ?>
</div><!-- Page Container -->
<?php $this->load->view("footer"); ?>
<script>
	$(document).ready(function() {
		setTimeout(function() {
			$("#status").fadeOut(2000);
		}, 3000);
	});
</script>
</body>
</html>
