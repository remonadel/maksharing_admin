<?php $this->load->view("header"); ?>
<div class="container">
    <div class="col-md-12">
        <div class="main-title">
            <h1>تعديل صفحة <?= $page['name'] ?></h1>
        </div>
    </div>
    <div class="row">
        <div class="masonary-grids">
            <form action="" method="post" id="insert-form">
                <div class="col-md-122">
                    <div class="col-md-122" style="margin-top: -4px;">
                        <div class="widget-area">
                            <?php if (isset($status)): ?>
                                <div class="col-md-122" style="margin-right: 10px;"><?= $status; ?></div>
                            <?php endif; ?>

                        </div>
                    </div>
                    <div class="col-md-122">
                        <div class="widget-area">

                            <div class="inline-form">
                                <label>محتوي الصفحة باللغة العربية*</label>
                                <textarea name="content_ar" class="content_input" id="tinymce-content"><?= stripslashes(trim(@$page["content_ar"])); ?></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-122">
                        <div class="widget-area ar">

                            <div class="inline-form">
                                <label>محتوي الصفحة باللغة الإنجليزية*</label>
                                <textarea name="content_en" class="content_input" id="tinymce-content"><?= htmlspecialchars_decode(trim(@$page["content_en"])); ?></textarea>
                            </div>
                        </div>
                    </div>

                        <div class="widget-area">
                            <div class="col-md-62">
                                <input type="submit" name="submit" value="حفظ" id="submit_form" class="btn btn-success btn-font" style="width: 100px;" />
                            </div>
                        </div>
                </div>

            </form>
        </div>
    </div>
<?php //$this->load->view("slide_panel"); ?>
</div><!-- Page Container -->
<?php $this->load->view("footer"); ?>
<script>
    $(document).ready(function () {
        var users = [
            <?php if (isset($users)): ?>
                <?php foreach ($users as $user): ?>
                    <?= "\"" . $user["name"] . "\"" . ","; ?>
                <?php endforeach; ?>
            <?php endif; ?>
        ];

        $("#author").autocomplete({source: users});
        $("#photography").autocomplete({source: users});
    });
</script>
<script>
    tinymce.init({
        selector: "textarea#tinymce-content",
        theme: "modern",
        height: 400,
        directionality: "rtl",
        language: "ar",
        menubar: false,
        content_css: "<?= ASSETS; ?>css/content.css",
        toolbar1: 'insertfile undo redo |  forecolor backcolor styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
        toolbar2: 'print preview media | emoticons | codesample undo redo pastetext | styleselect | fontselect | fontsizeselect ltr rtl' ,
        fontsize_formats: "8pt 9pt 10pt 11pt 12pt 26pt 36pt",
        image_advtab: true,
        plugins: [
            "preview searchreplace fullscreen wordcount charcount paste link image media  textcolor directionality"
        ],
        paste_as_text: true,
        invalid_elements : "",
        textcolor_map: [
            "000000", "Black",
            "993300", "Burnt orange",
            "333300", "Dark olive",
            "003300", "Dark green",
            "003366", "Dark azure",
            "000080", "Navy Blue",
            "333399", "Indigo",
            "333333", "Very dark gray",
            "800000", "Maroon",
            "FF6600", "Orange",
            "808000", "Olive",
            "008000", "Green",
            "008080", "Teal",
            "0000FF", "Blue",
            "666699", "Grayish blue",
            "808080", "Gray",
            "FF0000", "Red",
            "FF9900", "Amber",
            "99CC00", "Yellow green",
            "339966", "Sea green",
            "33CCCC", "Turquoise",
            "3366FF", "Royal blue",
            "800080", "Purple",
            "999999", "Medium gray",
            "FF00FF", "Magenta",
            "FFCC00", "Gold",
            "FFFF00", "Yellow",
            "00FF00", "Lime",
            "00FFFF", "Aqua",
            "00CCFF", "Sky blue",
            "993366", "Red violet",
            "FFFFFF", "White",
            "FF99CC", "Pink",
            "FFCC99", "Peach",
            "FFFF99", "Light yellow",
            "CCFFCC", "Pale green",
            "CCFFFF", "Pale cyan",
            "99CCFF", "Light sky blue",
            "CC99FF", "Plum"
        ]

    });
</script>

</body>
</html>
