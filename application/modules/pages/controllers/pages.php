<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pages extends MX_Controller {

	public function __construct()
    {
        parent::__construct();
		$this->deny->deny_if_logged_out();

		$this->load->model("pages_model");
    }


	public function edit($id)
	{
		$authorized = $this->common_model->authorized_to_view_page("news");
		if ($authorized)
		{
			$data = array();
			$page = $this->pages_model->get_page($id);
			if (!$page) redirect(site_url());
			$data["page"] = $page;
			if (isset($_POST["submit"]))
			{
				$content_en = htmlspecialchars(str_replace(array("<br />", "<br>", "<br/>"), "", $_POST['content_en']));
				$content_ar = htmlspecialchars(str_replace(array("<br />", "<br>", "<br/>"), "", $_POST['content_ar']));

				if (empty($content_en) OR empty($content_ar))
				{
					$data["status"] = "<p class='error-msg'>يجب إدخال جميع البيانات الإجبارية</p>";
				}
				else
				{
					// Edit information on database
					$this->pages_model->edit_page($content_en, $content_ar, $id);

					$this->session->set_flashdata("status", "تمت العملية بنجاح");
					$redirect_url = site_url() . "pages/edit/$id";
					redirect($redirect_url);
				}
			}
			$this->load->view("edit_page_view", $data);
		}
    }

}


/* End of file pages.php */
/* Location: ./application/modules/pages/controllers/pages.php */
