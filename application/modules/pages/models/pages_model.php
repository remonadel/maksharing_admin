<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pages_model extends CI_Model {
    
    public function __construct()
    {
        parent::__construct();
    }
    
    public function edit_page($content_en, $content_ar, $id)
    {

		$sql = "UPDATE `pages` SET `content_en` = ?, `content_ar` = ? WHERE `id` = ?";

		$query = $this->db->query($sql, array($content_en, $content_ar, $id));

    }
	
	public function get_page($id)
	{
		$sql = "SELECT * FROM `pages` WHERE `id` = ? ";
		$query = $this->db->query($sql, array($id));
		
		return ($query->num_rows() >= 1) ? $query->row_array() : FALSE;
	}

}


/* End of file pages_model.php */
/* Location: ./application/modules/pages/models/pages_model.php */