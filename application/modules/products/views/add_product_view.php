<?php $this->load->view("header"); ?>
<div class="container">
    <div class="col-md-12">
        <div class="main-title">
            <h1>إضافة منتج جديد</h1>
        </div>
    </div>
    <div class="row">
        <div class="masonary-grids">
            <form action="" method="post" id="insert-form">
                <div class="col-md-722">
                    <div class="col-md-122" style="margin-top: -4px;">
                        <div class="widget-area">
                            <?php if (isset($status)): ?>
                                <div class="col-md-122" style="margin-right: 10px;"><?= $status; ?></div>
                            <?php endif; ?>
                            <div class="col-md-122">
                                <div class="inline-form" id="title-div">
                                    <label>إسم المنتج *</label>
                                    <input class="input-style" id="name" type="text" name="name" value="<?= htmlspecialchars(trim(@$_POST["name"]));?>" required autofocus />
                                </div>
                            </div>
                            <div class="col-md-122">
                                <div class="inline-form" id="desc-div">
                                    <label>الملخص</label>
                                    <textarea style="height: 100px;" name="description" id="description"><?= @$_POST['description']; ?></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-122">
                        <div class="widget-area">
                            <div style="margin: 0 auto; width: 150px;">
                                <button id="add_image_btn" class="btn btn-primary btn-font" type="button" style="width: 150px; margin-left: 10px;">إضافة صورة</button>
                            </div>
                            <div class="inline-form">
                                <label>المحتوي *</label>
                                <textarea name="content" class="content_input" id="tinymce-content"><?= htmlspecialchars(trim(@$_POST["content"])); ?></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-122 clear">
                        <div class="widget-area">
                            <div class="col-md-62" style="margin-bottom: 12px;">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <?php if (isset($_POST["publish"]) && $_POST["publish"] == 1): ?>
                                            <input type="checkbox" name="publish" value="1" checked>
                                        <?php else: ?>
                                            <input type="checkbox" name="publish" value="1">
                                        <?php endif; ?>
                                    </span>
                                    <p class="form-control"> نشر</p>
                                </div>
                            </div>
                            <div class="col-md-62">
                                <input type="submit" name="submit" value="حفظ" id="submit_form" class="btn btn-success btn-font" style="width: 100px;" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-5new">
                    <div class="col-md-122">
                        <div class="widget-area">
                            <div class="col-md-122">
                                <div class="inline-form">
                                    <label>سعر *</label>
                                    <input type="number" min="1" name="price" id="price"
                                           value="<?= htmlspecialchars(trim(@$_POST["price"])); ?>" required />
                                </div>
                            </div>
                            <div class="col-md-122">
                                <div class="inline-form">
                                    <label>الكاتب</label>
                                    <input type="text" name="author" id="author"
                                           value="<?= htmlspecialchars(trim(@$_POST["author"])); ?>" />
                                </div>
                            </div>
                            <div class="col-md-122">
                                <div class="inline-form">
                                    <label>الكلمات الدالة</label>
                                    <input type="text" name="keywords"
                                           value="<?= htmlspecialchars(trim(@$_POST["keywords"])); ?>" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-122 clear">
                        <div class="widget-area">
                            <div class="col-md-62">
                                <div class="inline-form">
                                    <label class="c-label">القسم الرئيسي *</label>
                                    <select name="category_id" id="categories" required>
                                        <option value=""></option>
                                        <?php if (isset($categories)): ?>
                                            <?php foreach ($categories as $category): ?>
                                                <option value="<?= $category['id']; ?>"><?= $category["name"]; ?></option>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-62">
                                <div class="inline-form">
                                    <label class="c-label">القسم الفرعي *</label>
                                    <select name="subcategory_id" id="subcategories" required>
                                        <option value=""></option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-122 clear">
                        <div class="widget-area">
                            <div style="float: right; margin-left: 20px;">
                                <div><button id="add_main_image_btn" class="btn btn-primary btn-font" type="button" style="margin-bottom: 10px; width: 150px;">إضافة صورة رئيسية</button></div>
                                <div><button id="add_main_album_btn" class="btn btn-primary btn-font" type="button" style="margin-bottom: 10px; width: 150px;">إضافة ألبوم رئيسي</button></div>
                            </div>
                            <div style="float: right; margin-left: 20px;">
                                <input type="hidden" name="main_image" id="main_image" value="<?= @$_POST["main_image"]; ?>" />
                                <input type="hidden" name="main_album" id="main_album" value="<?= @$_POST['main_album']; ?>" />

                                <div id="main-media-div">
                                    <?php if (isset($_POST["main_image"]) && ! empty($_POST["main_image"])): ?>
                                        <img src="<?= IMG_ARCHIVE . '311x253/' . $_POST['main_image']; ?>" width="220" height="170">
                                    <?php elseif (isset($_POST["main_album"]) && ! empty($_POST["main_album"])): ?>
                                        <img src="<?= IMG_ARCHIVE . '311x253/' . end(explode('&', $_POST['main_album'])); ?>" width="220" height="170">
                                    <?php endif; ?>
                                </div>

                                <div class="inline-form" id="main-image-label-div" style="width: 219px; display: none;">
                                    <label>عنوان الصورة *</label>
                                    <input type="text" name="main_image_label" id="main-image-label" value="<?php if (isset($_POST['main_image_label'])) echo htmlspecialchars(trim($_POST['main_image_label'])); ?>" />
                                </div>
                            </div>
                            <div class="del_btn" style="display: none">
                                <button onclick="remove_content();" class="btn btn-primary btn-font" type="button"><span class="fa fa-remove"></span></button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
<?php //$this->load->view("slide_panel"); ?>
</div><!-- Page Container -->
<?php $this->load->view("footer"); ?>
<script>
    tinymce.init({
        selector: "textarea#tinymce-content",
        theme: "modern",
        height: 400,
        directionality: "rtl",
        language: "ar",
        menubar: false,
        content_css: "<?= ASSETS; ?>css/content.css",
        plugins: [
            "preview searchreplace fullscreen wordcount charcount paste link image media"
        ],
        toolbar: "searchreplace | undo redo | bold | bullist numlist | preview | link | image | fullscreen",
        paste_as_text: true,
        invalid_elements : "span,div,br",
        setup: function (ed) {
            $('#add_image_btn').click(function () {
                add_image(ed);
            });
        }
    });
</script>
<script>
    $(document).ready(function () {
        // Author autocomplete
        var users = [
            <?php if (isset($users)): ?>
                <?php foreach ($users as $user): ?>
                    <?= "\"" . $user["name"] . "\"" . ","; ?>
                <?php endforeach; ?>
            <?php endif; ?>
        ];

        $("#author").autocomplete({source: users});


        // Copy the subsections select, so we can easily reset it later
        var subcategoriesClone = $("#subcategories").clone();

        $("#categories").change(function () {
            categoriesVal = $(this).val();

            // Reset the subcategories select on each change to the categories select
            $("#subcategories").html(subcategoriesClone.html());

            $.ajax({
                type: "POST",
                url: "<?= site_url(); ?>categories/get_subcategories_of_category",
                data: {category_id: categoriesVal},
                success: function (response) {
                    response = JSON.parse(response);
                    if (response != "none")
                    {
                        var result = "";
                        for (var i = 0; i < response.length; i++)
                        {
                            result += "<option value='" + response[i].id + "'>" + response[i].name + "</option>";
                        }
                        $("#subcategories").append(result);
                    }
                }
            });
        });


        this_window_title = window.document.title;

        // Choosing main image code
        $("#add_main_image_btn").click(function () {
            images_window = window.open("<?= site_url(); ?>images/list_images", "_blank", "titlebar=no, toolbar=no, location=no, status=no, menubar=no, scrollbars=yes, resizable=no, top=0, left=0, width=1050, height=" + window.innerHeight);
            var timer = setInterval(check_window_close, 500);

            function check_window_close() {
                image = images_window.document.title;
                if (images_window.closed)
                {
                    // Only do any action if he chooses an image from the opened window and doesn't just close it again without choosing
                    if (image != this_window_title)
                    {
                        // Stop the timer
                        clearInterval(timer);

                        var result = image.split('&');

                        // Set the hidden input value to the video name
                        $("#main_image").val(result[1]);
                        var html = "<img src='<?= IMG_ARCHIVE; ?>311x253/" + result[1] + "' width='220' height='170'>";

                        $('#main-media-div').html(html);
                        $("#add_main_album_btn").attr('disabled', 'disabled');
                        $(".del_btn").css('display', 'block');
                        $("#main-image-label-div").css('display', 'block');
                    }
                }
            }
        });


        // Choosing main album code
        $("#add_main_album_btn").click(function () {
            albums_window = window.open("<?= site_url(); ?>albums/list_albums", "_blank", "titlebar=no, toolbar=no, location=no, status=no, menubar=no, scrollbars=yes, resizable=no, top=0, left=0, width=1050, height=" + window.innerHeight);
            var timer = setInterval(check_window_close, 500);

            function check_window_close() {
                album = albums_window.document.title;

                if (albums_window.closed)
                {
                    // Only do any action if he chooses an image from the opened window and doesn't just close it again without choosing
                    if (album != this_window_title)
                    {
                        // Stop the timer
                        clearInterval(timer);

                        var result = album.split('&');

                        // Set the hidden input value to the album name
                        $("#main_album").val(album);

                        var html = "<img src='<?= IMG_ARCHIVE; ?>311x253/" + result[1] + "' width='220' height='170'>";

                        $('#main-media-div').html(html);
                        $("#add_main_image_btn").attr('disabled', 'disabled');
                        $(".del_btn").css('display', 'block');
                    }
                }
            }
        });
    });


    function remove_content() {
        $("#main-media-div").html("");
        $("#main_image").val("");
        $("#main-image-label").val("");
        $("#main-image-label-div").css('display', 'none');
        $("#main_album").val("");
        $("#add_main_image_btn").removeAttr('disabled');
        $("#add_main_album_btn").removeAttr('disabled');
        $(".del_btn").css('display', 'none');
    }


    function add_image(editorObject)
    {
        html = editorObject.getContent();
        select_images_window = window.open("<?= site_url(); ?>images/list_images", "_blank", "titlebar=no, toolbar=no, location=no, status=no, menubar=no, scrollbars=yes, resizable=no, top=0, left=0, width=1050, height=" + window.innerHeight);
        var timer = setInterval(check_window_close, 500);

        function check_window_close()
        {
            image = select_images_window.document.title;

            if (select_images_window.closed)
            {
                // Only do any action if he chooses an image from the opened window and doesn't just close it again without choosing
                if (image != this_window_title)
                {
                    // Stop the timer
                    clearInterval(timer);

                    var result = image.split('&');

                    html = "<img src='<?= IMG_ARCHIVE; ?>original_lower_quality/" + result[1] + "' style='max-width:628px;' /><br>"
                    editorObject.insertContent(html);
                }
            }
        }
    }


    $("#submit_form").click(function (e) {
        // Form validations
        var name = $("#name").val().trim();
        var price = $("#price").val().trim();

        if ( ! name)
        {
            e.preventDefault();
            swal("يجب إدخال إسم المنتج");
        }
        else if ( ! price)
        {
            e.preventDefault();
            swal("يجب إدخال سعر المنتج");
        }
        else if ( ! $("input[name=main_image]").val() && ! $("input[name=main_album]").val())
        {
            e.preventDefault();
            swal("يرجي إختيار صورة رئيسية أو ألبوم رئيسي للمنتج");
        }
        else if ($("input[name=main_image]").val() && ! $("#main-image-label").val())
        {
            e.preventDefault();
            swal("يرجي إدخال عنوان الصورة الرئيسية للمنتج");
        }
    });
</script>
</body>
</html>
