<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Products_model extends CI_Model {
    
    public function __construct()
    {
        parent::__construct();
    }
    
	
	public function get_all_users()
	{
		$sql = "SELECT `name` FROM `users_details`";
		$query = $this->db->query($sql);
		
		return ($query->num_rows() >= 1) ? $query->result_array() : FALSE;
	}
	
	
    public function insert_product($name, $price, $author, $description, $content, $main_image, $main_image_label, $main_album, $keywords,
								   $category_id, $subcategory_id, $created_by_id, $published)
    {
		$published_by_id = ($published == 1) ? $created_by_id : NULL;
		$published_at = ($published == 1) ? date("Y-m-d H:i:s") : NULL;
		
        $sql = "INSERT INTO `products` (`name`, `price`, `author`, `description`, `content`, `main_image`, `main_image_label`, `main_album`, `keywords`,
				`category_id`, `subcategory_id`, `created_by_id`, `created_at`, `published`, `published_by_id`, `published_at`)
                VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, NOW(), ?, ?, ?)";
				
        $query = $this->db->query($sql, array($name, $price, $author, $description, $content, $main_image, $main_image_label, $main_album, $keywords,
											  $category_id, $subcategory_id, $created_by_id, $published, $published_by_id, $published_at));
    }
	
    
    public function edit_product($name, $price, $author, $description, $content, $main_image, $main_image_label, $main_album, $keywords,
								 $category_id, $subcategory_id, $published_here, $unpublished_here, $id)
    {
		if ($published_here)
		{
			$published_by_id = $this->session->userdata("id");
			
			$sql = "UPDATE `products` SET `name` = ?, `price` = ?, `author` = ?, `description` = ?, `content` = ?, `main_image` = ?, `main_image_label` = ?,
					`main_album` = ?, `keywords` = ?, `category_id` = ?, `subcategory_id` = ?, `published` = ?, `published_by_id` = ?, `published_at` = NOW()
					WHERE `id` = ?";
			
			$query = $this->db->query($sql, array($name, $price, $author, $description, $content, $main_image, $main_image_label, $main_album, $keywords,
												  $category_id, $subcategory_id, 1, $published_by_id, $id));
		}
		elseif ($unpublished_here)
		{
			$sql = "UPDATE `products` SET `name` = ?, `price` = ?, `author` = ?, `description` = ?, `content` = ?, `main_image` = ?, `main_image_label` = ?,
					`main_album` = ?, `keywords` = ?, `category_id` = ?, `subcategory_id` = ?, `published` = ?, `published_by_id` = ?, `published_at` = ?
					WHERE `id` = ?";
			
			$query = $this->db->query($sql, array($name, $price, $author, $description, $content, $main_image, $main_image_label, $main_album, $keywords,
												  $category_id, $subcategory_id, 0, NULL, NULL, $id));
		}
		else
		{
			$sql = "UPDATE `products` SET `name` = ?, `price` = ?, `author` = ?, `description` = ?, `content` = ?, `main_image` = ?, `main_image_label` = ?,
					`main_album` = ?, `keywords` = ?, `category_id` = ?, `subcategory_id` = ?
					WHERE `id` = ?";
			
			$query = $this->db->query($sql, array($name, $price, $author, $description, $content, $main_image, $main_image_label, $main_album, $keywords,
												  $category_id, $subcategory_id, $id));
		}
    }
	
	
	public function get_products_count($published)
	{
		$sql = "SELECT COUNT(`id`) AS `count` FROM `products` WHERE `published` = ?";
        $query = $this->db->query($sql, array($published));
        $count = $query->row_array();
        return $count["count"];
	}
	
	
	public function get_products($published, $offset, $limit)
	{
		$sql = "SELECT * FROM `products` WHERE `published` = ? ORDER BY `published_at` DESC, `created_at` DESC LIMIT ?, ?";
		$query = $this->db->query($sql, array($published, $offset, $limit));
		
		return ($query->num_rows() >= 1) ? $query->result_array() : FALSE;
	}
	
	
	public function get_search_rows_count($query)
    {
        $sql = "SELECT COUNT(`id`) AS `count` FROM `products` WHERE `name` LIKE '%$query%'";
        $query = $this->db->query($sql);
        $count = $query->row_array();
        return $count["count"];
    }
    
	
	public function search_products($query, $offset, $limit)
    {
        $sql = "SELECT * FROM `products` WHERE `name` LIKE '%$query%' ORDER BY `published_at` DESC, `created_at` DESC LIMIT ?, ?";
        $query = $this->db->query($sql, array($offset, $limit));
        
		return ($query->num_rows() >= 1) ? $query->result_array() : FALSE;
    }
	
	
	public function update_image_used_flag($image_token, $token_value, $action)
	{
		/* Updates the images "used" flag depending on the $action parameter, "+" adds one, "-" subtracts 1 */
		if ($action == "+")
		{
			$sql = "UPDATE `images` SET `times_used` = `times_used` + 1 WHERE `$image_token` = ?";
		}
		elseif ($action == "-")
		{
			$sql = "UPDATE `images` SET `times_used` = `times_used` - 1 WHERE `$image_token` = ?";
		}
		
		$query = $this->db->query($sql, array($token_value));
	}
	
}


/* End of file products_model.php */
/* Location: ./application/modules/products/models/products_model.php */