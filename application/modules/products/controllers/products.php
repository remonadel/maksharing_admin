<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Products extends MX_Controller {

	public function __construct()
    {
        parent::__construct();
		$this->deny->deny_if_logged_out();

		$this->load->model("products_model");
    }


	public function index()
	{
		redirect(site_url() . "products/unpublished");
	}


	public function unpublished()
	{
		$authorized = $this->common_model->authorized_to_view_page("products");
		if ($authorized)
		{
			$data = array();

			$current_page = (int) $this->uri->segment(3);
			$per_page = 20;
			$products_count = $this->products_model->get_products_count(0);

			$config["base_url"] = site_url() . "products/unpublishd";
			$config['uri_segment'] = 3;
			$config["total_rows"] = $products_count;
			$config["per_page"] = $per_page;
			$this->pagination->initialize($config);

			$products = $this->products_model->get_products(0, $current_page, $per_page);
			if ($products)
			{
				$data["products"] = $products;
				$data["pagination"] = $this->pagination->create_links();
			}

			if (isset($_POST["search"]))
			{
				$query = htmlspecialchars(trim($_POST["search"]));
				redirect(site_url() . "products/search/$query");
			}

			$this->load->view("manage_products_view", $data);
		}
    }


	public function published()
	{
		$authorized = $this->common_model->authorized_to_view_page("products");
		if ($authorized)
		{
			$data = array();

			$current_page = (int) $this->uri->segment(3);
			$per_page = 20;
			$products_count = $this->products_model->get_products_count(1);

			$config["base_url"] = site_url() . "products/publishd";
			$config['uri_segment'] = 3;
			$config["total_rows"] = $products_count;
			$config["per_page"] = $per_page;
			$this->pagination->initialize($config);

			$products = $this->products_model->get_products(1, $current_page, $per_page);
			if ($products)
			{
				$data["products"] = $products;
				$data["pagination"] = $this->pagination->create_links();
			}

			if (isset($_POST["search"]))
			{
				$query = htmlspecialchars(trim($_POST["search"]));
				redirect(site_url() . "products/search/$query");
			}

			$this->load->view("manage_products_view", $data);
		}
    }


	public function search($query)
	{
		$authorized = $this->common_model->authorized_to_view_page("products");
		if ($authorized)
		{
			if (empty($query)) redirect(site_url() . "products/unpublished");
			$query = urldecode($query);

			$data = array();

			$current_page = (int) $this->uri->segment(4);
			$per_page = 20;
			$products_count = $this->products_model->get_search_rows_count($query);

			$config["base_url"] = site_url() . "products/search/$query";
			$config['uri_segment'] = 4;
			$config["total_rows"] = $products_count;
			$config["per_page"] = $per_page;
			$this->pagination->initialize($config);

			$products = $this->products_model->search_products($query, $current_page, $per_page);
			if ($products)
			{
				$data["products"] = $products;
				$data["pagination"] = $this->pagination->create_links();
			}

			if (isset($_POST["search"]))
			{
				$query = htmlspecialchars(trim($_POST["search"]));
				redirect(site_url() . "products/search/$query");
			}

			$this->load->view("manage_products_view", $data);
		}
	}


	public function add()
	{
        $authorized = $this->common_model->authorized_to_view_page("products");
		if ($authorized)
		{
			$data = array();

			$users = $this->products_model->get_all_users();
			if ($users)	$data["users"] = $users;

			$categories = $this->common_model->get_all_from_table("categories");
			if ($categories) $data["categories"] = $categories;

			if (isset($_POST["submit"]))
			{
				$name = htmlspecialchars(trim($_POST["name"]));
				$price = htmlspecialchars(trim($_POST["price"]));
				$author = (empty($_POST["author"])) ? NULL : htmlspecialchars(trim($_POST["author"]));
				$description = (empty($_POST["description"])) ? NULL : htmlspecialchars(trim($_POST["description"]));
				$content = str_replace('../uploads/', site_url().'uploads/', trim($_POST["content"]));
				$content = htmlspecialchars(str_replace(array("<br />", "<br>", "<br/>"), "", $content));
				$main_image = (empty($_POST["main_image"])) ? NULL : $_POST["main_image"];
				$main_image_label = NULL;
				if ( ! is_null($main_image)) $main_image_label = (empty($_POST["main_image_label"])) ? NULL : htmlspecialchars(trim($_POST["main_image_label"]));
				$main_album = (empty($_POST["main_album"])) ? NULL : $_POST["main_album"];
				$keywords = (empty($_POST["keywords"])) ? NULL : htmlspecialchars(trim($_POST["keywords"]));
				$category_id = $_POST["category_id"];
				$subcategory_id = $_POST["subcategory_id"];
				$created_by_id = $this->session->userdata("id");
				$publish = (isset($_POST["publish"])) ? 1 : 0;

				if (empty($name) OR empty($price) OR empty($content) OR (is_null($main_image) && is_null($main_album)))
				{
					$data["status"] = "<p class='error-msg'>يجب إدخال جميع البيانات الإجبارية</p>";
				}
				else
				{
					// Insert information into database
					$this->products_model->insert_product($name, $price, $author, $description, $content, $main_image, $main_image_label, $main_album, $keywords,
														  $category_id, $subcategory_id, $created_by_id, $publish);

					// If main image was chosen, update its 'used' flag by adding 1
					if ( ! is_null($main_image)) $this->products_model->update_image_used_flag("name", $main_image, "+");

					$this->session->set_flashdata("status", "تمت العملية بنجاح");
					$redirect_url = ($publish == 0) ? site_url() . "products/unpublished" : site_url() . "products/published";
					redirect($redirect_url);
				}
			}

			$this->load->view("add_product_view", $data);
		}
	}


	public function view($id = "")
	{
		$authorized = $this->common_model->authorized_to_view_page("products");
		if ($authorized)
		{
			$product = $this->common_model->get_subject_with_token("products", "id", $id);
			if (empty($id) OR ! $product) show_404();

			$product["created_by_name"] = $this->common_model->get_info_by_token("users_details", "name", "id", $product["created_by_id"]);
			if ($product["published"] == 1)
			{
				$product["published_by_name"] = $this->common_model->get_info_by_token("users_details", "name", "id", $product["published_by_id"]);
			}
			$data["product"] = $product;

			$this->load->view("ajax_view_product_view", $data);
		}
	}


	public function edit($id = "")
	{
        $authorized = $this->common_model->authorized_to_view_page("products");
		if ($authorized)
		{
			$product = $this->common_model->get_subject_with_token("products", "id", $id);
			if (empty($id) OR ! $product) show_404();

			$data["product"] = $product;

			$users = $this->products_model->get_all_users();
			if ($users)	$data["users"] = $users;

			$categories = $this->common_model->get_all_from_table("categories");
			if ($categories) $data["categories"] = $categories;

			$subcategories_of_category = $this->common_model->get_all_subjects_with_token("subcategories", "category_id", $product["category_id"]);
			if ($subcategories_of_category) $data["subcategories_of_category"] = $subcategories_of_category;

			if (isset($_POST["submit"]))
			{
				$name = htmlspecialchars(trim($_POST["name"]));
				$price = htmlspecialchars(trim($_POST["price"]));
				$author = (empty($_POST["author"])) ? NULL : htmlspecialchars(trim($_POST["author"]));
				$description = (empty($_POST["description"])) ? NULL : htmlspecialchars(trim($_POST["description"]));
				$content = str_replace('../uploads/', site_url().'uploads/', trim($_POST["content"]));
				$content = htmlspecialchars(str_replace(array("<br />", "<br>", "<br/>"), "", $content));
				$main_image = (empty($_POST["main_image"])) ? NULL : $_POST["main_image"];
				$main_image_label = NULL;
				if ( ! is_null($main_image)) $main_image_label = (empty($_POST["main_image_label"])) ? NULL : htmlspecialchars(trim($_POST["main_image_label"]));
				$main_album = (empty($_POST["main_album"])) ? NULL : $_POST["main_album"];
				$keywords = (empty($_POST["keywords"])) ? NULL : htmlspecialchars(trim($_POST["keywords"]));
				$category_id = $_POST["category_id"];
				$subcategory_id = $_POST["subcategory_id"];
				$publish = (isset($_POST["publish"])) ? 1 : 0;
				$published_here = ($product["published"] == 0 && $publish == 1) ? TRUE : FALSE;
				$unpublished_here = ($product["published"] == 1 && $publish == 0) ? TRUE : FALSE;

				if (empty($name) OR empty($price) OR empty($content) OR (is_null($main_image) && is_null($main_album)))
				{
					$data["status"] = "<p class='error-msg'>يجب إدخال جميع البيانات الإجبارية</p>";
				}
				else
				{
					// Edit information on database
					$this->products_model->edit_product($name, $price, $author, $description, $content, $main_image, $main_image_label, $main_album, $keywords,
														$category_id, $subcategory_id, $published_here, $unpublished_here, $id);

					// Update old and/or new image's 'used' flags appropriately
					if (is_null($product["main_image"]) && ! is_null($main_image))
					{
						$this->products_model->update_image_used_flag("name", $main_image, "+");
					}
					elseif ( ! is_null($product["main_image"]) && is_null($main_image))
					{
						$this->products_model->update_image_used_flag("name", $product["main_image"], "-");
					}
					elseif ( ! is_null($product["main_image"]) && ! is_null($main_image))
					{
						if ($product["main_image"] != $main_image)
						{
							$this->products_model->update_image_used_flag("name", $product["main_image"], "-");
							$this->products_model->update_image_used_flag("name", $main_image, "+");
						}
					}

					$this->session->set_flashdata("status", "تمت العملية بنجاح");
					$redirect_url = ($publish == 0) ? site_url() . "products/unpublished" : site_url() . "products/published";
					redirect($redirect_url);
				}
			}

			$this->load->view("edit_product_view", $data);
		}
	}


	public function delete($id = "")
    {
		$authorized = $this->common_model->authorized_to_view_page("products");
		if ($authorized)
		{
			$product_exists = $this->common_model->subject_exists("products", "id", $id);
			if (empty($id) OR ! $product_exists) show_404();

			$this->common_model->delete_subject("products", "id", $id);

			$this->session->set_flashdata("status", "تمت العملية بنجاح");
			redirect($_SERVER['HTTP_REFERER']);
		}
	}

}


/* End of file products.php */
/* Location: ./application/modules/products/controllers/products.php */
