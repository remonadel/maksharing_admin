<?php $this->load->view("header"); ?>
<div class="container">
    <div class="col-md-12">
        <div class="main-title">
            <h1>إضافة سؤال جديد   </h1>
        </div>
    </div>
    <div class="row">
        <div class="masonary-grids">
            <form action="" method="post" id="insert-form">
                <div class="col-md-722">
                    <div class="col-md-122" style="margin-top: -4px;">
                        <div class="widget-area">
                            <?php if (isset($status)): ?>
                                <div class="col-md-122" style="margin-right: 10px;"><?= $status; ?></div>
                            <?php endif; ?>
                            <div class="col-md-122">
                                <div class="inline-form" id="title-div">
                                    <label> السؤال باللغة العربية *</label>
                                    <textarea name="question_ar" style="height: 100px;"><?= htmlspecialchars(trim(@$_POST["question_ar"])); ?></textarea>
                                </div>
                                <div class="inline-form" id="title-div">
                                    <label> السؤال باللغة الإنجليزية *</label>
                                    <textarea name="question_en" style="height: 100px;"><?= htmlspecialchars(trim(@$_POST["question_en"])); ?></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-122">
                        <div class="widget-area">
                            <div class="inline-form">
                                <label>الإجابة باللغة العربية *</label>
                                <textarea name="answer_ar" style="height: 100px;"><?= htmlspecialchars(trim(@$_POST["answer_ar"])); ?></textarea>
                            </div>
                            <div class="inline-form">
                                <label>الإجابة باللغة الإنجليزية *</label>
                                <textarea name="answer_en" style="height: 100px;"><?= htmlspecialchars(trim(@$_POST["answer_en"])); ?></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-122 clear">
                        <div class="widget-area">
                            <div class="col-md-62">
                                <input type="submit" name="submit" value="حفظ" id="submit_form" class="btn btn-success btn-font" style="width: 100px;" />
                            </div>
                        </div>
                    </div>
                </div>


            </form>
        </div>
    </div>
<?php //$this->load->view("slide_panel"); ?>
</div><!-- Page Container -->
<?php $this->load->view("footer"); ?>

</body>
</html>
