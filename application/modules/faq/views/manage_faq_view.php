<?php $this->load->view("header"); ?>

<div class="container">
    <div class="col-md-12" style="margin-bottom: 20px;">
        <div class="col-md-62">
            <div class="main-title">
                    <h1> ادارة الأسئلة الشائعة</h1>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="masonary-grids">
            <div class="col-md-12">
                <div class="widget-area" style="margin-top: 0;">
                    <?php if ($this->session->flashdata("status")): ?>
                        <div class="col-md-122" id="status" style="background-color: #EEE; padding: 10px; margin-bottom: 25px; margin-top: -10px;"><p class="success-msg"><?= $this->session->flashdata("status"); ?></p></div>
                    <?php endif; ?>

                    <div class="streaming-table" style="margin-top: 0px;">
                        <span id="found" class="label label-info"></span>
                        <table id="stream_table" class='table table-striped table-bordered test'>
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>السؤال باللغة العربية</th>
                                    <th>السؤال باللغة الإنجليزية</th>
                                    <th>الإجابة باللغة العربية</th>
                                    <th>الإجابة باللغة الإنجليزية</th>
                                    <th>التحكم</th>
                                </tr>
                            </thead>
                            <tbody class="tbody_admin">
                                <?php if (isset($questions)): ?>
                                <?php foreach ($questions as $question): ?>
                                    <tr class="tr_<?= $question["id"]; ?>">
                                        <td><?= $question["id"]; ?></td>
                                        <td>
                                            <?= $question["question_ar"]; ?>
                                        </td>
                                        <td>
                                            <?= $question["question_en"]; ?>
                                        </td>
                                        <td>
                                            <?= $question["answer_ar"]; ?>
                                        </td>
                                        <td>
                                            <?= $question["answer_en"]; ?>
                                        </td>
                                        <td>
                                            <a href="<?= site_url(); ?>faq/edit/<?= $question['id']; ?>">
                                                <button class="btn btn-warning btn-font new-width" type="button">تعديل</button>
                                            </a>
                                            <a onclick="alertDelete('faq/delete/<?= $question['id']; ?>', 'هل أنت متأكد من حذف هذا السؤال؟');" href="javascript:void(null);">
                                                <button class="btn btn-danger btn-font new-width" type="button" >حذف</button>
                                            </a>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                                <?php endif; ?>
                            </tbody>
                        </table>
                    </div>

                    <?php if (isset($pagination)): ?>
                        <div class="pagination-news">
                            <?= $pagination; ?>
                        </div>
                    <?php endif; ?>

                    <!-- Empty modal -->
                    <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content"></div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
<?php //$this->load->view("slide_panel"); ?>
</div><!-- Page Container -->
<?php $this->load->view("footer"); ?>

</body>
</html>
