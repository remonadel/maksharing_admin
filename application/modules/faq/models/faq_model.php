<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Faq_model extends CI_Model {
    
    public function __construct()
    {
        parent::__construct();
    }

	
    public function insert_question($question_ar, $question_en, $answer_ar, $answer_en)
    {
        $sql = "INSERT INTO `faq` (`question_ar`, `question_en`, `answer_ar`, `answer_en`)
                VALUES (?, ?, ?, ?)";
				
        $query = $this->db->query($sql, array($question_ar, $question_en, $answer_ar, $answer_en));
    }
	
    
    public function edit_question($question_ar, $question_en, $answer_ar, $answer_en, $id)
    {
			$sql = "UPDATE `faq` SET `question_ar` = ?, `question_en` = ?, `answer_ar` = ?, `answer_en` = ? WHERE `id` = ?";
			
			$query = $this->db->query($sql, array($question_ar, $question_en, $answer_ar, $answer_en, $id));

    }
	

	public function get_questions()
	{
		$sql = "SELECT * FROM `faq` ORDER BY id DESC";
		$query = $this->db->query($sql, array());
		
		return ($query->num_rows() >= 1) ? $query->result_array() : FALSE;
	}
	

}


/* End of file products_model.php */
/* Location: ./application/modules/products/models/products_model.php */