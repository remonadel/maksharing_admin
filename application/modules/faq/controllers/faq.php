<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Faq extends MX_Controller {

	public function __construct()
    {
        parent::__construct();
		$this->deny->deny_if_logged_out();

		$this->load->model("faq_model");
    }


	public function index()
	{
		$authorized = $this->common_model->authorized_to_view_page("products");
		if ($authorized)
		{
			$data = array();

			$questions = $this->faq_model->get_questions();
			if ($questions)
			{
				$data["questions"] = $questions;
			}

			$this->load->view("manage_faq_view", $data);
		}
	}

	public function add()
	{
        $authorized = $this->common_model->authorized_to_view_page("products");
		if ($authorized)
		{
			$data = array();

			if (isset($_POST["submit"]))
			{
				$question_ar = htmlspecialchars(trim($_POST["question_ar"]));
				$question_en = htmlspecialchars(trim($_POST["question_en"]));
				$answer_ar = htmlspecialchars(trim($_POST["answer_ar"]));
				$answer_en = htmlspecialchars(trim($_POST["answer_en"]));

				if ((empty($question_ar) AND empty($answer_ar)) AND (empty($question_en) OR empty($answer_en) ))
				{
					$data["status"] = "<p class='error-msg'>يجب إدخال السؤال بلغة واحدة علي الأقل  </p>";
				}
				else
				{
					// Insert information into database
					$this->faq_model->insert_question($question_ar, $question_en, $answer_ar, $answer_en);

					$this->session->set_flashdata("status", "تمت العملية بنجاح");
					$redirect_url =  site_url() . "faq" ;
					redirect($redirect_url);
				}
			}

			$this->load->view("add_faq_view", $data);
		}
	}

	public function edit($id = "")
	{
        $authorized = $this->common_model->authorized_to_view_page("products");
		if ($authorized)
		{
			$faq = $this->common_model->get_subject_with_token("faq", "id", $id);
			if (empty($id) OR ! $faq) show_404();

			$data["faq"] = $faq;

			if (isset($_POST["submit"]))
			{
				$question_ar = htmlspecialchars(trim($_POST["question_ar"]));
				$question_en = htmlspecialchars(trim($_POST["question_en"]));
				$answer_ar = htmlspecialchars(trim($_POST["answer_ar"]));
				$answer_en = htmlspecialchars(trim($_POST["answer_en"]));

				if ((empty($question_ar) AND empty($answer_ar)) AND (empty($question_en) OR empty($answer_en) ))
				{
					$data["status"] = "<p class='error-msg'>يجب إدخال السؤال بلغة واحدة علي الأقل  </p>";
				}
				else
				{
					// Edit information on database
					$this->faq_model->edit_question($question_ar, $question_en, $answer_ar, $answer_en, $id);

					$this->session->set_flashdata("status", "تمت العملية بنجاح");
					$redirect_url =  site_url() . "faq" ;
					redirect($redirect_url);
				}
			}

			$this->load->view("edit_faq_view", $data);
		}
	}


	public function delete($id = "")
    {
		$authorized = $this->common_model->authorized_to_view_page("products");
		if ($authorized)
		{
			$faq = $this->common_model->subject_exists("faq", "id", $id);
			if (empty($id) OR ! $faq) show_404();

			$this->common_model->delete_subject("faq", "id", $id);

			$this->session->set_flashdata("status", "تمت العملية بنجاح");
			redirect($_SERVER['HTTP_REFERER']);
		}
	}

}


/* End of file products.php */
/* Location: ./application/modules/products/controllers/products.php */
