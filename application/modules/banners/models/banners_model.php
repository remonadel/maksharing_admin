<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Banners_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }


    public function get_main_slider_banners()
	{
		$sql = "SELECT * FROM `banners` WHERE id <=4";
		$query = $this->db->query($sql, array());

		return ($query->num_rows() >= 1) ? $query->result_array() : FALSE;
	}

    public function get_secondary_slider_banners()
    {
        $sql = "SELECT * FROM `banners` WHERE id > 4 AND id <= 11";
        $query = $this->db->query($sql, array());

        return ($query->num_rows() >= 1) ? $query->result_array() : FALSE;
    }

    public function get_special_slider_banners()
    {
        $sql = "SELECT * FROM `banners` WHERE id > 11";
        $query = $this->db->query($sql, array());

        return ($query->num_rows() >= 1) ? $query->result_array() : FALSE;
    }

    public function edit_banner_link($desc_en, $desc_ar, $btn_text_en, $btn_text_ar, $link, $banner_id)
    {
		$sql = "UPDATE `banners` SET `description_en` = ?, `description_ar` = ?, `btn_text_en` = ?, `btn_text_ar` = ?
                , `btn_link` = ? WHERE `id` = ?";
		$query = $this->db->query($sql, array($desc_en, $desc_ar, $btn_text_en, $btn_text_ar, $link, $banner_id));
    }


    public function edit_banner_image($image, $id)
    {
		$sql = "UPDATE `banners` SET `image` = ? WHERE `id` = ?";
		$query = $this->db->query($sql, array($image, $id));
    }

}


/* End of file banners_model.php */
/* Location: ./application/modules/banners/models/banners_model.php */
