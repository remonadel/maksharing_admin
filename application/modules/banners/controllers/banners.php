<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Banners extends MX_Controller {

	public function __construct()
    {
        parent::__construct();
		$this->deny->deny_if_logged_out();

		$this->load->model("banners_model");
    }


	public function index()
	{
		$authorized = $this->common_model->authorized_to_view_page("banners");
		if ($authorized)
		{
			$data = array();

			$banners = $this->banners_model->get_main_slider_banners();
			if ($banners) $data["banners"] = $banners;

			if (isset($_POST["submit"]))
			{
				$banner_id = $_POST["banner_id"];
				$image_name = basename($_FILES["banner_image"]["name"]);
				$desc_en = (empty($_POST["description_en"])) ? NULL : htmlspecialchars(trim($_POST["description_en"]));
				$desc_ar = (empty($_POST["description_ar"])) ? NULL : htmlspecialchars(trim($_POST["description_ar"]));
				$btn_text_en = (empty($_POST["btn_text_en"])) ? NULL : htmlspecialchars(trim($_POST["btn_text_en"]));
				$btn_text_ar = (empty($_POST["btn_text_ar"])) ? NULL : htmlspecialchars(trim($_POST["btn_text_ar"]));
				$link = (empty($_POST["btn_link"])) ? NULL : htmlspecialchars(trim($_POST["btn_link"]));
				$success = TRUE;

				$this->banners_model->edit_banner_link($desc_en, $desc_ar, $btn_text_en, $btn_text_ar, $link, $banner_id);

				if ( ! empty($image_name))
				{
					$tmp_name = $_FILES["banner_image"]["tmp_name"];
					$allowed_exts = array("jpg");
					$a = explode(".", $image_name);
					$file_ext = strtolower(end($a)); unset($a);
					$path = BANNERS_PATH;

					// Restricting file uploading to image files only
					if ( ! in_array($file_ext, $allowed_exts))
					{
						$data["status"] = "<p class='error-msg'>لقد قمت برفع ملف غير مسموح به</p>";
						$success = FALSE;
					}
					else
					{
						// Success. Give the file a unique name and upload it, then insert data in database
						$cur_time = time();
						$image = $cur_time . "." . $file_ext;
						move_uploaded_file($tmp_name, $path . $image);

						// Get old banner name to delete it later
						$old_banner_name = $this->common_model->get_info_by_token("banners", "image", "id", $banner_id);

						$this->banners_model->edit_banner_image($image, $banner_id);

						// Delete old banner
						if (file_exists($path . $old_banner_name)) unlink($path . $old_banner_name);
					}
				}

				if ($success)
				{
					$this->session->set_flashdata("status", "<p class='success-msg'>تم تعديل البانر بنجاح</p</p>");
					redirect(site_url() . "banners");
				}
			}

			$this->load->view("edit_banners_view", $data);
		}
	}

	public function secondary()
	{
		$authorized = $this->common_model->authorized_to_view_page("banners");
		if ($authorized)
		{
			$data = array();

			$banners = $this->banners_model->get_secondary_slider_banners();
			if ($banners) $data["banners"] = $banners;

			if (isset($_POST["submit"]))
			{
				$banner_id = $_POST["banner_id"];
				$image_name = basename($_FILES["banner_image"]["name"]);
				$banner_link = (empty($_POST["banner_link"])) ? NULL : htmlspecialchars(trim($_POST["banner_link"]));
				$success = TRUE;

				$this->banners_model->edit_banner_link($banner_link, $banner_id);

				if ( ! empty($image_name))
				{
					$tmp_name = $_FILES["banner_image"]["tmp_name"];
					$allowed_exts = array("jpg");
					$a = explode(".", $image_name);
					$file_ext = strtolower(end($a)); unset($a);
					$path = BANNERS_PATH;

					// Restricting file uploading to image files only
					if ( ! in_array($file_ext, $allowed_exts))
					{
						$data["status"] = "<p class='error-msg'>لقد قمت برفع ملف غير مسموح به</p>";
						$success = FALSE;
					}
					else
					{
						// Success. Give the file a unique name and upload it, then insert data in database
						$cur_time = time();
						$image = $cur_time . "." . $file_ext;
						move_uploaded_file($tmp_name, $path . $image);

						// Get old banner name to delete it later
						$old_banner_name = $this->common_model->get_info_by_token("banners", "image", "id", $banner_id);

						$this->banners_model->edit_banner_image($image, $banner_id);

						// Delete old banner
						if (file_exists($path . $old_banner_name)) unlink($path . $old_banner_name);
					}
				}

				if ($success)
				{
					$this->session->set_flashdata("status", "<p class='success-msg'>تم تعديل البانر بنجاح</p</p>");
					redirect(site_url() . "banners/secondary");
				}
			}

			$this->load->view("edit_banners_view2", $data);
		}
	}

	public function special()
	{
		$authorized = $this->common_model->authorized_to_view_page("banners");
		if ($authorized)
		{
			$data = array();

			$banners = $this->banners_model->get_special_slider_banners();
			if ($banners) $data["banners"] = $banners;

			if (isset($_POST["submit"]))
			{
				$banner_id = $_POST["banner_id"];
				$image_name = basename($_FILES["banner_image"]["name"]);
				$banner_link = (empty($_POST["banner_link"])) ? NULL : htmlspecialchars(trim($_POST["banner_link"]));
				$success = TRUE;

				$this->banners_model->edit_banner_link($banner_link, $banner_id);

				if ( ! empty($image_name))
				{
					$tmp_name = $_FILES["banner_image"]["tmp_name"];
					$allowed_exts = array("jpg");
					$a = explode(".", $image_name);
					$file_ext = strtolower(end($a)); unset($a);
					$path = BANNERS_PATH;

					// Restricting file uploading to image files only
					if ( ! in_array($file_ext, $allowed_exts))
					{
						$data["status"] = "<p class='error-msg'>لقد قمت برفع ملف غير مسموح به</p>";
						$success = FALSE;
					}
					else
					{
						// Success. Give the file a unique name and upload it, then insert data in database
						$cur_time = time();
						$image = $cur_time . "." . $file_ext;
						move_uploaded_file($tmp_name, $path . $image);

						// Get old banner name to delete it later
						$old_banner_name = $this->common_model->get_info_by_token("banners", "image", "id", $banner_id);

						$this->banners_model->edit_banner_image($image, $banner_id);

						// Delete old banner
						if (file_exists($path . $old_banner_name)) unlink($path . $old_banner_name);
					}
				}

				if ($success)
				{
					$this->session->set_flashdata("status", "<p class='success-msg'>تم تعديل البانر بنجاح</p</p>");
					redirect(site_url() . "banners/special");
				}
			}

			$this->load->view("edit_banners_view2", $data);
		}
	}

}


/* End of file banners.php */
/* Location: ./application/modules/banners/controllers/banners.php */
