<?php $this->load->view("header"); ?>

<div class="container">
	<div class="col-md-12">
		<div class="main-title">
			<h1> تعديل السلايدر الرئيسي</h1>
		</div>
	</div>
	<div class="row">
		<div class="masonary-grids">
			<div class="col-md-12">
				<div class="widget-area">
					<?php if (isset($status)): ?>
						<div class="col-md-122" style="margin-bottom: 10px; margin-top: -10px;"><?= $status; ?></div>
					<?php endif; ?>
					<?php if ($this->session->flashdata("status")): ?>
						<div class="col-md-122" style="margin-bottom: 10px;"><?= $this->session->flashdata("status"); ?></div>
					<?php endif; ?>
					<?php if (isset($banners)): ?>
                    <?php $i=1; foreach ($banners as $banner): ?>
    					<div class="col-md-62" style="padding: 10px; padding-right: 20px; border: 1px solid grey;">
                            <form action="" method="post" enctype="multipart/form-data">
                                <li style="margin-bottom: 16px;">البنر <?= $i; ?></li>
                                <div style="width: 350px; margin: 0 auto;">
                                    <img src="<?= BANNERS . $banner['image']; ?>" style="width: 350px; height: 200px;" />
                                </div>
                                <input type="hidden" name="banner_id" value="<?= $banner['id']; ?>" />
                                <div class="inline-form">
        							<label class="c-label">الصورة *</label>
        							<input type="file" name="banner_image" accept=".jpg" title="يجب ان تكون الصورة jpg" />
        						</div>
                                <div class="inline-form">
        							<label class="c-label">الوصف باللغة الإنجليزية</label>
        							<input type="text" name="description_en" value="<?= $banner['description_en']; ?>" />
        						</div>
								<div class="inline-form">
									<label class="c-label">الوصف باللغة العربية</label>
									<input type="text" name="description_ar" value="<?= $banner['description_ar']; ?>" />
								</div>
								<div class="inline-form">
									<label class="c-label"> اسم الزر باللغة الإنجليزية</label>
									<input type="text" name="btn_text_en" value="<?= $banner['btn_text_en']; ?>" />
								</div>
								<div class="inline-form">
									<label class="c-label">اسم الزر باللغة العربية</label>
									<input type="text" name="btn_text_ar" value="<?= $banner['btn_text_ar']; ?>" />
								</div>
								<div class="inline-form">
									<label class="c-label">رابط الزر</label>
									<input type="text" name="btn_link" value="<?= $banner['btn_link']; ?>" />
								</div>
                                <div class="col-md-122" style="margin-top: 10px;">
                                    <input type="submit" name="submit" value="تعديل" class="btn btn-success btn-font" style="width: 100px;" />
                                </div>
                            </form>
    					</div>
                    <?php $i++; endforeach;  ?>
				<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
	<?php //$this->load->view("slide_panel"); ?>
</div><!-- Page Container -->
<?php $this->load->view("footer"); ?>
</body>
</html>
