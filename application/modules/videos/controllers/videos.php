<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Videos extends MX_Controller {

	public function __construct()
    {
        parent::__construct();
		$this->deny->deny_if_logged_out();

		$this->load->model("videos_model");
    }


	public function index()
	{
		$authorized = $this->common_model->authorized_to_view_page("videos");
		if ($authorized)
		{
			$data = array();

			$current_page = (int) $this->uri->segment(3);
			$per_page = 20;
			$videos_count = $this->videos_model->get_videos_count();

			$config["base_url"] = site_url() . "videos/index";
			$config['uri_segment'] = 3;
			$config["total_rows"] = $videos_count;
			$config["per_page"] = $per_page;
			$this->pagination->initialize($config);

			$videos = $this->videos_model->get_videos($current_page, $per_page);
			if ($videos)
			{
				$data["videos"] = $videos;
				$data["pagination"] = $this->pagination->create_links();
			}

			$this->load->view("manage_videos_view", $data);
		}
	}

	public function add()
	{
        $authorized = $this->common_model->authorized_to_view_page("videos");
		if ($authorized)
		{
			$data = array();
			if (isset($_POST["submit"]))
			{
				$link = htmlspecialchars(trim($_POST["link"]));
				$name_ar = htmlspecialchars(trim($_POST["name_ar"]));
				$name_en = htmlspecialchars(trim($_POST["name_en"]));
				if (empty($link) OR empty($name_ar) OR empty($name_en))
				{
					$data["status"] = "<p class='error-msg'>يجب إدخال جميع البيانات الإجبارية</p>";
				}
				else
				{
					// Insert information into database
					$this->videos_model->insert_video($name_ar, $name_en, $link);

					$this->session->set_flashdata("status", "تمت العملية بنجاح");
					redirect(site_url() . "videos");
				}
			}

			$this->load->view("add_video_view", $data);
		}
	}

	public function edit($id = "")
	{
        $authorized = $this->common_model->authorized_to_view_page("videos");
		if ($authorized)
		{
			$video = $this->common_model->get_subject_with_token("videos", "id", $id);
			if (empty($id) OR ! $video) show_404();

			$data["video"] = $video;

			if (isset($_POST["submit"]))
			{

				$link = htmlspecialchars(trim($_POST["link"]));
				$name_ar = htmlspecialchars(trim($_POST["name_ar"]));
				$name_en = htmlspecialchars(trim($_POST["name_en"]));
				if (empty($name_ar) OR empty($link) OR empty($name_en))
				{
					$data["status"] = "<p class='error-msg'>يجب إدخال جميع البيانات الإجبارية</p>";
				}
				else
				{
					// Edit information on database
					$this->videos_model->edit_video($name_ar, $name_en, $link, $id);

					$this->session->set_flashdata("status", "تمت العملية بنجاح");
					redirect(site_url() . "videos");
				}
			}

			$this->load->view("edit_video_view", $data);
		}
	}


	public function delete($id = "")
    {
		$authorized = $this->common_model->authorized_to_view_page("videos");
		if ($authorized)
		{
			$video_exists = $this->common_model->subject_exists("videos", "id", $id);
			if (empty($id) OR ! $video_exists) show_404();

			$this->common_model->delete_subject("videos", "id", $id);

			$this->session->set_flashdata("status", "تمت العملية بنجاح");
			redirect($_SERVER['HTTP_REFERER']);
		}
	}

}


/* End of file videos.php */
/* Location: ./application/modules/videos/controllers/videos.php */
