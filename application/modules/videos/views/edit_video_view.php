<?php $this->load->view("header"); ?>
<div class="container">
    <div class="col-md-12">
        <div class="main-title">
            <h1>تعديل فيديو</h1>
        </div>
    </div>
    <div class="row">
        <div class="masonary-grids">
            <form action="" method="post" id="insert-form">
                <div class="col-md-722">

                    <div class="col-md-122 clear">
                        <div class="widget-area">
                            <div class="col-md-122">
                                <div class="inline-form">
                                    <label>الاسم باللغة العربية *</label>
                                    <input type="text" name="name_ar"
                                           value="<?= htmlspecialchars(trim(@$video["name_ar"])); ?>" required />
                                </div>
                                <div class="inline-form">
                                    <label>الاسم باللغة الانجليزية *</label>
                                    <input type="text" name="name_en"
                                           value="<?= htmlspecialchars(trim(@$video["name_en"])); ?>" required />
                                </div>
                                <div class="inline-form">
                                    <label>الرابط *</label>
                                    <input type="text" name="link" id="link"
                                           value="<?= htmlspecialchars(trim(@$video["link"])); ?>" required />
                                </div>
                            </div>
                            <div class="col-md-62">
                                <input type="submit" name="submit" value="حفظ" id="submit_form" class="btn btn-success btn-font" style="width: 100px;" />
                            </div>
                        </div>
                    </div>
                </div>

            </form>
        </div>
    </div>
<?php //$this->load->view("slide_panel"); ?>
</div><!-- Page Container -->
<?php $this->load->view("footer"); ?>
<script>
    $(document).ready(function () {
        // Author autocomplete
        var users = [
            <?php if (isset($users)): ?>
                <?php foreach ($users as $user): ?>
                    <?= "\"" . $user["name"] . "\"" . ","; ?>
                <?php endforeach; ?>
            <?php endif; ?>
        ];

        $("#author").autocomplete({source: users});


        this_window_title = window.document.title;

        // Choosing main image code
        $("#add_main_image_btn").click(function () {
            images_window = window.open("<?= site_url(); ?>images/list_images", "_blank", "titlebar=no, toolbar=no, location=no, status=no, menubar=no, scrollbars=yes, resizable=no, top=0, left=0, width=1050, height=" + window.innerHeight);
            var timer = setInterval(check_window_close, 500);

            function check_window_close() {
                image = images_window.document.title;
                if (images_window.closed)
                {
                    // Only do any action if he chooses an image from the opened window and doesn't just close it again without choosing
                    if (image != this_window_title)
                    {
                        // Stop the timer
                        clearInterval(timer);

                        var result = image.split('&');

                        // Set the hidden input value to the video name
                        $("#main_image").val(result[1]);
                        var html = "<img src='<?= IMG_ARCHIVE; ?>311x253/" + result[1] + "' width='220' height='170'>";

                        $('#main-media-div').html(html);
                        $(".del_btn").css('display', 'block');
                    }
                }
            }
        });
    });


    function remove_content() {
        $("#main-media-div").html("");
        $("#main_image").val("");
        $("#add_main_image_btn").removeAttr('disabled');
        $(".del_btn").css('display', 'none');
    }


    $("#submit_form").click(function (e) {
        // Form validations
        var title = $("#title").val().trim();
        var link = $("#link").val().trim();

        if ( ! title)
        {
            e.preventDefault();
            swal("يجب إدخال عنوان الفيديو");
        }
        else if ( ! link)
        {
            e.preventDefault();
            swal("يجب إدخال رابط الفيديو");
        }
        else if ( ! $("input[name=main_image]").val())
        {
            e.preventDefault();
            swal("يرجي إختيار صورة للفيديو");
        }
    });
</script>
</body>
</html>
