<div class="widget-area" style="margin-top: 0px;">
	<div class="col-md-122" style="padding: 20px; margin-bottom: 30px; background-color: #EEE;">
		<div class="col-md-62">
			<?php if ($video["published"] == 1): ?>
				<h4>
					<span class="fon-bo-ba">الرابط: </span>
					<span class="fon-rg-ba"><a target="_blank" href="http://www.electricity.com/videos/details/<?= $video["id"]; ?>">مشاهدة الفيديو</a></span>
				</h4>
			<?php endif; ?>
			<?php if ( ! is_null($video["author"])): ?>
				<h4>
					<span class="fon-bo-ba">كتب: </span>
					<span class="fon-rg-ba"><?= $video["author"]; ?></span>
				</h4>
			<?php endif; ?>
			<h4>
				<span class="fon-bo-ba">أُدخل بواسطة: </span>
				<span class="fon-rg-ba"><?= $video["created_by_name"]; ?></span>
			</h4>
			<h4>
				<span class="fon-bo-ba">تاريخ الإدخال: </span>
				<span class="fon-rg-ba"><?= date("Y-m-d H:i:s", strtotime($video["created_at"])); ?></span>
			</h4>
		<?php if ($video["published"] == 1): ?>
			<h4>
				<span class="fon-bo-ba">نُشر بواسطة: </span>
				<span class="fon-rg-ba"><?= $video["published_by_name"]; ?></span>
			</h4>
			<h4>
				<span class="fon-bo-ba">تاريخ النشر: </span>
				<span class="fon-rg-ba"><?= date("Y-m-d H:i:s", strtotime($video["published_at"])); ?></span>
			</h4>
		<?php endif; ?>
		</div>
		<div class="col-md-62" style="position: relative; left: 75px;">
			<iframe frameborder="0" width="480" height="270" src="https://www.youtube.com/embed/<?= str_replace("https://youtu.be/", "", $video['link']); ?>" allowfullscreen></iframe>
		</div>
	</div>
	<div class="fon-bo-y">
        <h3 style="width: 90%; margin-bottom: 15px; font-family: font_bo; color: #cc0000"><?= $video["title"]; ?></h3>
		<?php if ( ! is_null($video["description"])) echo $video["description"]; ?>
	</div>
</div>
<script>
    $(".fon-bo-y p").addClass("p-description");
</script>
