<?php $this->load->view("header"); ?>
<div class="container">
    <div class="col-md-12">
        <div class="main-title">
            <h1>إضافة فيديو جديد</h1>
        </div>
    </div>
    <div class="row">
        <div class="masonary-grids">
            <form action="" method="post" id="insert-form">
                <div class="col-md-722">

                    <div class="col-md-122 clear">
                        <div class="widget-area">
                            <div class="col-md-122">
                                <div class="inline-form">
                                    <label>الاسم باللغة العربية *</label>
                                    <input type="text" name="name_ar"
                                           value="<?= htmlspecialchars(trim(@$_POST["name_ar"])); ?>" required />
                                </div>
                                <div class="inline-form">
                                    <label>الاسم باللغة الانجليزية *</label>
                                    <input type="text" name="name_en"
                                           value="<?= htmlspecialchars(trim(@$_POST["name_en"])); ?>" required />
                                </div>
                                <div class="inline-form">
                                    <label>الرابط *</label>
                                    <input type="text" name="link" id="link"
                                           value="<?= htmlspecialchars(trim(@$_POST["link"])); ?>" required />
                                </div>
                            </div>
                            <div class="col-md-62">
                                <input type="submit" name="submit" value="حفظ" id="submit_form" class="btn btn-success btn-font" style="width: 100px;" />
                            </div>
                        </div>
                    </div>
                </div>

            </form>
        </div>
    </div>
<?php //$this->load->view("slide_panel"); ?>
</div><!-- Page Container -->
<?php $this->load->view("footer"); ?>

</body>
</html>
