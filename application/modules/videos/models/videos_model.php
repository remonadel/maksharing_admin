<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Videos_model extends CI_Model {
    
    public function __construct()
    {
        parent::__construct();
    }
	
    public function insert_video($name_ar, $name_en, $link)
    {
        $sql = "INSERT INTO `videos` (`name_ar`, `name_en`, `link`)
                VALUES (?,?,?)";
				
        $query = $this->db->query($sql, array($name_ar, $name_en, $link));
    }
	
    
    public function edit_video($name_ar, $name_en, $link, $id)
    {

		$sql = "UPDATE `videos` SET `name_ar` = ?, `name_en` = ?, `link` = ? WHERE `id` = ?";

		$query = $this->db->query($sql, array($name_ar, $name_en, $link, $id));

    }
	
	
	public function get_videos_count()
	{
		$sql = "SELECT COUNT(`id`) AS `count` FROM `videos`";
        $query = $this->db->query($sql, array());
        $count = $query->row_array();
        return $count["count"];
	}
	
	
	public function get_videos($offset, $limit)
	{
		$sql = "SELECT * FROM `videos` ORDER BY `created_at` DESC LIMIT ?, ?";
		$query = $this->db->query($sql, array($offset, $limit));
		
		return ($query->num_rows() >= 1) ? $query->result_array() : FALSE;
	}

	
}


/* End of file videos_model.php */
/* Location: ./application/modules/videos/models/videos_model.php */