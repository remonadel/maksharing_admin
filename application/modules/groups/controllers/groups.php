<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Groups extends MX_Controller {

	public function __construct()
    {
        parent::__construct();
		$this->deny->deny_if_logged_out();

		$this->load->model("groups_model");
    }


	public function index()
	{
		$authorized = $this->common_model->authorized_to_view_page("users");
		if ($authorized)
		{
			$data = array();

			$current_page = (int) $this->uri->segment(2);
			$per_page = 20;
			$groups_count = $this->common_model->get_table_rows_count("groups");

			$config["base_url"] = site_url() . "groups/";
			$config['uri_segment'] = 2;
			$config["total_rows"] = $groups_count;
			$config["per_page"] = $per_page;
			$this->pagination->initialize($config);

			$groups = $this->groups_model->get_groups($current_page, $per_page);
			if ($groups)
			{
				$data["groups"] = $groups;
				$data["pagination"] = $this->pagination->create_links();
			}

			if (isset($_POST["search"]))
			{
				$query = htmlspecialchars(trim($_POST["search"]));
				redirect(site_url() . "groups/search/$query");
			}

			$this->load->view("manage_groups_view", $data);
		}
	}


	public function search($query = "")
	{
		$authorized = $this->common_model->authorized_to_view_page("users");
		if ($authorized)
		{
			if (empty($query)) redirect(site_url() . "groups");
			$query = urldecode($query);

			$data = array();

			$current_page = (int) $this->uri->segment(4);
			$per_page = 20;
			$groups_count = $this->common_model->get_search_rows_count("groups", "name", $query);

			$config["base_url"] = site_url() . "groups/search/$query";
			$config['uri_segment'] = 4;
			$config["total_rows"] = $groups_count;
			$config["per_page"] = $per_page;
			$this->pagination->initialize($config);

			$groups = $this->groups_model->search_groups("name", $query, $current_page, $per_page);
			if ($groups)
			{
				$data["groups"] = $groups;
				$data["pagination"] = $this->pagination->create_links();
			}

			if (isset($_POST["search"]))
			{
				$query = htmlspecialchars(trim($_POST["search"]));
				redirect(site_url() . "groups/search/$query");
			}

			$this->load->view("manage_groups_view", $data);
		}
	}


	public function add()
	{
		$authorized = $this->common_model->authorized_to_view_page("users");
		if ($authorized)
		{
			$data = array();

			if (isset($_POST["submit"]))
			{
				$name = htmlspecialchars(trim($_POST["name"]));
				$banners = (isset($_POST["banners"])) ? 1 : 0;
				$pdfs = (isset($_POST["pdfs"])) ? 1 : 0;
				$products = (isset($_POST["products"])) ? 1 : 0;
				$companies = (isset($_POST["companies"])) ? 1 : 0;
				$news = (isset($_POST["news"])) ? 1 : 0;
				$videos = (isset($_POST["videos"])) ? 1 : 0;
				$images = (isset($_POST["images"])) ? 1 : 0;
				$users = (isset($_POST["users"])) ? 1 : 0;

				if (empty($name))
				{
					$data["status"] = "<p class='error-msg'>يجب إدخال إسم المجموعة</p>";
				}
				elseif ($this->common_model->subject_exists("groups", "name", $name))
				{
					$data["status"] = "<p class='error-msg'>هذة المجموعة موجودة بالفعل</p>";
				}
				else
				{
					$this->groups_model->insert_group($name, $banners, $pdfs, $products, $companies,
													  $news, $videos, $images, $users);

					$this->session->set_flashdata("status", "تمت العملية بنجاح");
					redirect(site_url() . "groups");
				}
			}

			$this->load->view("add_group_view", $data);
		}
	}


	public function view($id)
	{
		$authorized = $this->common_model->authorized_to_view_page("users");
		if ($authorized)
		{
			$group = $this->common_model->get_subject_with_token("groups", "id", $id);
			if (empty($id) OR ! $group) show_404();

			$data["group"] = $group;

			$this->load->view("ajax_view_group_view", $data);
		}
	}


	public function edit($id)
	{
		$authorized = $this->common_model->authorized_to_view_page("users");
		if ($authorized)
		{
			$group = $this->common_model->get_subject_with_token("groups", "id", $id);
			if (empty($id) OR ! $group) show_404();

			$data["group"] = $group;

			if (isset($_POST["submit"]))
			{
				$name = htmlspecialchars(trim($_POST["name"]));
				$banners = (isset($_POST["banners"])) ? 1 : 0;
				$pdfs = (isset($_POST["pdfs"])) ? 1 : 0;
				$products = (isset($_POST["products"])) ? 1 : 0;
				$companies = (isset($_POST["companies"])) ? 1 : 0;
				$news = (isset($_POST["news"])) ? 1 : 0;
				$videos = (isset($_POST["videos"])) ? 1 : 0;
				$images = (isset($_POST["images"])) ? 1 : 0;
				$users = (isset($_POST["users"])) ? 1 : 0;

				if (empty($name))
				{
					$data["status"] = "<p class='error-msg'>يجب إدخال إسم المجموعة</p>";
				}
				elseif ($name != $group["name"] &&
				 		$this->common_model->subject_exists("groups", "name", $name))
				{
					$data["status"] = "<p class='error-msg'>هذة المجموعة موجودة بالفعل</p>";
				}
				else
				{
					$this->groups_model->update_group($name, $banners, $pdfs, $products, $companies,
					 								  $news, $videos, $images, $users, $id);

					$this->session->set_flashdata("status", "تمت العملية بنجاح");
					redirect(site_url() . "groups");
				}
			}

			$this->load->view("edit_group_view", $data);
		}
	}


	public function delete($id = "")
    {
		$authorized = $this->common_model->authorized_to_view_page("users");
		if ($authorized)
		{
			$group = $this->common_model->get_subject_with_token("groups", "id", $id);
			if (empty($id) OR ! $group) show_404();

			$this->common_model->delete_subject("groups", "id", $id);

			$this->session->set_flashdata("status", "تمت العملية بنجاح");
			redirect(site_url() . "groups");
		}
    }

}


/* End of file groups.php */
/* Location: ./application/modules/groups/controllers/groups.php */
