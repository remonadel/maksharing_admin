<div class="widget-area" style="margin-top: 0px;">
	<h1 style="margin-top: 0px; margin-bottom: 20px;"><?= $group["name"]; ?></h1>
	<div class="col-md-122" style="margin-bottom: 10px;">
		<h3>- الصلاحيات:</h3>
		<h4><input type="checkbox" disabled <?php if ($group["banners"] == 1) echo "checked"; ?> /> البنرات</h4>
		<h4><input type="checkbox" disabled <?php if ($group["pdfs"] == 1) echo "checked"; ?> /> PDFS</h4>
		<h4><input type="checkbox" disabled <?php if ($group["products"] == 1) echo "checked"; ?> /> المنتجات</h4>
		<h4><input type="checkbox" disabled <?php if ($group["companies"] == 1) echo "checked"; ?> /> الشركات</h4>
		<h4><input type="checkbox" disabled <?php if ($group["news"] == 1) echo "checked"; ?> /> الأخبار</h4>
		<h4><input type="checkbox" disabled <?php if ($group["videos"] == 1) echo "checked"; ?> /> الفيديوهات</h4>
		<h4><input type="checkbox" disabled <?php if ($group["images"] == 1) echo "checked"; ?> /> الصور</h4>
		<h4><input type="checkbox" disabled <?php if ($group["users"] == 1) echo "checked"; ?> /> المستخدمين</h4>
	</div>
</div>
