<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Groups_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }


    public function insert_group($name, $banners, $pdfs, $products, $companies, $news, $videos,
                                 $images, $users)
    {
        $sql = "INSERT INTO `groups` (`name`, `banners`, `pdfs`, `products`, `companies`, `news`,
                `videos`, `images`, `users`)
                VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";

        $query = $this->db->query($sql, array($name, $banners, $pdfs, $products, $companies, $news,
                                              $videos, $images, $users));
    }


    private function update_permissions_of_users_in_group($banners, $pdfs, $products, $companies,
                                                          $news, $videos, $images, $users, $group_id)
    {
        $sql = "UPDATE `users_permissions` SET `banners` = ?, `pdfs` = ?, `products` = ?,
                `companies` = ?, `news` = ?, `videos` = ?, `images` = ?, `users` = ?
                WHERE `group_id` = ?";

        $query = $this->db->query($sql, array($banners, $pdfs, $products, $companies, $news,
                                              $videos, $images, $users, $group_id));
    }


    public function update_group($name, $banners, $pdfs, $products, $companies, $news, $videos,
                                 $images, $users, $group_id)
    {
        $sql = "UPDATE `groups` SET `name` = ?, `banners` = ?, `pdfs` = ?, `products` = ?,
                `companies` = ?, `news` = ?, `videos` = ?, `images` = ?, `users` = ?
                WHERE `id` = ?";

        $query = $this->db->query($sql, array($name, $banners, $pdfs, $products, $companies,
                                              $news, $videos, $images, $users, $group_id));

        // Also update permissions of all users with given group
        $this->update_permissions_of_users_in_group($banners, $pdfs, $products, $companies,
                                                    $news, $videos, $images, $users, $group_id);
    }


    public function get_groups($offset, $limit)
    {
        $sql = "SELECT * FROM `groups` ORDER BY `id` DESC LIMIT ?, ?";
        $query = $this->db->query($sql, array($offset, $limit));

        return ($query->num_rows() >= 1) ? $query->result_array() : FALSE;
    }


	public function search_groups($token_type, $token_value, $offset, $limit)
    {
        $sql = "SELECT * FROM `groups` WHERE `$token_type` LIKE CONCAT('%', ?, '%') ORDER BY `id` DESC LIMIT ?, ?";
        $query = $this->db->query($sql, array($token_value, $offset, $limit));

        return ($query->num_rows() >= 1) ? $query->result_array() : FALSE;
    }

}


/* End of file groups_model.php */
/* Location: ./application/modules/groups/models/groups_model.php */
