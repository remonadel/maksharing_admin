<?php $this->load->view("header"); ?>

<div class="container">
    <div class="col-md-12">
		<div class="main-title">
			<h1>الرئيسية</h1>
		</div>
	</div>
        <!--<div class="col-md-12">
            <div class="widget-area pattern">
                <div class="mini-profile-widget">
                    <div class="mini-profile-area">	
                        <span><img src="<?= ASSETS; ?>images/resource/me.jpg" alt=""></span>
                        <h3>Labrina Scholer</h3>
                        <i>Senior Designer</i>
                        <ul class="social-btns">
                            <li><a href="#" title=""><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#" title=""><i class="fa fa-google-plus"></i></a></li>
                            <li><a href="#" title=""><i class="fa fa-twitter"></i></a></li>
                        </ul>
                    </div>
                    <p>Print this page to PDF for the complete set of vectors. Or to use on the desktop.</p>
                </div>
            </div>
        </div>
        <div class="stats-counter-sec">
            <div class="top-margin">
                <div class="row">
                    <div class="col-md-3">
                        <div class="stats-counter">
                            <h3>New Visits</h3>
                            <span>21,069</span>
                            <i class="fa fa-clock-o green"></i>
                            <h6>Total Visits : 9,34,001</h6>
                        </div><!-- Stats Counter -->
                    <!--</div>
                    <div class="col-md-3">
                        <div class="stats-counter">
                            <h3>New Signups</h3>
                            <span>1,346</span>
                            <i class="fa fa-user red"></i>
                            <h6>Total Users : 22,344</h6>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="stats-counter">
                            <div class="ribbon-wrapper"><div class="ribbon-design blue">Today</div></div>
                            <h3>Todays Earning</h3>
                            <span>2,345</span>
                            <i class="fa fa-usd blue"></i>
                            <h6>Total Earning : $345,00</h6>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="stats-counter">
                            <h3>Real Visitors</h3>
                            <span id="random">714</span>
                            <i class="fa  fa-area-chart gray"></i>
                            <h6>Total Visitors : 235,670</h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-5" style="float: right;">
            <div class="widget-area">
                <h2 class="widget-title"><strong>User</strong> Timeline</h2>
                <ul class="panel-function">
                    <li class="dropdown">
                        <a role="button" data-toggle="dropdown" href="#"> <b class="caret"></b></a>
                        <ul class="dropdown-menu" role="menu">
                            <li role="presentation"><a title="" class="hide-btn"><i class="fa fa-minus"></i></a></li>
                            <li role="presentation"><a title="" class="close-sec"><i class="fa fa-close"></i></a></li>
                        </ul>
                    </li>
                </ul>
                <div class="widget">
                    <div class="timeline-sec" id="timeline-scroll" style="width: 737px; padding-right: 10px; outline: none; overflow: hidden;" tabindex="0">
                        <ul>
                            <li>
                                <div class="timeline">
                                    <div class="user-timeline">
                                        <span><img src="<?= ASSETS; ?>images/resource/sender2.jpg" alt=""></span>
                                    </div>
                                    <div class="timeline-detail">
                                        <div class="timeline-head">
                                            <h3>Jonathan Gardel<span>2 min ago</span><i class="red">Admin</i></h3>
                                            <div class="social-share">
                                                <a title=""><i class="fa fa-share-alt"></i></a>
                                                <ul class="social-btns">
                                                    <li><a title="Facebook" data-toggle="tooltip" data-placement="left" href="#"><i class="fa fa-facebook"></i></a></li>
                                                    <li><a title="Google" data-toggle="tooltip" data-placement="left" href="#"><i class="fa fa-google-plus"></i></a></li>
                                                    <li><a title="Twitter" data-toggle="tooltip" data-placement="left" href="#"><i class="fa fa-twitter"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="timeline-content">
                                            <p>Set it as the font <a href="#" title="">John Doe</a> in your applition, and copy and paste the icons. Print this page.</p>
                                            <div class="progress w-tooltip">
                                                <div style="width: 70%;" aria-valuemax="100" aria-valuemin="0" aria-valuenow="70" role="progressbar" class="red progress-bar">
                                                    <span><i>uploading</i>70%</span>
                                                </div>
                                            </div>
                                            <div data-toggle="buttons" class="btn-group btn-group-sm">
                                                <label class="btn btn-default">
                                                    <input type="radio" checked="" name="options"><i class="fa fa-comment-o"></i> Reply
                                                </label>
                                                <label class="btn btn-default">
                                                    <input type="radio" name="options"> <i class="fa fa-thumbs-o-up"></i> Like
                                                </label>
                                            </div>
                                            <form class="post-reply">
                                                <textarea placeholder="Write your comment"></textarea>
                                                <i class="fa fa-comments-o"></i>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </li>

                            <li>
                                <div class="timeline">
                                    <div class="user-timeline">
                                        <span><img src="<?= ASSETS; ?>images/resource/sender3.jpg" alt=""></span>
                                    </div>
                                    <div class="timeline-detail">
                                        <div class="timeline-head">
                                            <h3>Yameen khandil<span>2 hours ago</span><i class="blue">Mod</i></h3>
                                            <div class="social-share">
                                                <a title=""><i class="fa fa-share-alt"></i></a>
                                                <ul class="social-btns">
                                                    <li><a title="Facebook" data-toggle="tooltip" data-placement="left" href="#"><i class="fa fa-facebook"></i></a></li>
                                                    <li><a title="Google" data-toggle="tooltip" data-placement="left" href="#"><i class="fa fa-google-plus"></i></a></li>
                                                    <li><a title="Twitter" data-toggle="tooltip" data-placement="left" href="#"><i class="fa fa-twitter"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="timeline-content">
                                            <p>at <a href="#" title="">Khana Zidi</a> Jonathan DOe Uploaded 4 new photos.</p>
                                            <div class="timeline-gallery">
                                                <ul>
                                                    <li><a title="Gallery Image" class="html5lightbox" href="<?= ASSETS; ?>images/resource/view.gif"><i class="fa fa-search"></i><img src="<?= ASSETS; ?>images/resource/gallery1.jpg" alt=""></a></li>
                                                    <li><a title="Gallery Image" class="html5lightbox" href="<?= ASSETS; ?>images/resource/view.gif"><i class="fa fa-search"></i><img src="<?= ASSETS; ?>images/resource/gallery2.jpg" alt=""></a></li>
                                                    <li><a title="Gallery Image" class="html5lightbox" href="<?= ASSETS; ?>images/resource/view.gif"><i class="fa fa-search"></i><img src="<?= ASSETS; ?>images/resource/gallery3.jpg" alt=""></a></li>
                                                    <li><a title="Gallery Image" class="html5lightbox" href="<?= ASSETS; ?>images/resource/view.gif"><i class="fa fa-search"></i><img src="<?= ASSETS; ?>images/resource/gallery4.jpg" alt=""></a></li>
                                                </ul>
                                            </div>
                                            <div data-toggle="buttons" class="btn-group btn-group-sm">
                                                <label class="btn btn-default">
                                                    <input type="radio" checked="" name="options"><i class="fa fa-comment-o"></i> Reply
                                                </label>
                                                <label class="btn btn-default">
                                                    <input type="radio" name="options"> <i class="fa fa-thumbs-o-up"></i> Like
                                                </label>
                                            </div>
                                            <form class="post-reply">
                                                <textarea placeholder="Write your comment"></textarea>
                                                <i class="fa fa-comments-o"></i>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </li>

                            <li>
                                <div class="timeline">
                                    <div class="user-timeline">
                                        <span><img src="<?= ASSETS; ?>images/resource/sender1.jpg" alt=""></span>
                                    </div>
                                    <div class="timeline-detail">
                                        <div class="timeline-head">
                                            <h3>Brindal Dazi<span>4 min ago</span><i class="green">User</i></h3>
                                            <div class="social-share">
                                                <a title=""><i class="fa fa-share-alt"></i></a>
                                                <ul class="social-btns">
                                                    <li><a title="Facebook" data-toggle="tooltip" data-placement="left" href="#"><i class="fa fa-facebook"></i></a></li>
                                                    <li><a title="Google" data-toggle="tooltip" data-placement="left" href="#"><i class="fa fa-google-plus"></i></a></li>
                                                    <li><a title="Twitter" data-toggle="tooltip" data-placement="left" href="#"><i class="fa fa-twitter"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="timeline-content">
                                            <p>Set it as the font <a href="#" title="">John Doe</a> in your applition, and copy and paste the icons. Print this page to PDF for te completet of vectors. Or to use othe the Font Aweo Awesome</p>
                                            <div data-toggle="buttons" class="btn-group btn-group-sm">
                                                <label class="btn btn-default">
                                                    <input type="radio" checked="" name="options"><i class="fa fa-comment-o"></i> Reply
                                                </label>
                                                <label class="btn btn-default">
                                                    <input type="radio" name="options"> <i class="fa fa-thumbs-o-up"></i> Like
                                                </label>
                                            </div>
                                            <form class="post-reply">
                                                <textarea placeholder="Write your comment"></textarea>
                                                <i class="fa fa-comments-o"></i>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </li>

                        </ul>
                    </div><div style="position: absolute; z-index: 1; margin: 0px; padding: 0px; opacity: 0; display: block; left: 767px; top: 69px;"><div class="enscroll-track track3" style="position: relative; height: 373px;"><a href="" class="handle3" style="position: absolute; z-index: 1; height: 178.82904884318768px; top: 95.80246913580247px;"><div class="top"></div><div class="bottom"></div></a></div></div>
                </div>
            </div>
        </div>
        <div class="col-md-7">
            <div class="widget-area pattern">
                <h2 class="widget-title"><strong>Visitor</strong> Statistics</h2>
                <ul class="panel-function">
                    <li class="dropdown">
                        <a  role="button" data-toggle="dropdown" href="#"> <b class="caret"></b></a>
                        <ul  class="dropdown-menu" role="menu" >
                            <li role="presentation"><a title="" class="hide-btn"><i class="fa fa-minus"></i></a></li>
                            <li role="presentation"><a title="" class="close-sec"><i class="fa fa-close"></i></a></li>
                        </ul>
                    </li>
                </ul>
                <div class="widget">
                    <div id="graph-wrapper">
                        <div class="graph-info">
                            <a href="" class="visitors"><span class="green"></span>Visitors</a>
                            <a href="" class="returning"><span class="blue"></span>Returning Visitors</a>
                            <a href="#" id="bars" class="graph-tab-btn"><span><i class="fa fa-bar-chart-o"></i></span></a>
                            <a href="#" id="lines" class="active graph-tab-btn"><span><i class="fa fa-code-fork"></i></span></a>
                        </div>
                        <div class="graph-container">
                            <div id="graph-lines"></div>
                            <div id="graph-bars"></div>
                        </div>
                        <div class="graph-details">
                            <ul>
                                <li>
                                    <span id="new-orders" class="sparkline">4,5,6,7,6,5,4,3,2,2,4</span>
                                    <p>18,304<i>New Orders</i></p>
                                </li>
                                <li>
                                    <span id="new-sales" class="sparkline">2,3,4,5,2,5,6,2,8,5,1,5</span>
                                    <p>2,39,231<i>New Sales</i></p>
                                </li>
                                <li>
                                    <span id="new-visitors" class="sparkline">6,5,3,1,4,5,6,7,3,2,3,8</span>
                                    <p>56,234<i>New Visitors</i></p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div><!-- Widget -->
            </div><!-- Widget Area -->
        </div>
    </div><!-- title Date Range -->-->
    <?php //$this->load->view("slide_panel"); ?> 
</div><!-- Page Container -->
<?php $this->load->view("footer"); ?> 
    <!--<script type="text/javascript" src="<?= ASSETS; ?>js/daterangepicker.js"></script>
    <script type="text/javascript" src="<?= ASSETS; ?>js/moment.js"></script>
    
    
    <script type="text/javascript" src="<?= ASSETS; ?>js/chart-line-and-graph.js"></script>
    <script type="text/javascript" src="<?= ASSETS; ?>js/jquery.flot.min.js"></script>
    <script type="text/javascript" src="<?= ASSETS; ?>js/jquery.sparkline.min.js"></script>-->
<!--    <script type="text/javascript" src="<?= ASSETS; ?>js/owl.carousel.min.js"></script>
    
<script type="text/javascript">
    $(document).ready(function() {

        $(".carousal-sec").owlCarousel({
            autoPlay: true,
            stopOnHover: true,
            goToFirstSpeed: 500,
            slideSpeed: 500,
            singleItem: true,
            autoHeight: true,
            transitionStyle: "backSlide",
            navigation: true
        });

        $("#pie").sparkline([1, 1, 2], {
            type: 'pie',
            width: '40',
            height: '40',
            sliceColors: ['#2dcb73', '#fd6a59', '#17c3e5', '#109618', '#66aa00', '#dd4477', '#0099c6', '#990099 ']});

        $(function() {
            $("#new-orders").sparkline([4, 5, 6, 7, 6, 5, 4, 3, 2, 2, 3, 4], {
                type: 'bar',
                height: '40px',
                barSpacing: 3,
                barWidth: 6,
                barColor: '#2dcb73',
                negBarColor: '#D6A838'});
        });

        $(function() {
            $("#new-sales").sparkline([2, 3, 4, 5, 2, 5, 6, 2, 8, 5, 1, 5], {
                type: 'bar',
                height: '40px',
                barSpacing: 3,
                barWidth: 6,
                barColor: '#ff604f',
                negBarColor: '#D6A838'});
        });

        $(function() {
            $("#new-visitors").sparkline([6, 5, 3, 1, 4, 5, 6, 7, 3, 2, 3, 8], {
                type: 'bar',
                height: '40px',
                barSpacing: 3,
                barWidth: 6,
                barColor: '#17c3e5',
                negBarColor: '#D6A838'});
        });

        $('#reportrange').daterangepicker(
                {
                    startDate: moment().subtract('days', 29),
                    endDate: moment(),
                    minDate: '01/01/2012',
                    maxDate: '12/31/2014',
                    dateLimit: {days: 60},
                    showDropdowns: true,
                    showWeekNumbers: true,
                    timePicker: false,
                    timePickerIncrement: 1,
                    timePicker12Hour: true,
                    ranges: {
                        'Today': [moment(), moment()],
                        'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
                        'Last 7 Days': [moment().subtract('days', 6), moment()],
                        'Last 30 Days': [moment().subtract('days', 29), moment()],
                        'This Month': [moment().startOf('month'), moment().endOf('month')],
                        'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
                    },
                    opens: 'left',
                    buttonClasses: ['btn btn-default'],
                    applyClass: 'btn-small btn-primary',
                    cancelClass: 'btn-small',
                    format: 'MM/DD/YYYY',
                    separator: ' to ',
                    locale: {
                        applyLabel: 'Submit',
                        fromLabel: 'From',
                        toLabel: 'To',
                        customRangeLabel: 'Custom Range',
                        daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                        monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                        firstDay: 1
                    }
                },
        function(start, end) {
            console.log("Callback has been called!");
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }
        );
        //Set the initial state of the picker label
        $('#reportrange span').html(moment().subtract('days', 29).format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));

        $(function() {
            $('#map').vectorMap({map: 'world_en'});
        })


    });
</script>-->-->
</body>
</html>