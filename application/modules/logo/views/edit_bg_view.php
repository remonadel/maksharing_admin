<?php $this->load->view("header"); ?>

<div class="container">
	<div class="col-md-12">
		<div class="main-title">
			<h1> تعديل الخلفية  </h1>
		</div>
	</div>
	<div class="row">
		<div class="masonary-grids">
			<div class="col-md-12">
				<div class="widget-area">
					<div class="wizard-form-h">
						<form action="" method="post" enctype="multipart/form-data">
							<div class="col-md-122" style="margin-bottom: 10px;">
								<?php if (isset($status)) echo $status; ?>
							</div>

							<div class="col-md-122">
								<div class="inline-form">
									<label class="c-label">الصورة *</label>
									<input type="file" name="bgimg" accept=".jpg" title="يجب ان تكون الصورة jpg  " />
								</div>
							</div>
							<div class="col-md-122" style="margin-top: 12px;">
								<img src="<?= UPLOADS;?>slide_3.jpg" alt="logo"
									 width="200px"  />
							</div>
							<div class="col-md-122" style="margin-top: 12px;">
								<input type="submit" name="submit" value="Save" class="btn btn-success large" />
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php //$this->load->view("slide_panel"); ?>
</div><!-- Page Container -->
<?php $this->load->view("footer"); ?>
</body>
</html>
