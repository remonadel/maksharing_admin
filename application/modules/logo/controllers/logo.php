<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Logo extends MX_Controller {

	public function __construct()
    {
        parent::__construct();
		$this->deny->deny_if_logged_out();

    }

	public function index()
	{
		$authorized = $this->common_model->authorized_to_view_page("products");
		if ($authorized)
		{
			if (isset($_POST["submit"]))
			{
				$success = true;
				if ( ! empty($_FILES["logo"]["name"]))
				{
					$image_name = basename($_FILES["logo"]["name"]);
					$tmp_name = $_FILES["logo"]["tmp_name"];
					$allowed_ext = "png";
					$a = explode(".", $image_name);
					$file_ext = strtolower(end($a)); unset($a);
					// Restricting file uploading to zip files only
					if ($file_ext != $allowed_ext)
					{
						$success = FALSE;
						$data["status"] = "<p class='error-msg'>لقد قمت برفع ملف غير مسموح به</p>";
					}
					else
					{
						// Delete old image and upload new one
						//@unlink(UPLOADS_PATH . 'logo.png');
						move_uploaded_file($tmp_name, UPLOADS_PATH . 'logo.png');
					}
				}

				if ($success)
				{

					$this->session->set_flashdata("status", "<p class='success-msg'> تم تعديل اللوجو</p>");
					redirect(site_url() . "logo");
				}

			}

			$this->load->view("edit_logo_view");
		}
	}

	public function background()
	{
		$authorized = $this->common_model->authorized_to_view_page("products");
		if ($authorized)
		{
			if (isset($_POST["submit"]))
			{
				$success = true;
				if ( ! empty($_FILES["bgimg"]["name"]))
				{
					$image_name = basename($_FILES["bgimg"]["name"]);
					$tmp_name = $_FILES["bgimg"]["tmp_name"];
					$allowed_ext = "jpg";
					$a = explode(".", $image_name);
					$file_ext = strtolower(end($a)); unset($a);
					// Restricting file uploading to zip files only
					if ($file_ext != $allowed_ext)
					{
						$success = FALSE;
						$data["status"] = "<p class='error-msg'>لقد قمت برفع ملف غير مسموح به</p>";
					}
					else
					{
						// Delete old image and upload new one
						//@unlink(UPLOADS_PATH . 'logo.png');
						move_uploaded_file($tmp_name, UPLOADS_PATH . 'slide_3.jpg');
					}
				}

				if ($success)
				{

					$this->session->set_flashdata("status", "<p class='success-msg'> تم تعديل صورة الخلفية</p>");
					redirect(site_url() . "logo/background");
				}

			}

			$this->load->view("edit_bg_view");
		}
	}

}


/* End of file categories.php */
/* Location: ./application/modules/categories/controllers/categories.php */
