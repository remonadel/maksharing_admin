<?php $this->load->view("head.php"); ?>
<body>
	<div class="row">
		<div class="col-md-62" style="float: right;  position: relative; top: 18px; right: 50px;">
			<a class="btn btn-primary btn-font" href="<?= site_url(); ?>images/add/mini">إضافة صورة جديدة</a>
		</div>
		<div class="col-md-62" style="float: right; position: relative; top: 4px; left: 28px;">
			<form action="" method="post" class="search">
				<input type="text" name="search" placeholder="بحث" required autofocus />
				<button name="submit"><i class="fa fa-search"></i></button>
			</form>
		</div>
	</div>
	<div class="row">
		<div class="widget-area" style="padding-right: 70px;">
			<div class="col-md-122">
				<?php if (isset($images)): ?>
				<?php foreach ($images as $image): ?>
					<div class="archive-img-div" style="margin-bottom: 25px; position: relative;">
						<img class="archive-img" src="<?= IMG_ARCHIVE . '311x253/' . $image['name'] . '?' . @filemtime(IMG_ARCHIVE_PATH . '311x253/' . $image['name']); ?>"
							 id="<?= $image['id']; ?>&<?= $image['name']; ?>" />
						<div>
							<i class="fa fa-external-link-square long-url-btn" style="cursor: pointer;" title="رابط الصورة الطولية"></i>
							<span style="margin: 0px 6px;" title="<?= $image['description']; ?>"><?= mb_substr($image["description"], 0, 6, "utf-8"); ?></span>|<span style="float: left; margin: 0px 6px;">منذ: <?= $image["uploaded_at"]; ?></span>
						</div>
						<div style="display: none; padding: 6px; background-color: #C2C2C2; position: absolute; bottom: 74px; right: 6px;">
							<input class="long-url-val" style="width: 100%;" value="<?= IMG_ARCHIVE . 'original_lower_quality/' . $image['name']; ?>" />
						</div>
					</div>
				<?php endforeach; ?>
				<?php endif; ?>

				<?php if (isset($pagination)): ?>
					<div class="pagination-news" style="margin-top: 18px;">
						<?= $pagination; ?>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>

	<script>
		$(document).ready(function() {
			$(".long-url-btn").click(function() {
				$(this).parent().next().toggle();
				$(this).parent().next().find(".long-url-val").select();
			});

			$(".archive-img").click(function() {
				var image = $(this).attr("id");
				window.document.title = image;
				window.close();
			});
		});
	</script>
</body>
</html>
