<?php $this->load->view("header"); ?>

<div class="container">
	<div class="col-md-12">
		<div class="main-title">
			<h1>تعديل صورة</h1>
		</div>
	</div>
	<div class="row">
		<div class="masonary-grids">
			<div class="col-md-12">
				<div class="widget-area">
					<div class="wizard-form-h">
						<form action="" method="post">
							<?php if (file_exists(IMG_ARCHIVE_PATH . "cache/647x354_" . $image['name'])): ?>
								<div>
									<img id="647x354"
									src="<?= IMG_ARCHIVE . 'cache/647x354_' . $image['name']; ?>" />
								</div><br />
							<?php endif; ?>

							<?php if (file_exists(IMG_ARCHIVE_PATH . "cache/311x253_" . $image['name'])): ?>
								<div>
									<img id="311x253"
									src="<?= IMG_ARCHIVE . 'cache/311x253_' . $image['name']; ?>" />
								</div><br />
							<?php endif; ?>

							<?php if (file_exists(IMG_ARCHIVE_PATH . "cache/235x253_" . $image['name'])): ?>
								<div>
									<img id="235x253"
									src="<?= IMG_ARCHIVE . 'cache/235x253_' . $image['name']; ?>" />
								</div><br />
							<?php endif; ?>

							<?php if (file_exists(IMG_ARCHIVE_PATH . "cache/156x112_" . $image['name'])): ?>
								<div>
									<img id="156x112"
									src="<?= IMG_ARCHIVE . 'cache/156x112_' . $image['name']; ?>" />
								</div><br />
							<?php endif; ?>

							<div class="col-md-62" style="margin-bottom: 10px;">
								<div class="inline-form">
									<label class="c-label">الوصف</label>
									<input type="text" name="description" value="<?= $image['description']; ?>" required />
								</div>
							</div>

							<input type="hidden" name="647x354_x" id="647x354_x" />
							<input type="hidden" name="647x354_y" id="647x354_y" />
							<input type="hidden" name="311x253_x" id="311x253_x" />
							<input type="hidden" name="311x253_y" id="311x253_y" />
							<input type="hidden" name="235x253_x" id="235x253_x" />
							<input type="hidden" name="235x253_y" id="235x253_y" />
							<input type="hidden" name="156x112_x" id="156x112_x" />
							<input type="hidden" name="156x112_y" id="156x112_y" />
							<div class="col-md-122">
								<input type="submit" name="submit" value="حفظ" class="btn btn-success btn-font" style="width: 90px; margin-top: 20px;" />
							</div>
						</form>
					</div>
				</div>
				<div class="widget-area">
					<h4 style="margin-bottom: 0px; margin-top: -5px;">تبديل الصورة:</h4>
					<div class="col-md-122">
						<form action="<?= site_url() . 'images/add'; ?>" method="post" enctype="multipart/form-data">
							<div class="inline-form" style="margin-bottom: 15px;">
								<label class="c-label">الصورة الجديدة</label>
								<input type="file" name="image" title="jpg, jpeg, png, or gif" required />
							</div>
							<input type="hidden" name="existing_name" value="<?= $image['name']; ?>" />
							<input type="hidden" name="existing_id" value="<?= $image['id']; ?>" />
							<input type="hidden" name="existing_watermark" value="<?= $image['watermarked']; ?>" />
							<input type="submit" name="replace" value="تنفيذ" class="btn btn-primary btn-font" style="width: 90px; margin-top: 15px;" />
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php //$this->load->view("slide_panel"); ?>
</div><!-- Page Container -->
<?php $this->load->view("footer"); ?>
<script>
	// This will fire after the entire page is loaded, including images
	$(window).load(function() {
		var api_647x354;
		var api_311x253;
		var api_235x253;
		var api_156x112;

		var crop_width_647x354 = "<?= $size_647x354[0]; ?>";
		var crop_height_647x354 = "<?= $size_647x354[1]; ?>";
		var crop_width_311x253 = "<?= $size_311x253[0]; ?>";
		var crop_height_311x253 = "<?= $size_311x253[1]; ?>";
		var crop_width_235x253 = "<?= $size_235x253[0]; ?>";
		var crop_height_235x253 = "<?= $size_235x253[1]; ?>";
		var crop_width_156x112 = "<?= $size_156x112[0]; ?>";
		var crop_height_156x112 = "<?= $size_156x112[1]; ?>";

		// 647x354
		var opt_647x354 = {
			onChange: editCoords647x354,
			onSelect: editCoords647x354
		};
		opt_647x354.allowResize = false;
		opt_647x354.allowSelect = false;
		api_647x354 = $.Jcrop('#647x354', opt_647x354);
		api_647x354.setSelect([0, 0, crop_width_647x354, crop_height_647x354]);

		// 311x253
		var opt_311x253 = {
			onChange: editCoords311x253,
			onSelect: editCoords311x253
		};
		opt_311x253.allowResize = false;
		opt_311x253.allowSelect = false;
		api_311x253 = $.Jcrop('#311x253', opt_311x253);
		api_311x253.setSelect([0, 0, crop_width_311x253, crop_height_311x253]);

		// 235x253
		var opt_235x253 = {
			onChange: editCoords235x253,
			onSelect: editCoords235x253
		};
		opt_235x253.allowResize = false;
		opt_235x253.allowSelect = false;
		api_235x253 = $.Jcrop('#235x253', opt_235x253);
		api_235x253.setSelect([0, 0, crop_width_235x253, crop_height_235x253]);

		// 156x112
		var opt_156x112 = {
			onChange: editCoords156x112,
			onSelect: editCoords156x112
		};
		opt_156x112.allowResize = false;
		opt_156x112.allowSelect = false;
		api_156x112 = $.Jcrop('#156x112', opt_156x112);
		api_156x112.setSelect([0, 0, crop_width_156x112, crop_height_156x112]);
	});

	function editCoords647x354(c) {
		$("#647x354_x").val(c.x);
		$("#647x354_y").val(c.y);
	};

	function editCoords311x253(c) {
		$("#311x253_x").val(c.x);
		$("#311x253_y").val(c.y);
	};

	function editCoords235x253(c) {
		$("#235x253_x").val(c.x);
		$("#235x253_y").val(c.y);
	};

	function editCoords156x112(c) {
		$("#156x112_x").val(c.x);
		$("#156x112_y").val(c.y);
	};
</script>
</body>
</html>
