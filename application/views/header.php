<?php $this->load->view("head.php"); ?>
<body>
    <div class="main">
        <header class="header">
            <div class="dropdown profile">
                <a title="">
                    <i class="caret" style="margin-left: 10px;margin-top: 16px;float: right;"></i>
                    <p class="p-name-pro"><?= $this->session->userdata("name"); ?></p>
                    <?php $user_pic = $this->session->userdata("picture"); ?>
                    <?php if ( ! empty($user_pic)): ?>
                        <img src="<?= USER_PHOTOS; ?><?= $this->session->userdata("picture"); ?>" alt="<?= $this->session->userdata("name"); ?>" />
                    <?php endif; ?>
                </a>
				<div class="profile drop-list">
                    <ul>
                        <li><a href="<?= site_url(); ?>change_password" title=""><i class="fa fa-edit"></i>تغيير كلمة السر</a></li>
                        <li><a href="<?= site_url(); ?>change_picture" title=""><i class="fa fa-user"></i>تغيير الصورة</a></li>
                    </ul>
                </div>
            </div>
            <div class="logo">
                <a href="" title=""></a>
                <a href="#" class="toggle-menu"><i class="fa fa-bars"></i></a>
            </div>
        </header><!-- Header -->
        <div class="page-container menu-left">
            <aside class="sidebar">
                <div class="profile-stats">
                    <div class="mini-profile">
                        <span><img src="<?= USER_PHOTOS; ?><?= $this->session->userdata("picture"); ?>" alt="<?= $this->session->userdata("name"); ?>" /></span>
                        <h3><?= $this->session->userdata("name"); ?></h3>
                        <h6 class="status online"><i></i> متاح الان</h6>
                        <a href="<?= site_url(); ?>logout" title="Logout" class="logout red" data-toggle="tooltip" data-placement=
                           "right"><i class="fa fa-power-off"></i></a>
                    </div>
                </div>
                <div class="menu-sec">
                    <div id="menu-toogle" class="menus">

                    <?php if ($this->session->userdata("banners") == 1): ?>
                        <div class="single-menu">
                            <h2><a><i class="fa fa-picture-o"></i><span>اللوجو و الخلفية  </span></a></h2>
                            <div class="sub-menu">
                                <ul>
                                    <li><a href="<?= site_url(); ?>logo"> تغيير اللوجو</a></li>
                                    <li><a href="<?= site_url(); ?>logo/background"> تغيير صورة الخلفية</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="single-menu">
                            <h2><a><i class="fa fa-picture-o"></i><span>الصفحة الرئيسية </span></a></h2>
                            <div class="sub-menu">
                                <ul>
                                    <li><a href="<?= site_url(); ?>banners">السلايدر الرئيسي</a></li>
                                    <li><a href="<?= site_url(); ?>banners/secondary">السلايدر الفرعى</a></li>
                                </ul>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php if ($this->session->userdata("banners") == 1): ?>
                        <div class="single-menu">
                            <h2><a><i class="fa fa-picture-o"></i><span>الصفحات</span></a></h2>
                            <div class="sub-menu">
                                <ul>
                                    <li><a href="<?= site_url(); ?>pages/edit/1"> صفحة طرق الدفع</a></li>
                                    <li><a href="<?= site_url(); ?>pages/edit/2"> صفحة اشتراك CCcam </a></li>
                                    <li><a href="<?= site_url(); ?>pages/edit/3"> صفحة الترحيب 1 </a></li>
                                    <li><a href="<?= site_url(); ?>pages/edit/4">   صفحة الترحيب 2 </a></li>
                                    <li><a href="<?= site_url(); ?>pages/edit/5"> صفحة الترحيب 3 </a></li>
                                    <li><a href="<?= site_url(); ?>pages/edit/6">   صفحة الترحيب 4 </a></li>

                                </ul>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php if ($this->session->userdata("banners") == 1): ?>
                        <div class="single-menu">
                            <h2><a><i class="fa fa-picture-o"></i><span>خطط الأسعار</span></a></h2>
                            <div class="sub-menu">
                                <ul>
                                    <li><a href="<?= site_url(); ?>pricing/index/1"> خطط أسعار CCcam</a></li>
                                    <li><a href="<?= site_url(); ?>pricing/index/2">  خطط أسعار IPTv </a></li>
                                    <li><a href="<?= site_url(); ?>pricing/add">  إضافة خطة أسعار  </a></li>
                                    <li><a href="<?= site_url(); ?>banners/special">  عروض التوفير    </a></li>
                                </ul>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php if ($this->session->userdata("banners") == 1): ?>
                            <div class="single-menu">
                                <h2><a><i class="fa fa-picture-o"></i><span>الأسئلة الشائعة </span></a></h2>
                                <div class="sub-menu">
                                    <ul>
                                        <li><a href="<?= site_url(); ?>faq"> التحكم بالأسئلة  </a></li>
                                        <li><a href="<?= site_url(); ?>faq/add">  إضافة سؤال  </a></li>
                                    </ul>
                                </div>
                            </div>
                        <?php endif; ?>
                        <?php if ($this->session->userdata("banners") == 1): ?>
                            <div class="single-menu">
                                <h2><a><i class="fa fa-picture-o"></i><span>تصنيفات الباقات  </span></a></h2>
                                <div class="sub-menu">
                                    <ul>
                                        <li><a href="<?= site_url(); ?>categories"> التحكم بتصنيفات IPTv </a></li>
                                        <li><a href="<?= site_url(); ?>categories/add"> IPTv إضافة تصنيف  </a></li>
                                    </ul>
                                </div>
                            </div>
                        <?php endif; ?>
                        <?php if ($this->session->userdata("banners") == 1): ?>
                            <div class="single-menu">
                                <h2><a><i class="fa fa-picture-o"></i><span>قنوات IPTv   </span></a></h2>
                                <div class="sub-menu">
                                    <ul>
                                        <li><a href="<?= site_url(); ?>subcategories"> التحكم  بالقنوات </a></li>
                                        <li><a href="<?= site_url(); ?>subcategories/add"> IPTv إضافة قناة  </a></li>
                                    </ul>
                                </div>
                            </div>
                        <?php endif; ?>
                        <?php if ($this->session->userdata("videos") == 1): ?>
                            <div class="single-menu">
                                <h2><a><i class="fa fa-desktop"></i><span>الفيديوهات</span></a></h2>
                                <div class="sub-menu">
                                    <ul>
                                        <li><a href="<?= site_url(); ?>videos/add">فيديو جديد</a></li>
                                        <li><a href="<?= site_url(); ?>videos">إدارة الفيديوهات </a></li>
                                    </ul>
                                </div>
                            </div>
                        <?php endif; ?>
                        <?php if ($this->session->userdata("images") == 1): ?>
                            <div class="single-menu">
                                <h2><a><i class="fa fa-file-image-o"></i><span>الصور</span></a></h2>
                                <div class="sub-menu">
                                    <ul>
                                        <li><a href="<?= site_url(); ?>images/add">إضافة صور</a></li>
                                        <li><a href="<?= site_url(); ?>images">إدارة الصور</a></li>

                                    </ul>
                                </div>
                            </div>
                        <?php endif; ?>
                 <!--  <?php if ($this->session->userdata("pdfs") == 1): ?>
                        <div class="single-menu">
                            <h2><a><i class="fa fa-file-pdf-o"></i><span>PDFS</span></a></h2>
                            <div class="sub-menu">
                                <ul>
                                    <li><a href="<?= site_url(); ?>pdfs/add">إضافة PDF</a></li>
                                    <li><a href="<?= site_url(); ?>pdfs">إدارة الPDFS</a></li>
                                </ul>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php if ($this->session->userdata("products") == 1): ?>
                        <div class="single-menu">
                            <h2><a><i class="fa fa-shopping-cart"></i><span>المنتجات</span></a></h2>
                            <div class="sub-menu">
                                <ul>
                                    <li><a href="<?= site_url(); ?>products/add">منتج جديد</a></li>
                                    <li><a href="<?= site_url(); ?>products">إدارة المنتجات</a></li>
                                    <li><p class="bor-bott-men"></p></li>
                                    <li><a href="<?= site_url(); ?>categories/add">قسم رئيسي جديد</a></li>
                                    <li><a href="<?= site_url(); ?>categories">إدارة الأقسام الرئيسية</a></li>
                                    <li><a href="<?= site_url(); ?>subcategories/add">قسم فرعي جديد</a></li>
                                    <li><a href="<?= site_url(); ?>subcategories">إدارة الأقسام الفرعية</a></li>
                                </ul>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php if ($this->session->userdata("companies") == 1): ?>
						<div class="single-menu">
                            <h2><a><i class="fa fa-flag"></i><span>الشركات</span></a></h2>
                            <div class="sub-menu">
                                <ul>
                                    <li><a href="<?= site_url(); ?>companies/add">شركة جديدة</a></li>
                                    <li><a href="<?= site_url(); ?>companies">إدارة الشركات</a></li>
                                </ul>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php if ($this->session->userdata("news") == 1): ?>
                        <div class="single-menu" id="cli-dr1">
                            <h2><a><i class="fa fa-list"></i><span>الأخبار</span></a></h2>
                            <div class="sub-menu">
                                <ul>
                                    <li><a href="<?= site_url(); ?>news/add">خبر جديد</a></li>
                                    <li><a href="<?= site_url(); ?>news/unpublished">إدارة الأخبار</a></li>
                                    <li style="display: none;"><a href="<?= site_url(); ?>news/breaking">إدارة الأخبار</a></li>
                                    <li style="display: none;"><a href="<?= site_url(); ?>news/reviewed">إدارة الأخبار</a></li>
                                    <li style="display: none;"><a href="<?= site_url(); ?>news/published">إدارة الأخبار</a></li>
                                </ul>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php if ($this->session->userdata("videos") == 1): ?>
						<div class="single-menu">
                            <h2><a><i class="fa fa-desktop"></i><span>الفيديوهات</span></a></h2>
                            <div class="sub-menu">
                                <ul>
                                    <li><a href="<?= site_url(); ?>videos/add">فيديو جديد</a></li>
                                    <li><a href="<?= site_url(); ?>videos/unpublished">إدارة الفيديوهات </a></li>
                                </ul>
                            </div>
                        </div>
                    <?php endif; ?>



                    <?php if ($this->session->userdata("users") == 1): ?>
                        <div class="single-menu" id="cli-dr12">
                            <h2><a><i class="fa fa-user"></i><span>المستخدمين</span></a></h2>
                            <div class="sub-menu">
                                <ul>
                                    <li><a href="<?= site_url(); ?>users/add">مستخدم جديد</a></li>
                                    <li><a href="<?= site_url(); ?>users">إدارة المستخدمين</a></li>
                                    <li><p class="bor-bott-men"></p></li>
                                    <li><a href="<?= site_url(); ?>groups/add">مجموعة جديدة</a></li>
                                    <li><a href="<?= site_url(); ?>groups">إدارة المجموعات</a></li>
                                </ul>
                            </div>
                        </div>
                    <?php endif; ?>-->

                    </div>
                    <p>Copyright 2016 Maksharing</p>
                </div><!-- Menu Sec -->
            </aside><!-- Aside Sidebar -->
            <div class="content-sec">
                <div class="breadcrumbs">
                    <ul>
                        <li><a href="<?= site_url(); ?>" title="Home"><i class="fa fa-home"></i></a></li>
                    </ul>
                </div><!-- breadcrumbs -->
