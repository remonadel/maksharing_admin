<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Admin</title>
        <link rel="shortcut icon" type="image/x-icon" href="<?= ASSETS; ?>images/lightning.png" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- Stylesheets -->
        <link rel="stylesheet" type="text/css" href="<?= ASSETS; ?>css/font-awesome.css" /><!-- Font Awesome -->
        <link rel="stylesheet" type="text/css" href="<?= ASSETS; ?>css/bootstrap.css" /><!-- Bootstrap -->
        <link rel="stylesheet" type="text/css" href="<?= ASSETS; ?>css/style.css" /><!-- Style -->
        <link rel="stylesheet" type="text/css" href="<?= ASSETS; ?>css/responsive.css" /><!-- Responsive -->
        <link rel="stylesheet" type="text/css" href="<?= ASSETS; ?>css/jquery-ui.min.css" />
        <link rel="stylesheet" type="text/css" href="<?= ASSETS; ?>css/jquery.datetimepicker.css" />
        <link rel="stylesheet" type="text/css" href="<?= ASSETS; ?>css/jquery.timepicker.css" />
        <link rel="stylesheet" type="text/css" href="<?= ASSETS; ?>css/jquery.Jcrop.min.css" />
        <link rel="stylesheet" type="text/css" href="<?= ASSETS; ?>css/dropzone.css" />
        <link rel="stylesheet" type="text/css" href="<?= ASSETS; ?>css/sweetalert.css" />
        <link rel="stylesheet" type="text/css" href="<?= ASSETS; ?>css/google.css" />
        <!-- JS files -->
        <script src="<?= ASSETS; ?>js/jquery-1.11.2.min.js"></script>
        <script src="<?= ASSETS; ?>js/jquery-ui.min.js"></script>
        <script src="<?= ASSETS; ?>js/jquery.datetimepicker.js"></script>
        <script src="<?= ASSETS; ?>js/jquery.timepicker.js"></script>
        <script src="<?= ASSETS; ?>js/jquery.Jcrop.min.js"></script>
        <script src="<?= ASSETS; ?>js/template_Jcrop_script.js"></script>
        <script src="<?= ASSETS; ?>js/bootstrap.js"></script>
        <script src="<?= ASSETS; ?>js/sweetalert.min.js"></script>
        <script src="<?= ASSETS; ?>tinymce/tinymce.min.js"></script>
        <script src="<?= ASSETS; ?>js/dropzone.js"></script>
        <script src="<?= ASSETS; ?>js/modernizr.js"></script>
        <script src="<?= ASSETS; ?>js/enscroll.js"></script>
        <script src="<?= ASSETS; ?>js/script.js"></script>
    </head>
