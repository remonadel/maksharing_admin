<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = 'home';
$route['login'] = 'home/login';
$route['logout'] = 'home/logout';
$route['change_password'] = 'home/change_password';
$route['change_picture'] = 'home/change_picture';
$route['pdfs/(:num)'] = "pdfs";
$route['products/published/(:num)'] = "products/published";
$route['products/unpublished/(:num)'] = "products/unpublished";
$route['companies/(:num)'] = "companies";
$route['news/published/(:num)'] = "news/published";
$route['news/unpublished/(:num)'] = "news/unpublished";
$route['videos/published/(:num)'] = "videos/published";
$route['videos/unpublished/(:num)'] = "videos/unpublished";
$route['images/(:num)'] = "images";
$route['users/(:num)'] = "users";
$route['groups/(:num)'] = "groups";
$route['404_override'] = '';


/* End of file routes.php */
/* Location: ./application/config/routes.php */
