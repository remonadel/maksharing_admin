<?php if( ! defined('BASEPATH')) exit('No direct script access allowed');

class Deny {
    
    private $CI;
    
    public function __construct()
    {
        $this->CI =& get_instance();
        
        $this->CI->load->library('session');
    }
    
    public function logged_in()
    {
        return $this->CI->session->userdata("logged_in");
    }
    
    
    public function deny_if_logged_in()
    {
        if ($this->logged_in() === TRUE)
        {
            redirect(site_url());
        }
    }
    
    
    public function deny_if_logged_out()
    {
        if ($this->logged_in() === FALSE)
        {
            redirect(site_url("/login"));
        }
    }
    
}


/* End of file Deny.php */
/* Location: ./application/libraries/Deny.php */